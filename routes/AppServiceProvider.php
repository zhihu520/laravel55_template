<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        session_start();
        $model = new \App\Model\H();
        $a = $model->get('shop');
        foreach ($a as $k => $v) {
            $v->xilie = $model->find_all('xilie','pinpai_id',$v->id);
        }
        $_SESSION['home']['jiazu'] = $a;
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
