<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
$qiantai = 'home_';//前台前缀
$houtai = 'admin_';//后台前缀
$_SESSION['web']['qiantai'] = $qiantai;
$_SESSION['web']['houtai'] = $houtai;
// //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//前台管理系统--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------------------前台首页--------------------------------------------------------------
Route::get('/','Home\Index\IndexController@index');//前台首页
Route::get('about','Home\About\IndexController@index');//关于我们
Route::get('news','Home\News\IndexController@index');//新闻资讯
Route::get('news_con','Home\News\IndexController@news_con');//新闻详情
Route::get('case','Home\Anli\IndexController@index');//项目案例
Route::get('case_con','Home\Anli\IndexController@case_con');//项目案例
Route::get('wenti','Home\Wenti\IndexController@index');//常见问题
Route::get('wentishow','Home\Wenti\IndexController@wentishow');//常见问题
Route::get('contact','Home\Contact\IndexController@index');//联系我们
// //--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//后台管理系统--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------------------------------------------------------------------------------后台首页--------------------------------------------------------------
Route::get('admin','Admin\Index\IndexController@index');//后台首页
//----------------------------------------------------------------------------------------------------------------------------------------------
// 	//ajax 上下架,删除操作
Route::post('lower','Admin\Ajax\IndexController@lower');//ajax 下架操作
Route::post('show','Admin\Ajax\IndexController@show');//ajax 显示操作
Route::post('lower_show','Admin\Ajax\IndexController@lower_show');//ajax 单个显示/下架操作
Route::post('del_one','Admin\Ajax\IndexController@del_one');//ajax 单个删除
Route::post('del','Admin\Ajax\IndexController@del');//ajax 删除操作
//----------------------------------------------------------------------------------------------------------------------------------------------------------------登录管理--------------------------------------------------------------
Route::get($houtai.'login','Admin\Login\IndexController@index');//登录页面
Route::post($houtai.'dologin','Admin\Login\IndexController@login');//ajax 验证登录
Route::get($houtai.'login_true','Admin\Login\IndexController@login_true');//ajax 下面 登录成功,存储信息
Route::get($houtai.'loginout','Admin\Login\IndexController@loginout');//登录页面
//----------------------------------------------------------------------------------------------------------------------------------------------------------------新闻模块--------------------------------------------------------------
// 新闻分类   
Route::get($houtai.'news_group','Admin\News_group\IndexController@index');//这是新闻分类导航按钮
Route::get($houtai.'news_group_add','Admin\News_group\IndexController@add');//点击新增按钮
Route::post($houtai.'donews_group_add','Admin\News_group\IndexController@doadd');//确认新增
Route::get($houtai.'news_group_edit','Admin\News_group\IndexController@edit');//点击编辑按钮
Route::post($houtai.'donews_group_edit','Admin\News_group\IndexController@doedit');//确认编辑 
// 新闻列表
Route::get($houtai.'news_list','Admin\News_list\IndexController@index');//这是新闻列表导航按钮
Route::get($houtai.'news_list_add','Admin\News_list\IndexController@add');//点击新增按钮
Route::post($houtai.'donews_list_add','Admin\News_list\IndexController@doadd');//确认新增
Route::get($houtai.'news_list_edit','Admin\News_list\IndexController@edit');//点击编辑按钮
Route::post($houtai.'donews_list_edit','Admin\News_list\IndexController@doedit');//确认编辑
//----------------------------------------------------------------------------------------------------------------------------------------------------------------常见问题--------------------------------------------------------------
// 常见问题   
Route::get($houtai.'question','Admin\Question\IndexController@index');//这是常见问题导航按钮
Route::get($houtai.'question_add','Admin\Question\IndexController@add');//点击新增按钮
Route::post($houtai.'doquestion_add','Admin\Question\IndexController@doadd');//确认新增
Route::get($houtai.'question_edit','Admin\Question\IndexController@edit');//点击编辑按钮
Route::post($houtai.'doquestion_edit','Admin\Question\IndexController@doedit');//确认编辑 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------常见案例--------------------------------------------------------------
// 案例分类   
Route::get($houtai.'case_group','Admin\Anli_group\IndexController@index');//这是案例分类导航按钮
Route::get($houtai.'anli_group_add','Admin\Anli_group\IndexController@add');//点击新增按钮
Route::post($houtai.'doanli_group_add','Admin\Anli_group\IndexController@doadd');//确认新增
Route::get($houtai.'anli_group_edit','Admin\Anli_group\IndexController@edit');//点击编辑按钮
Route::post($houtai.'doanli_group_edit','Admin\Anli_group\IndexController@doedit');//确认编辑
// 常见案例   
Route::get($houtai.'case','Admin\Anli\IndexController@index');//这是常见案例导航按钮
Route::get($houtai.'anli_add','Admin\Anli\IndexController@add');//点击新增按钮
Route::post($houtai.'doanli_add','Admin\Anli\IndexController@doadd');//确认新增
Route::get($houtai.'anli_edit','Admin\Anli\IndexController@edit');//点击编辑按钮
Route::post($houtai.'doanli_edit','Admin\Anli\IndexController@doedit');//确认编辑 
//----------------------------------------------------------------------------------------------------------------------------------------------------------------系统设置--------------------------------------------------------------
// 公司简介   
Route::get($houtai.'about','Admin\About\IndexController@index');//这是公司简介导航按钮
Route::post($houtai.'doabout','Admin\About\IndexController@doabout');//这是公司简介确认信息
// // 公司信息   
Route::get($houtai.'info','Admin\About\IndexController@info');//这是公司信息导航按钮
Route::post($houtai.'doinfo','Admin\About\IndexController@doinfo');//这是公司信息导航按钮
// Route::get($houtai.'anli_add','Admin\Anli\IndexController@add');//点击新增按钮
// Route::post($houtai.'doanli_add','Admin\Anli\IndexController@doadd');//确认新增
// Route::get($houtai.'anli_edit','Admin\Anli\IndexController@edit');//点击编辑按钮
// Route::post($houtai.'doanli_edit','Admin\Anli\IndexController@doedit');//确认编辑 



