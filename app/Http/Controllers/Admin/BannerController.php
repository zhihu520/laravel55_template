<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class BannerController extends Controller
{	
	public $assign = [];


	/**
	 * /	pc端首页
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		// echo "<pre />";
		$model = new \App\Model\admin\pc_banner();
		$info = $model->get();
		// var_dump($info);die;
		$this ->assign['info'] = $info;
		return view('Admin.pc_banner',$this->assign);
	}

	/**
	 * /	pc端全部下架
	 * @return [type] [description]
	 */
	public function alllowerpc_banner()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\pc_banner();
		$reslut = $model->alllower($ids);
	}

	/**
	 * /	pc端全部显示
	 * @return [type] [description]
	 */
	public function allshowsizepc_banner()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\pc_banner();
		$reslut = $model->allshowsize($ids);
	}

	/**
	 * /	pc端批量删除
	 * @return [type] [description]
	 */
	public function alldeletesizepc_banner()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\pc_banner();
		$reslut = $model->alldeletesize($ids);
	}

	/**
	 * /	pc端单个显示下架
	 * @return [type] [description]
	 */
	public function ajaxeditpc_banner()
	{
		$model = new \App\Model\admin\pc_banner();
		$id = $_POST['id'];
		$reslut = $model->gengxin($id);
	}

	/**
	 * /	pc端单个删除
	 * @return [type] [description]
	 */
	public function ajaxdelpc_banner()
	{
		$id = $_POST['id'];
		$model = new \App\Model\admin\pc_banner();
		$reslut = $model->del($id);
	}

	/**
	 * /	手机端首页
	 * @return [type] [description]
	 */
	public function appindex()
	{
		// echo "<pre />";
		$model = new \App\Model\admin\sj_banner();
		$info = $model->get();
		// var_dump($info);die;
		$this ->assign['info'] = $info;
		return view('Admin.app_banner',$this->assign);
	}

	/**
	 * /	手机端全部下架
	 * @return [type] [description]
	 */
	public function alllowersj_banner()
	{
		// var_dump($_POST);die;
		$ids = $_POST['id'];
		$model = new \App\Model\admin\sj_banner();
		$reslut = $model->alllower($ids);
	}

	/**
	 * /	手机端全部显示
	 * @return [type] [description]
	 */
	public function allshowsizesj_banner()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\sj_banner();
		$reslut = $model->allshowsize($ids);
	}

	/**
	 * /	手机端批量删除
	 * @return [type] [description]
	 */
	public function alldeletesizesj_banner()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\sj_banner();
		$reslut = $model->alldeletesize($ids);
	}

	/**
	 * /	手机端单个显示下架
	 * @return [type] [description]
	 */
	public function ajaxeditsj_banner()
	{
		$model = new \App\Model\admin\sj_banner();
		$id = $_POST['id'];
		$reslut = $model->gengxin($id);
	}

	/**
	 * /	手机端单个删除
	 * @return [type] [description]
	 */
	public function ajaxdelsj_banner()
	{
		$id = $_POST['id'];
		$model = new \App\Model\admin\sj_banner();
		$reslut = $model->del($id);
	}

	/**
	 * /	pc端 新增
	 * @return [type] [description]
	 */
	public function pcbanneraddedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		$addoredit = $_POST['addoredit'];
		if ($addoredit == 'add') {
			if (!empty($_FILES['icon']['name'])) {
				$file = $_FILES["icon"]["tmp_name"];//获取的上传的临时文件  
				$name = $_FILES["icon"]["name"];//获取上传文件的文件名 
				$time = time();
				$time = $time.rand(0,1000000000); 
				$time = md5($time);
				date_default_timezone_set('PRC');
				$shijian = time();
				// var_dump(date('Y-m-d H',$shijian));die;
				$shijian = date('Y-m-d H',$shijian);
				$path="./file/amin/".$shijian.'/'.$time.'/';  
				if (is_dir($path)){    
				    echo "对不起！目录 " . $path . " 已经存在！";  
				    echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
				}else{  
				        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
				    $res=mkdir($path,0777,true); //递归创建文件目录  
				    if ($res){  
				        var_dump($res);//boolean true  
				            if (is_dir($path)){    
				                echo "目录 $path 创建成功";  
				                echo $path.$name;  
				                echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
				            }  
				    }else{  
				        echo "目录 $path 创建失败";  
				    }  
				}	
			}
			$title = $_POST['title'];
			$status = $_POST['status'];
			if (empty($_FILES['icon']['name'])) {
				$icon = '';
			}else{
				$icon = $path.$name;
			}
			if ($_POST['shanbushan'] == 'shan') {
			$icon = '';
			}
			$model = new \App\Model\admin\pc_banner();
			$reslut = $model->add($title,$status,$icon);
			return redirect("pc_banner");
		}else{
			if (!empty($_FILES['icon1']['name'])) {
				$file = $_FILES["icon1"]["tmp_name"];//获取的上传的临时文件  
				$name = $_FILES["icon1"]["name"];//获取上传文件的文件名 
				$time = time();
				$time = $time.rand(0,1000000000); 
				$time = md5($time);
				date_default_timezone_set('PRC');
				$shijian = time();
				// var_dump(date('Y-m-d H',$shijian));die;
				$shijian = date('Y-m-d H',$shijian);
				$path="./file/amin/".$shijian.'/'.$time.'/';  
				if (is_dir($path)){    
				    echo "对不起！目录 " . $path . " 已经存在！";  
				    echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
				}else{  
				        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
				    $res=mkdir($path,0777,true); //递归创建文件目录  
				    if ($res){  
				        var_dump($res);//boolean true  
				            if (is_dir($path)){    
				                echo "目录 $path 创建成功";  
				                echo $path.$name;  
				                echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
				            }  
				    }else{  
				        echo "目录 $path 创建失败";  
				    }  
				}	
			}
			$model = new \App\Model\admin\pc_banner();
			$title = $_POST['title'];
			$status = $_POST['status'];
			$id = $_POST['id'];
			if (empty($_FILES['icon1']['name'])) {
				$data = $model->find($id);
				$icon = $data['icon'];
			}else{
				$icon = $path.$name;
			}
			if ($_POST['shanbushan'] == 'shan') {
			$icon = '';
			}
			$reslut = $model->updatepc($id,$title,$status,$icon);
			return redirect("pc_banner");
		}
	}

	/**
	 * /	pc端 广告编辑查看
	 * @return [type] [description]
	 */
	public function diannaoguanggaobianji()
	{
		$id = $_POST['id'];
		$model = new \App\Model\admin\pc_banner();
		$data = $model->find($id);
		$shuju['title'] = $data['title'];
		$shuju['status'] = $data['status'];
		$shuju['icon'] = $data['icon'];
		return $shuju;
	}
}


















