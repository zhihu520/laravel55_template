<?php
namespace App\Http\Controllers\Admin\Group;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{	
	public $assign = [];
	public $table = 'group';
	public $model = '';
	public $m = '';

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = new \App\Model\M();
       $this->m = new \App\Model\admin\Group();
       return true;
	}

	/**
	 * /	角色列表
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $info = $this->model->paginate($this->table);
		$this ->assign['info'] = $info;
		return view('Admin.Group.index',$this->assign);
	}

	/**
	 * /	新增
	 * @return [type] [description]
	 */
	public function add()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
		return view('Admin.Group.add');
	}

	/**
	 * /	确认新增
	 * @return [type] [description]
	 */
	public function doadd()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $title = $_POST['title'];
		$main = $_POST['main'];
		$status = $_POST['status'];
		if (empty($_POST['quanxian'])) {
			$menu_auth = '';
		}else{
			$menu_auth = $_POST['quanxian'];
		}
		$result = $this->m->add($title,$main,$status,$menu_auth);
		return redirect("group");
	}

	/**
	 * /	编辑
	 * @return [type] [description]
	 */
	public function edit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
		$info = $this->model->find($this->table,'id',$_GET['id']);
		$info->menu_auth = explode(",", $info->menu_auth);
		$this ->assign['info'] = $info;
		return view('Admin.Group.edit',$this->assign);
	}

	/**
	 * /	确认编辑
	 * @return [type] [description]
	 */
	public function doedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
		$title = $_POST['title'];
		$status = $_POST['status'];
		$main = $_POST['main'];
		if (empty($_POST['quanxian'])) {
			$menu_auth = '';
		}else{
			$menu_auth = $_POST['quanxian'];
		}
		$result = $this->m->updategroup($_POST['id'],$title,$status,$main,$menu_auth);
		return redirect("group");
	}

//ajax----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * /	ajax 批量下架
	 * @return [type] [description]
	 */
	public function xia()
	{
		$reslut = $this->model->xia($this->table,$_POST['id']);
	}

	/**
	 * /	ajax 批量显示
	 * @return [type] [description]
	 */
	public function shang()
	{
		$reslut = $this->model->shang($this->table,$_POST['id']);
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function shang_xia()
	{
		$reslut = $this->model->shang_xia($this->table,$_POST['id']);
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function del_one()
	{
		$reslut = $this->model->del_one($this->table,$_POST['id']);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function del()
	{
		$reslut = $this->model->del($this->table,$_POST['id']);
	}

}


















