<?php
namespace App\Http\Controllers\Admin\Question;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{	
	public $assign = [];

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = $_SESSION['M'];
       if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login")->send();
        }
       return true;
	}

	/**
	 * /	常见问题
	 * @return [type] [description]
	 */
	public function index()
	{
        // echo "<pre>";
        $info = $this->model->paginate('question');
        $this ->assign['info'] = $info;
        // var_dump($info);die;
		return view('Admin.Question.index',$this->assign);
	}

	/**
	 * /	新增
	 * @return [type] [description]
	 */
	public function add()
	{
		return view('Admin.Question.add');
	}

	/**
	 * /	确认新增
	 * @return [type] [description]
	 */
	public function doadd()
	{
        $data['title'] = $_POST['title'];
        $data['miaoshu'] = $_POST['miaoshu'];
        $data['status'] = $_POST['status'];
        $data['create_time'] = time();
        $data['update_time'] = time();
        $id = $this->model->add('question',$data);
        return redirect($_SESSION['web']['houtai'].'question');
	}

	/**
	 * /	编辑
	 * @return [type] [description]
	 */
	public function edit()
	{
        $info = $this->model->find('question','id',$_GET['id']);
        $this ->assign['info'] = $info;
		return view('Admin.Question.edit',$this->assign);
	}

	/**
	 * /	确认编辑
	 * @return [type] [description]
	 */
	public function doedit()
	{
        $data['title'] = $_POST['title'];
        $data['miaoshu'] = $_POST['miaoshu'];
        $data['status'] = $_POST['status'];
        $data['create_time'] = time();
        $data['update_time'] = time();
        $result = $this->model->edit('question',$_POST['id'],$data);
        return redirect($_SESSION['web']['houtai'].'question');
	}
}


















