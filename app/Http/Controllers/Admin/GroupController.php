<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class GroupController extends Controller
{	
	public $assign = [];


	/**
	 * /	角色列表页面
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		// echo "<pre />";
		$model = new \App\Model\admin\group();
		$info = $model->get();
		// var_dump($info);die;
		$this ->assign['info'] = $info;
		return view('Admin.group',$this->assign);
	}

	/**
	 * /	角色编辑确认
	 * @return [type] [description]
	 */
	public function usergroupedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		$model = new \App\Model\admin\group();
		// $id = $_COOKIE['admin']['gid'];
		// echo "<pre />";
		// var_dump($_POST);die;
		$data = $_POST;
		$info = $model->gengxin($data);
		return redirect("group");
	}

	/**
	 * /	新增页面
	 * @return [type] [description]
	 */
	public function groupadd()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		return view('Admin.groupadd');
	}

	/**
	 * /	点击新增
	 * @return [type] [description]
	 */
	public function dogroupadd()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		$model = new \App\Model\admin\group();
		$title = $_POST['title'];
		$main = $_POST['main'];
		$status = $_POST['status'];
		if (empty($_POST['quanxian'])) {
			$menu_auth = '';
		}else{
			$menu_auth = $_POST['quanxian'];
		}
		$result = $model->add($title,$main,$status,$menu_auth);
		return redirect("group");
	}

	/**
	 * /	编辑页面
	 * @return [type] [description]
	 */
	public function groupedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		// echo "<pre />";
		$id = $_GET['id'];
		$model = new \App\Model\admin\group();
		$info = $model->find($id);
		$info['menu_auth'] = explode(",", $info['menu_auth']);
		// var_dump($info);die;
		$this ->assign['info'] = $info;
		return view('Admin.groupedit',$this->assign);
	}

	/**
	 * /	点击编辑
	 * @return [type] [description]
	 */
	public function dogroupedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		$id = $_POST['id'];
		$model = new \App\Model\admin\group();
		// echo "<pre />";
		// var_dump($_POST);die;
		$title = $_POST['title'];
		$status = $_POST['status'];
		$main = $_POST['main'];
		if (empty($_POST['quanxian'])) {
			$menu_auth = '';
		}else{
			$menu_auth = $_POST['quanxian'];
		}
		$result = $model->updategroup($id,$title,$status,$main,$menu_auth);
		return redirect("group");
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function ajaxeditgroup()
	{
		$model = new \App\Model\admin\group();
		$id = $_POST['id'];
		$reslut = $model->gengxin($id);
	}

	/**
	 * /	全部下架
	 * @return [type] [description]
	 */
	public function alllowergroup()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\group();
		$reslut = $model->alllower($ids);
	}

	/**
	 * /	全部显示
	 * @return [type] [description]
	 */
	public function allshowsizegroup()
	{

		$ids = $_POST['id'];
		$model = new \App\Model\admin\group();
		$reslut = $model->allshowsize($ids);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function alldeletesizegroup()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\group();
		$reslut = $model->alldeletesize($ids);
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function ajaxdelgroup()
	{
		$id = $_POST['id'];
		$model = new \App\Model\admin\group();
		$reslut = $model->del($id);
	}
}


















