<?php
namespace App\Http\Controllers\Admin\Xilie;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{	
	public $assign = [];
	public $table = 'xilie';
	public $model = '';
	public $m = '';

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = new \App\Model\M();
       $this->m = new \App\Model\admin\Xilie();
       return true;
	}

	/**
	 * /	品牌列表
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        if (!empty($_GET['id'])) {
        	$_SESSION['admin']['data']['xilie']['page'] = $_GET['id'];
        }
        $pinpai = $this->model->find('shop','id',$_SESSION['admin']['data']['xilie']['page']);
        $_SESSION['admin']['data']['shop']['id'] = $pinpai->id;
        $_SESSION['admin']['data']['shop']['title'] = $pinpai->title_z;
        $info = $this->m->get($_SESSION['admin']['data']['xilie']['page']);
        $this ->assign['info'] = $info;
		return view('Admin.Xilie.index',$this->assign);
	}

	/**
	 * /	新增
	 * @return [type] [description]
	 */
	public function add()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
		return view('Admin.Xilie.add',$this->assign);
	}

	/**
	 * /	确认新增
	 * @return [type] [description]
	 */
	public function doadd()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $result = $this->m->add($_POST['title'],$_POST['status'],$_POST['main']);
        return redirect($_SESSION['web']['houtai'].'show_xilie?id='.$_SESSION['admin']['data']['shop']['id']);
	}

	/**
	 * /	编辑
	 * @return [type] [description]
	 */
	public function edit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $info = $this->model->find($this->table,'id',$_GET['id']);
        $_SESSION['admin']['data']['xilie']['title'] = $info->title;
        $_SESSION['admin']['data']['xilie']['id'] = $info->id;
        $this ->assign['info'] = $info;
		return view('Admin.Xilie.edit',$this->assign);
	}

	/**
	 * /	确认编辑
	 * @return [type] [description]
	 */
	public function doedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $result = $this->m->update_list($_POST['id'],$_POST['title'],$_POST['status'],$_POST['main']);
        return redirect($_SESSION['web']['houtai'].'show_xilie?id='.$_SESSION['admin']['data']['shop']['id']);
	}

//ajax----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * /	ajax 批量下架
	 * @return [type] [description]
	 */
	public function xia()
	{
		$reslut = $this->model->xia($this->table,$_POST['id']);
	}

	/**
	 * /	ajax 批量显示
	 * @return [type] [description]
	 */
	public function shang()
	{
		$reslut = $this->model->shang($this->table,$_POST['id']);
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function shang_xia()
	{
		$reslut = $this->model->shang_xia($this->table,$_POST['id']);
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function del_one()
	{
		$reslut = $this->model->del_one($this->table,$_POST['id']);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function del()
	{
		$reslut = $this->model->del($this->table,$_POST['id']);
	}

}


















