<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class CaseController extends Controller
{	
	public $assign = [];


	/**
	 * /	案例首页
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		// echo "<pre />";
		$model = new \App\Model\admin\caselist();
		$info = $model->get();
		// var_dump($info);die;
		$this ->assign['info'] = $info;
		return view('Admin.caselist',$this->assign);
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function ajaxeditcaselist()
	{
		$model = new \App\Model\admin\caselist();
		$id = $_POST['id'];
		$reslut = $model->gengxin($id);
		// var_dump($reslut);die;
	}

	/**
	 * /	全部下架
	 * @return [type] [description]
	 */
	public function alllowercaselist()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\caselist();
		$reslut = $model->alllower($ids);
	}

	/**
	 * /	全部显示
	 * @return [type] [description]
	 */
	public function allshowsizecaselist()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\caselist();
		$reslut = $model->allshowsize($ids);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function alldeletesizecaselist()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\caselist();
		$reslut = $model->alldeletesize($ids);
	}

	/**
	 * /	多图片批量删除
	 * @return [type] [description]
	 */
	public function xianyibutijiao()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\caselistimg();
		$reslut = $model->shanchuxunhuan($ids);
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function ajaxdelcaselist()
	{
		$id = $_POST['id'];
		$model = new \App\Model\admin\caselist();
		$reslut = $model->del($id);
	}

	/**
	 * /	新增页面
	 * @return [type] [description]
	 */
	public function caselistadd()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		return view('Admin.caselistadd');
	}

	/**
	 * /	点击新增
	 * @return [type] [description]
	 */
	public function docaselistadd()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		// echo "<pre />";
		// var_dump($_POST);die;
		// var_dump($_FILES);die;
		if (!empty($_FILES['icon']['name'])) {
			$file = $_FILES["icon"]["tmp_name"];//获取的上传的临时文件  
			$name = $_FILES["icon"]["name"];//获取上传文件的文件名 
			$time = time();
			$time = $time.rand(0,1000000000); 
			$time = md5($time);
			date_default_timezone_set('PRC');
			$shijian = time();
			// var_dump(date('Y-m-d H',$shijian));die;
			$shijian = date('Y-m-d H',$shijian);
			$path="./file/amin/".$shijian.'/'.$time.'/';  
			if (is_dir($path)){    
			    echo "对不起！目录 " . $path . " 已经存在！";  
			    echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			}else{  
			        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
			    $res=mkdir($path,0777,true); //递归创建文件目录  
			    if ($res){  
			        var_dump($res);//boolean true  
			            if (is_dir($path)){    
			                echo "目录 $path 创建成功";  
			                echo $path.$name;  
			                echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			            }  
			    }else{  
			        echo "目录 $path 创建失败";  
			    }  
			}
			$icon = $path.$name;	
		}
		if (!empty($_FILES['touxiang']['name'])) {
			$file = $_FILES["touxiang"]["tmp_name"];//获取的上传的临时文件  
			$name = $_FILES["touxiang"]["name"];//获取上传文件的文件名 
			$time = time();
			$time = $time.rand(0,1000000000); 
			$time = md5($time);
			date_default_timezone_set('PRC');
			$shijian = time();
			// var_dump(date('Y-m-d H',$shijian));die;
			$shijian = date('Y-m-d H',$shijian);
			$path="./file/amin/".$shijian.'/'.$time.'/';  
			if (is_dir($path)){    
			    echo "对不起！目录 " . $path . " 已经存在！";  
			    echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			}else{  
			        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
			    $res=mkdir($path,0777,true); //递归创建文件目录  
			    if ($res){  
			        var_dump($res);//boolean true  
			            if (is_dir($path)){    
			                echo "目录 $path 创建成功";  
			                echo $path.$name;  
			                echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			            }  
			    }else{  
			        echo "目录 $path 创建失败";  
			    }  
			}
			$touxiang = $path.$name;	
		}
		$model = new \App\Model\admin\caselist();
		// echo "<pre />";
		// var_dump($_POST);die;
		$title = $_POST['title'];
		$miaoshu = $_POST['baidu'];
		$main = $_POST['main'];
		$status = $_POST['status'];
		$yanse = $_POST['yanse'];
		$english = $_POST['english'];
		$shejishi = $_POST['shejishi'];
		$jianjie = $_POST['jianjie'];
		if (empty($_FILES['touxiang']['name'])) {
			$touxiang = '';
		}
		if (empty($_FILES['icon']['name'])) {
			$icon = '';
		}
		$result = $model->add($title,$miaoshu,$main,$icon,$status,$yanse,$english,$touxiang,$shejishi,$jianjie);
		$lid = $model->getlast();
		$lastid = $lid[0]['id'];
		if (!empty($_FILES['duotu']['name'])) {
			$file = $_FILES["duotu"]["tmp_name"];//获取的上传的临时文件  
			$name = $_FILES["duotu"]["name"];//获取上传文件的文件名 
			// var_dump($name);die;
			$modelimg = new \App\Model\admin\caselistimg();
			foreach ($name as $k => $v) {
				// var_dump($file[$k]);die;
				$time = time();
				$time = $time.rand(0,1000000000); 
				$time = md5($time);
				date_default_timezone_set('PRC');
				$shijian = time();
				// var_dump(date('Y-m-d H',$shijian));die;
				$shijian = date('Y-m-d H',$shijian);
				$path="./file/amin/".$shijian.'/'.$time.'/';  
				if (is_dir($path)){    
				    echo "对不起！目录 " . $path . " 已经存在！";  
				    echo  move_uploaded_file($file[$k] ,$path.$v)? 'ok' : 'false';  
				    $cid = $lastid;
				    $icon = $icon = $path.$v;
				    $result = $modelimg->add($cid,$icon);
				}else{  
				        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
				    $res=mkdir($path,0777,true); //递归创建文件目录  
				    if ($res){  
				        var_dump($res);//boolean true  
				            if (is_dir($path)){    
				                echo "目录 $path 创建成功";  
				                echo $path.$v;  
				                echo  move_uploaded_file($file[$k] ,$path.$v)? 'ok' : 'false';  
				                $cid = $lastid;
							    $icon = $icon = $path.$v;
							    $result = $modelimg->add($cid,$icon);
				            }  
				    }else{  
				        echo "目录 $path 创建失败";  
				    }  
				}	
			}
		}
		return redirect("admin_case");
	}

	/**
	 * /	编辑页面
	 * @return [type] [description]
	 */
	public function caselistedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		// echo "<pre />";
		$id = $_GET['id'];
		$model = new \App\Model\admin\caselist();
		$modelimg = new \App\Model\admin\caselistimg();
		$info = $model->find($id);
		$img = $modelimg->huoqu($id);
		$this ->assign['info'] = $info;
		$this ->assign['img'] = $img;
		// var_dump($img);die;
		return view('Admin.caselistedit',$this->assign);
	}

	/**
	 * /	点击编辑
	 * @return [type] [description]
	 */
	public function docaselistedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		$id = $_POST['id'];
		if (!empty($_FILES['icon']['name'])) {
			$file = $_FILES["icon"]["tmp_name"];//获取的上传的临时文件  
			$name = $_FILES["icon"]["name"];//获取上传文件的文件名 
			$time = time();
			$time = $time.rand(0,1000000000); 
			$time = md5($time);
			date_default_timezone_set('PRC');
			$shijian = time();
			// var_dump(date('Y-m-d H',$shijian));die;
			$shijian = date('Y-m-d H',$shijian);
			$path="./file/amin/".$shijian.'/'.$time.'/';  
			if (is_dir($path)){    
			    echo "对不起！目录 " . $path . " 已经存在！";  
			    echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			}else{  
			        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
			    $res=mkdir($path,0777,true); //递归创建文件目录  
			    if ($res){  
			        var_dump($res);//boolean true  
			            if (is_dir($path)){    
			                echo "目录 $path 创建成功";  
			                echo $path.$name;  
			                echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			            }  
			    }else{  
			        echo "目录 $path 创建失败";  
			    }  
			}
			$icon = $path.$name;	
		}
		if (!empty($_FILES['touxiang']['name'])) {
			$file = $_FILES["touxiang"]["tmp_name"];//获取的上传的临时文件  
			$name = $_FILES["touxiang"]["name"];//获取上传文件的文件名 
			$time = time();
			$time = $time.rand(0,1000000000); 
			$time = md5($time);
			date_default_timezone_set('PRC');
			$shijian = time();
			// var_dump(date('Y-m-d H',$shijian));die;
			$shijian = date('Y-m-d H',$shijian);
			$path="./file/amin/".$shijian.'/'.$time.'/';  
			if (is_dir($path)){    
			    echo "对不起！目录 " . $path . " 已经存在！";  
			    echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			}else{  
			        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
			    $res=mkdir($path,0777,true); //递归创建文件目录  
			    if ($res){  
			        var_dump($res);//boolean true  
			            if (is_dir($path)){    
			                echo "目录 $path 创建成功";  
			                echo $path.$name;  
			                echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			            }  
			    }else{  
			        echo "目录 $path 创建失败";  
			    }  
			}
			$touxiang = $path.$name;	
		}
		$model = new \App\Model\admin\caselist();
		// echo "<pre />";
		// var_dump($_POST);die;
		$title = $_POST['title'];
		$miaoshu = $_POST['baidu'];
		$main = $_POST['main'];
		$status = $_POST['status'];
		$yanse = $_POST['yanse'];
		$english = $_POST['english'];
		$shejishi = $_POST['shejishi'];
		$jianjie = $_POST['jianjie'];
		if (empty($_FILES['icon']['name'])) {
			$chacha = $model->find($id);
			$icon = $chacha['icon'];
		}
		if (empty($_FILES['touxiang']['name'])) {
			$touxiangimg = $model->find($id);
			$touxiang = $touxiangimg['touxiang'];
		}
		// var_dump($touxiang);die;
		if ($_POST['shanchu'] == 'icon') {
			$icon = '';
		}
		if ($_POST['shanchu'] == 'touxiang') {
			$touxiang = '';
		}
		$result = $model->updatenew($id,$title,$miaoshu,$main,$icon,$status,$yanse,$english,$touxiang,$shejishi,$jianjie);
		$lid = $model->getlast();
		$lastid = $lid[0]['id'];
		if (!empty($_FILES['duotu']['name'][0])) {
			$file = $_FILES["duotu"]["tmp_name"];//获取的上传的临时文件  
			$name = $_FILES["duotu"]["name"];//获取上传文件的文件名 
			// var_dump($name);die;
			$modelimg = new \App\Model\admin\caselistimg();
			foreach ($name as $k => $v) {
				// var_dump($file[$k]);die;
				$time = time();
				$time = $time.rand(0,1000000000); 
				$time = md5($time);
				date_default_timezone_set('PRC');
				$shijian = time();
				// var_dump(date('Y-m-d H',$shijian));die;
				$shijian = date('Y-m-d H',$shijian);
				$path="./file/amin/".$shijian.'/'.$time.'/';  
				if (is_dir($path)){    
				    echo "对不起！目录 " . $path . " 已经存在！";  
				    echo  move_uploaded_file($file[$k] ,$path.$v)? 'ok' : 'false';  
				    $cid = $lastid;
				    $icon = $icon = $path.$v;
				    $result = $modelimg->add($cid,$icon);
				}else{  
				        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
				    $res=mkdir($path,0777,true); //递归创建文件目录  
				    if ($res){  
				        var_dump($res);//boolean true  
				            if (is_dir($path)){    
				                echo "目录 $path 创建成功";  
				                echo $path.$v;  
				                echo  move_uploaded_file($file[$k] ,$path.$v)? 'ok' : 'false';  
				                $cid = $lastid;
							    $icon = $icon = $path.$v;
							    $result = $modelimg->add($cid,$icon);
				            }  
				    }else{  
				        echo "目录 $path 创建失败";  
				    }  
				}	
			}
		}
		return redirect("admin_case");
	}
}


















