<?php
namespace App\Http\Controllers\Admin\Shop_list;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{	
	public $assign = [];
	public $table = 'chanpin';
	public $pinpai = 'shop';
	public $xilie = 'xilie';
	public $fenlei = 'product';
	public $model = '';
	public $m = '';

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = new \App\Model\M();
       $this->m = new \App\Model\admin\Chanpin();
       return true;
	}

	/**
	 * /	产品列表
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $info = $this->model->paginate($this->table);
        $this ->assign['info'] = $info;
        // echo "<pre />";
        // var_dump($info);die;
		return view('Admin.Shop_list.index',$this->assign);
	}

	/**
	 * /	新增
	 * @return [type] [description]
	 */
	public function add()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $pinpai = $this->model->get($this->pinpai);
        $this ->assign['pinpai'] = $pinpai;
		return view('Admin.Shop_list.add',$this->assign);
	}

	/**
	 * /	确认新增
	 * @return [type] [description]
	 */
	public function doadd()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
		if ($_POST['pinpai_id'] == 0) {
			echo "<script>alert('请选择品牌!');location.href='".$_SERVER["HTTP_REFERER"]."';</script>";
			return;
		}
		if ($_POST['xilie_id'] == 0) {
			echo "<script>alert('请选择系列!');location.href='".$_SERVER["HTTP_REFERER"]."';</script>";
			return;
		}
		if ($_POST['fenlei_id'] == 0) {
			echo "<script>alert('请选择分类!');location.href='".$_SERVER["HTTP_REFERER"]."';</script>";
			return;
		}
        if (!empty($_FILES['icon']['name'])) {
        	$icon = $this->model->uploads('icon');
        }else{
        	$icon = '';
        }
        $result = $this->m->add($_POST['xinghao'],$_POST['shuoming'],$_POST['guige'],$_POST['guige_shuzi'],$_POST['yongcai'],$_POST['jiage'],$_POST['pinpai_id'],$_POST['xilie_id'],$_POST['fenlei_id'],$_POST['status'],$icon,$_POST['main']);
        return redirect($_SESSION['web']['houtai'].'shop_list');
	}

	/**
	 * /	编辑
	 * @return [type] [description]
	 */
	public function edit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $info = $this->model->find($this->table,'id',$_GET['id']);
        $pinpai = $this->model->get($this->pinpai);
        $pinpai_title = $this->model->find($this->pinpai,'id',$info->pinpai_id);
        $xilie_title = $this->model->find($this->xilie,'id',$info->xilie_id);
        $fenlei_title = $this->model->find($this->fenlei,'id',$info->fenlei_id);
        $this ->assign['pinpai_title'] = $pinpai_title;
        $this ->assign['xilie_title'] = $xilie_title;
        $this ->assign['fenlei_title'] = $fenlei_title;
        $this ->assign['pinpai'] = $pinpai;
        $this ->assign['info'] = $info;
		return view('Admin.Shop_list.edit',$this->assign);
	}

	/**
	 * /	确认编辑
	 * @return [type] [description]
	 */
	public function doedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
		if ($_POST['pinpai_id'] == 0) {
			echo "<script>alert('请选择品牌!');location.href='".$_SERVER["HTTP_REFERER"]."';</script>";
			return;
		}
		if ($_POST['xilie_id'] == 0) {
			echo "<script>alert('请选择系列!');location.href='".$_SERVER["HTTP_REFERER"]."';</script>";
			return;
		}
		if ($_POST['fenlei_id'] == 0) {
			echo "<script>alert('请选择分类!');location.href='".$_SERVER["HTTP_REFERER"]."';</script>";
			return;
		}
        $file = $this->model->find($this->table,'id',$_POST['id'])->icon;
        if (!empty($_FILES['icon']['name'])) {
        	$icon = $this->model->uploads('icon');
        	if(file_exists($file)){
		        unlink($file);
		    }
        }else{
        	$icon = $file;
        }
        if ($_POST['shanchu'] == 'shan') {
        	$icon = '';
        }
        $result = $this->m->update_list($_POST['id'],$_POST['xinghao'],$_POST['shuoming'],$_POST['guige'],$_POST['guige_shuzi'],$_POST['yongcai'],$_POST['jiage'],$_POST['pinpai_id'],$_POST['xilie_id'],$_POST['fenlei_id'],$_POST['status'],$icon,$_POST['main']);
        return redirect($_SESSION['web']['houtai'].'shop_list');
	}

//ajax----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * /	ajax 批量下架
	 * @return [type] [description]
	 */
	public function xia()
	{
		$reslut = $this->model->xia($this->table,$_POST['id']);
	}

	/**
	 * /	ajax 批量显示
	 * @return [type] [description]
	 */
	public function shang()
	{
		$reslut = $this->model->shang($this->table,$_POST['id']);
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function shang_xia()
	{
		$reslut = $this->model->shang_xia($this->table,$_POST['id']);
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function del_one()
	{
		$reslut = $this->model->del_one($this->table,$_POST['id']);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function del()
	{
		$reslut = $this->model->del($this->table,$_POST['id']);
	}

	/**
	 * /	根据品牌变化查看系列
	 * @return [type] [description]
	 */
	public function pinpai_xilie()
	{
		$result = $this->model->find_all($this->xilie,'pinpai_id',$_POST['id']);
		if (count($result) == 0) {
			return 0;
		}else{
			foreach ($result as $k => $v) {
				$data[$k]['id'] = $v->id;
				$data[$k]['title'] = $v->title;
			}
			return $data;
		}
	}

	/**
	 * /	根据系列变化查看分类
	 * @return [type] [description]
	 */
	public function xilie_fenlei()
	{
		$result = $this->model->find_all($this->fenlei,'xilie_id',$_POST['id']);
		if (count($result) == 0) {
			return 0;
		}else{
			foreach ($result as $k => $v) {
				$data[$k]['id'] = $v->id;
				$data[$k]['title'] = $v->xinghao;
			}
			return $data;
		}
	}

}


















