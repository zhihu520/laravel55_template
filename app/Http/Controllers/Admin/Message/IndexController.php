<?php
namespace App\Http\Controllers\Admin\Message;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{	
	public $assign = [];
	public $table = 'leave';
	public $model = '';
	public $m = '';

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = new \App\Model\M();
       // $this->m = new \App\Model\admin\Wd_jxsfw();
       return true;
	}

	/**
	 * /	问答管理/钢琴管理首页
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $info = $this->model->paginate($this->table);
        $this ->assign['info'] = $info;
		return view('Admin.Message.index',$this->assign);
	}

	/**
	 * /	问答管理/钢琴管理首页
	 * @return [type] [description]
	 */
	public function chakan()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $info = $this->model->find($this->table,'id',$_GET['id']);
        $this ->assign['info'] = $info;
		return view('Admin.Message.chakan',$this->assign);
	}

//ajax----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function del_one()
	{
		$reslut = $this->model->del_one($this->table,$_POST['id']);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function del()
	{
		$reslut = $this->model->del($this->table,$_POST['id']);
	}

}


















