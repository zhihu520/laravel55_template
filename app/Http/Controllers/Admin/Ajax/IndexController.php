<?php
namespace App\Http\Controllers\Admin\Ajax;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{	
	public $assign = [];

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = $_SESSION['M'];
       return true;
	}

	/**
	 * /	ajax 批量下架
	 * @return [type] [description]
	 */
	public function lower()
	{
		$reslut = $this->model->lower($_POST['table'],$_POST['id']);
	}

	/**
	 * /	ajax 批量显示
	 * @return [type] [description]
	 */
	public function show()
	{
		$reslut = $this->model->show($_POST['table'],$_POST['id']);
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function lower_show()
	{
		$reslut = $this->model->lower_show($_POST['table'],$_POST['id']);
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function del_one()
	{
		$reslut = $this->model->del_one($_POST['table'],$_POST['id']);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function del()
	{
		$reslut = $this->model->del($_POST['table'],$_POST['id']);
	}

}


















