<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class UserController extends Controller
{	
	public $assign = [];


	/**
	 * /	角色列表
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		// echo "<pre />";
		$model = new \App\Model\admin\user();
		$modelf = new \App\Model\admin\group();
		$info = $model->get();
		$fenzu = $modelf->get();
		$this ->assign['info'] = $info;
		$this ->assign['fenzu'] = $fenzu;
		return view('Admin.user',$this->assign);
	}

	/**
	 * /	ajax用户编辑
	 * @return [type] [description]
	 */
	public function userbianji()
	{
		$id = $_POST['id'];
		$model = new \App\Model\admin\user();
		$info = $model->find($id);
		$data['username'] = $info['username'];
		$data['fenzu'] = $info['fenzu'];
		$data['status'] = $info['status'];
		return $data;
	}

	/**
	 * /	用户 新增/编辑
	 * @return [type] [description]
	 */
	public function useraddedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		// echo "<pre />";
		$model = new \App\Model\admin\user();
		$modelf = new \App\Model\admin\group();
		$addoredit = $_POST['addoredit'];
		if ($addoredit == 'add') {
			$gid = $_POST['gid'];
			$username = $_POST['username'];
			$password = md5($_POST['password']);
			$status = $_POST['status'];
			$yongdefenzu = $modelf->find($gid);
			$fenzu = $yongdefenzu['title'];
			$result = $model->add($gid,$username,$password,$status,$fenzu);
		}else{
			$id = $_POST['id'];
			$gid = $_POST['gid'];
			// var_dump($_POST);die;
			if (empty($_POST['password'])) {
				$yongdemima = $model->find($id);
				$_POST['password'] = $yongdemima['password'];
			}
			$username = $_POST['username'];
			$password = md5($_POST['password']);
			$status = $_POST['status'];
			if ($gid == '0') {
				$fenzu = '';
			}else{
				$yongdefenzu = $modelf->find($gid);
				$fenzu = $yongdefenzu['title'];
			}
			$result = $model->updateuser($id,$gid,$username,$password,$status,$fenzu);
		}
		return redirect("user");
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function alldeletesizeuser()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\user();
		$reslut = $model->alldeletesize($ids);
	}

	/**
	 * /	全部下架
	 * @return [type] [description]
	 */
	public function allloweruser()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\user();
		$reslut = $model->alllower($ids);
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function ajaxedituser()
	{
		$model = new \App\Model\admin\user();
		$id = $_POST['id'];
		$reslut = $model->gengxin($id);
		// var_dump($reslut);die;
	}

	/**
	 * /	全部显示
	 * @return [type] [description]
	 */
	public function allshowsizeuser()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\user();
		$reslut = $model->allshowsize($ids);
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function ajaxdeluser()
	{
		$id = $_POST['id'];
		$model = new \App\Model\admin\user();
		$reslut = $model->del($id);
	}

}


















