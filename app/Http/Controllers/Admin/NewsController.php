<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Storage;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{	
	public $assign = [];

	/**
	 * /	新闻
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		// echo "<pre />";
		$model = new \App\Model\admin\news();
		$info = $model->get();
		// var_dump($info);die;
		$this ->assign['info'] = $info;
		return view('Admin.news',$this->assign);
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function ajaxedit()
	{
		$model = new \App\Model\admin\news();
		$id = $_POST['id'];
		$reslut = $model->gengxin($id);
		// var_dump($reslut);die;
	}

	/**
	 * /	全部下架
	 * @return [type] [description]
	 */
	public function alllower()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\news();
		$reslut = $model->alllower($ids);
	}

	/**
	 * /	全部显示
	 * @return [type] [description]
	 */
	public function allshowsize()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\news();
		$reslut = $model->allshowsize($ids);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function alldeletesize()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\news();
		$reslut = $model->alldeletesize($ids);
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function ajaxdel()
	{
		$id = $_POST['id'];
		$model = new \App\Model\admin\news();
		$reslut = $model->del($id);
	}

	/**
	 * /	编辑
	 * @return [type] [description]
	 */
	public function newsajaxedit()
	{
		$id = $_POST['id'];
		$model = new \App\Model\admin\news();
		$reslut = $model->show($id);
		return $reslut;
	}

	/**
	 * /	新增页面
	 * @return [type] [description]
	 */
	public function newadd()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		return view('Admin.newadd');
	}

	/**
	 * /	点击新增
	 * @return [type] [description]
	 */
	public function add()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		// echo "<pre />";
		// var_dump($_POST);die;
		// var_dump($_FILES);die;
		if (!empty($_FILES['icon']['name'])) {
			$file = $_FILES["icon"]["tmp_name"];//获取的上传的临时文件  
			$name = $_FILES["icon"]["name"];//获取上传文件的文件名 
			$time = time();
			$time = $time.rand(0,1000000000); 
			$time = md5($time);
			date_default_timezone_set('PRC');
			$shijian = time();
			// var_dump(date('Y-m-d H',$shijian));die;
			$shijian = date('Y-m-d H',$shijian);
			$path="./file/amin/".$shijian.'/'.$time.'/';  
			if (is_dir($path)){    
			    echo "对不起！目录 " . $path . " 已经存在！";  
			    echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			}else{  
			        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
			    $res=mkdir($path,0777,true); //递归创建文件目录  
			    if ($res){  
			        var_dump($res);//boolean true  
			            if (is_dir($path)){    
			                echo "目录 $path 创建成功";  
			                echo $path.$name;  
			                echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			            }  
			    }else{  
			        echo "目录 $path 创建失败";  
			    }  
			}	
		}
		$model = new \App\Model\admin\news();
		// echo "<pre />";
		// var_dump($_POST);die;
		$title = $_POST['title'];
		$miaoshu = $_POST['miaoshu'];
		$main = $_POST['baidu'];
		$status = $_POST['status'];
		if (empty($_FILES['icon']['name'])) {
			$icon = '';
		}else{
			$icon = $path.$name;
		}
		$result = $model->add($title,$miaoshu,$main,$icon,$status);
		return redirect("news");
	}
	
	/**
	 * /	编辑页面
	 * @return [type] [description]
	 */
	public function newedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		// echo "<pre />";
		$id = $_GET['id'];
		$model = new \App\Model\admin\news();
		$info = $model->find($id);
		$this ->assign['info'] = $info;
		return view('Admin.newedit',$this->assign);
	}

	/**
	 * /	点击编辑
	 * @return [type] [description]
	 */
	public function edit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		$id = $_POST['id'];
		if (!empty($_FILES['icon']['name'])) {
			$file = $_FILES["icon"]["tmp_name"];//获取的上传的临时文件  
			$name = $_FILES["icon"]["name"];//获取上传文件的文件名 
			$time = time();
			$time = $time.rand(0,1000000000); 
			$time = md5($time);
			date_default_timezone_set('PRC');
			$shijian = time();
			// var_dump(date('Y-m-d H',$shijian));die;
			$shijian = date('Y-m-d H',$shijian);
			$path="./file/amin/".$shijian.'/'.$time.'/';  
			if (is_dir($path)){    
			    echo "对不起！目录 " . $path . " 已经存在！";  
			    echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			}else{  
			        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
			    $res=mkdir($path,0777,true); //递归创建文件目录  
			    if ($res){  
			        var_dump($res);//boolean true  
			            if (is_dir($path)){    
			                echo "目录 $path 创建成功";  
			                echo $path.$name;  
			                echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			            }  
			    }else{  
			        echo "目录 $path 创建失败";  
			    }  
			}
			$icon = $path.$name;	
		}
		if (!empty($_FILES['iconcon']['name'])) {
			$file = $_FILES["iconcon"]["tmp_name"];//获取的上传的临时文件  
			$name = $_FILES["iconcon"]["name"];//获取上传文件的文件名 
			$time = time();
			$time = $time.rand(0,1000000000); 
			$time = md5($time);
			date_default_timezone_set('PRC');
			$shijian = time();
			// var_dump(date('Y-m-d H',$shijian));die;
			$shijian = date('Y-m-d H',$shijian);
			$path="./file/amin/".$shijian.'/'.$time.'/';  
			if (is_dir($path)){    
			    echo "对不起！目录 " . $path . " 已经存在！";  
			    echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			}else{  
			        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
			    $res=mkdir($path,0777,true); //递归创建文件目录  
			    if ($res){  
			        var_dump($res);//boolean true  
			            if (is_dir($path)){    
			                echo "目录 $path 创建成功";  
			                echo $path.$name;  
			                echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			            }  
			    }else{  
			        echo "目录 $path 创建失败";  
			    }  
			}
			$iconcon = $path.$name;	
		}
		$model = new \App\Model\admin\news();
		// echo "<pre />";
		// var_dump($_POST);die;
		$title = $_POST['title'];
		$miaoshu = $_POST['miaoshu'];
		$main = $_POST['baidu'];
		$status = $_POST['status'];
		if (empty($_FILES['icon']['name'])) {
			$data = $model->find($id);
			$icon = $data['icon'];
		}
		if (empty($_FILES['iconcon']['name'])) {
			$data = $model->find($id);
			$iconcon = $data['iconcon'];
		}
		if ($_POST['shanchu'] == 'icon') {
			$icon = '';
		}
		if ($_POST['shanchu'] == 'neiye') {
			$iconcon = '';
		}
		$result = $model->updatenew($id,$title,$miaoshu,$main,$icon,$iconcon,$status);
		return redirect("news");
	}
	
}


















