<?php
namespace App\Http\Controllers\Admin\About;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{	
	public $assign = [];

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = $_SESSION['M'];
       if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login")->send();
        }
       return true;
	}

	/**
	 * /	公司简介
	 * @return [type] [description]
	 */
	public function index()
	{
		return view('Admin.About.index');
	}

	/**
	 * /	确认修改
	 * @return [type] [description]
	 */
	public function doabout()
	{
        $data['about'] = $_POST['about'];
        $result = $this->model->edit('system',1,$data);
        return redirect($_SESSION['web']['houtai'].'about');
	}

    /**
     * /    公司信息
     * @return [type] [description]
     */
    public function info()
    {
        return view('Admin.About.info');
    }

    /**
     * /    确认修改
     * @return [type] [description]
     */
    public function doinfo()
    {
        $result = $this->model->edit('system',1,$_POST);
        return redirect($_SESSION['web']['houtai'].'info');
    }
}


















