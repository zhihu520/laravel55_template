<?php
namespace App\Http\Controllers\Admin\Login;

use Illuminate\Http\Request;
use Storage;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Model\M;
use App\Model\admin\User;
use App\Model\admin\Group;

class IndexController extends Controller
{	
	public $assign = [];
	public $model = '';
	public $user = '';
	public $group = '';

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
		$this->model = new M();
		$this->user = new User();
		$this->group = new Group();
		return true;
	}
	
	/**
	 * /	登录页面
	 * @return [type] [description]
	 */
	public function index()
	{
		return view('Admin.Login.index');
	}

	/**
	 * /	提交登录
	 * @return [type] [description]
	 */
	public function login()
	{
		$username = $_POST['username'];
		$password = $_POST['password'];
		$result = $this->user->showname($username);
		if (empty($result['username'])) {
			$data = '0';
			return $data;
		}
		// var_dump($result['status']);die;
		if ($result['status'] == 'false') {
			$data = '0';
			return $data;
		}
		$gid = $result['gid'];
		$menu_auth = $this->group->menu_auth($gid);
		// var_dump($menu_auth);die;
		// var_dump(count($menu_auth));die;
		for ($j=0; $j < 1000; $j++) { 
			setcookie("admin[menu_auth][$j]",'', time()-1);
		}
		for ($i=0; $i < count($menu_auth); $i++) { 
			setcookie("admin[menu_auth][$i]",$menu_auth[$i], time()+3600*24);
		}
		$data['password'] = '1';
		$data['status'] = $result['status'];
		if (empty($password)) {
			$data['password'] = '0';
			return $data;
		}
		if ($result['password'] !== md5($password)) {
			$data['password'] = '0';
			return $data;
		}
		setcookie("admin[gid]",$gid, time()+3600*24);
		setcookie("admin[username]",$result['username'], time()+3600*24);
		setcookie("admin[create_time]",time(), time()+3600*24);
		return $data;
	}

	/**
	 * /	登陆成功 跳转页面
	 * @return [type] [description]
	 */
	public function login_true()
	{
		setcookie("TestCookie",100, time()+3600);
		return redirect("admin");
	}

	/**
	 * /	退出登录
	 * @return [type] [description]
	 */
	public function loginout()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        setcookie("TestCookie",100, time()-1);
        setcookie("admin",100, time()-1);
		return redirect("admin");
	}
}


















