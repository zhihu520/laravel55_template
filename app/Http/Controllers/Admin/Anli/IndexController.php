<?php
namespace App\Http\Controllers\Admin\Anli;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{	
	public $assign = [];

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = $_SESSION['M'];
       if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login")->send();
        }
       return true;
	}

	/**
	 * /	案例列表
	 * @return [type] [description]
	 */
	public function index()
	{
        $info = $this->model->paginate('anli');
        $this ->assign['info'] = $info;
		return view('Admin.Anli.index',$this->assign);
	}

	/**
	 * /	新增
	 * @return [type] [description]
	 */
	public function add()
	{
        $info = $this->model->get('anli_group');
        $this ->assign['info'] = $info;
		return view('Admin.Anli.add',$this->assign);
	}

	/**
	 * /	确认新增
	 * @return [type] [description]
	 */
	public function doadd()
	{
        if (!empty($_FILES['icon']['name'])) {
        	// $icon = $this->model->uploads('icon');
            $icon = $this->model->qiniuyun($_FILES['icon']['tmp_name']);
        }else{
        	$icon = '';
        }
        $data['title'] = $_POST['title'];
        $data['miaoshu'] = $_POST['miaoshu'];
        $data['gid'] = $_POST['news_group'];
        $data['icon'] = $icon;
        $data['create_time'] = time();
        $data['update_time'] = time();
        $id = $this->model->add('anli',$data);
        return redirect($_SESSION['web']['houtai'].'case');
	}

	/**
	 * /	编辑
	 * @return [type] [description]
	 */
	public function edit()
	{
        $info = $this->model->find('anli','id',$_GET['id']);
        $data = $this->model->get('anli_group');
        $this ->assign['info'] = $info;
        $this ->assign['data'] = $data;
		return view('Admin.Anli.edit',$this->assign);
	}

	/**
	 * /	确认编辑
	 * @return [type] [description]
	 */
	public function doedit()
	{
        if (!empty($_FILES['icon']['name'])) {
        	// $icon = $this->model->uploads('icon');
            $icon = $this->model->qiniuyun($_FILES['icon']['tmp_name']);
        }else{
        	$icon = $this->model->find('anli','id',$_POST['id'])->icon;
        }
        $data['title'] = $_POST['title'];
        $data['miaoshu'] = $_POST['miaoshu'];
        $data['gid'] = $_POST['news_group'];
        $data['status'] = $_POST['status'];
        $data['icon'] = $icon;
        $data['update_time'] = time();
        $result = $this->model->edit('anli',$_POST['id'],$data);
        return redirect($_SESSION['web']['houtai'].'case');
	}
}


















