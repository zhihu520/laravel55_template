<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class TeamController extends Controller
{	
	public $assign = [];


	/**
	 * /	首页
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		// echo "<pre />";
		$model = new \App\Model\admin\team();
		$info = $model->get();
		// var_dump($info);die;
		$this ->assign['info'] = $info;
		return view('Admin.team',$this->assign);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function alldeletesizeteam()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\team();
		$reslut = $model->alldeletesize($ids);
	}

	/**
	 * /	全部下架
	 * @return [type] [description]
	 */
	public function alllowerteam()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\team();
		$reslut = $model->alllower($ids);
	}

	/**
	 * /	全部显示
	 * @return [type] [description]
	 */
	public function allshowsizeteam()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\team();
		$reslut = $model->allshowsize($ids);
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function ajaxeditteam()
	{
		$model = new \App\Model\admin\team();
		$id = $_POST['id'];
		$reslut = $model->gengxin($id);
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function ajaxdelteam()
	{
		$id = $_POST['id'];
		$model = new \App\Model\admin\team();
		$reslut = $model->del($id);
	}

	/**
	 * /	团队新增页面
	 * @return [type] [description]
	 */
	public function teamadd()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		return view('Admin.teamadd');
	}

	/**
	 * /	点击新增
	 * @return [type] [description]
	 */
	public function doteamadd()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		if (!empty($_FILES['icon']['name'])) {
			$file = $_FILES["icon"]["tmp_name"];//获取的上传的临时文件  
			$name = $_FILES["icon"]["name"];//获取上传文件的文件名 
			$time = time();
			$time = $time.rand(0,1000000000); 
			$time = md5($time);
			date_default_timezone_set('PRC');
			$shijian = time();
			// var_dump(date('Y-m-d H',$shijian));die;
			$shijian = date('Y-m-d H',$shijian);
			$path="./file/amin/".$shijian.'/'.$time.'/';  
			if (is_dir($path)){    
			    echo "对不起！目录 " . $path . " 已经存在！";  
			    echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			}else{  
			        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
			    $res=mkdir($path,0777,true); //递归创建文件目录  
			    if ($res){  
			        var_dump($res);//boolean true  
			            if (is_dir($path)){    
			                echo "目录 $path 创建成功";  
			                echo $path.$name;  
			                echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			            }  
			    }else{  
			        echo "目录 $path 创建失败";  
			    }  
			}	
		}
		$model = new \App\Model\admin\team();
		// echo "<pre />";
		// var_dump($_POST);die;
		$title = $_POST['title'];
		$miaoshu = $_POST['miaoshu'];
		$status = $_POST['status'];
		if (empty($_FILES['icon']['name'])) {
			$icon = '';
		}else{
			$icon = $path.$name;
		}
		$result = $model->add($title,$miaoshu,$status,$icon);
		return redirect("admin_team");
	}

	/**
	 * /	编辑页面
	 * @return [type] [description]
	 */
	public function teamedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		// echo "<pre />";
		$id = $_GET['id'];
		$model = new \App\Model\admin\team();
		$data = $info = $model->find($id);
		$this ->assign['info'] = $info;
		return view('Admin.teamedit',$this->assign);
	}

	/**
	 * /	点击编辑
	 * @return [type] [description]
	 */
	public function doteamedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		$id = $_POST['id'];
		if (!empty($_FILES['icon']['name'])) {
			$file = $_FILES["icon"]["tmp_name"];//获取的上传的临时文件  
			$name = $_FILES["icon"]["name"];//获取上传文件的文件名 
			$time = time();
			$time = $time.rand(0,1000000000); 
			$time = md5($time);
			date_default_timezone_set('PRC');
			$shijian = time();
			// var_dump(date('Y-m-d H',$shijian));die;
			$shijian = date('Y-m-d H',$shijian);
			$path="./file/amin/".$shijian.'/'.$time.'/';  
			if (is_dir($path)){    
			    echo "对不起！目录 " . $path . " 已经存在！";  
			    echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			}else{  
			        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
			    $res=mkdir($path,0777,true); //递归创建文件目录  
			    if ($res){  
			        var_dump($res);//boolean true  
			            if (is_dir($path)){    
			                echo "目录 $path 创建成功";  
			                echo $path.$name;  
			                echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
			            }  
			    }else{  
			        echo "目录 $path 创建失败";  
			    }  
			}	
		}
		$model = new \App\Model\admin\team();
		// echo "<pre />";
		// var_dump($_POST);die;
		$title = $_POST['title'];
		$miaoshu = $_POST['miaoshu'];
		$status = $_POST['status'];
		if (empty($_FILES['icon']['name'])) {
			$data = $model->find($id);
			$icon = $data['icon'];
		}else{
			$icon = $path.$name;
		}
		if ($_POST['shanchu'] == 'shan') {
			$icon = '';
		}
		$result = $model->gaidongxi($id,$title,$miaoshu,$status,$icon);
		return redirect("admin_team");
	}

}


















