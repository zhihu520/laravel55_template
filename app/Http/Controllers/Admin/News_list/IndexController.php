<?php
namespace App\Http\Controllers\Admin\News_list;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{	
	public $assign = [];

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = $_SESSION['M'];
       if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login")->send();
        }
       return true;
	}

	/**
	 * /	新闻列表
	 * @return [type] [description]
	 */
	public function index()
	{
        $info = $this->model->paginate('news_list');
        $this ->assign['info'] = $info;
		return view('Admin.News_list.index',$this->assign);
	}

	/**
	 * /	新增
	 * @return [type] [description]
	 */
	public function add()
	{
        $info = $this->model->get('news_group');
        $this ->assign['info'] = $info;
		return view('Admin.News_list.add',$this->assign);
	}

	/**
	 * /	确认新增
	 * @return [type] [description]
	 */
	public function doadd()
	{
        if (!empty($_FILES['icon']['name'])) {
            // $icon = $this->model->uploads('icon');
        	$icon = $this->model->qiniuyun($_FILES['icon']['tmp_name']);
        }else{
        	$icon = '';
        }
        $data['title'] = $_POST['title'];
        $data['miaoshu'] = $_POST['miaoshu'];
        $data['gid'] = $_POST['news_group'];
        $data['icon'] = $icon;
        $id = $this->model->add('news_list',$data);
        return redirect($_SESSION['web']['houtai'].'news_list');
	}

	/**
	 * /	编辑
	 * @return [type] [description]
	 */
	public function edit()
	{
        $info = $this->model->find('news_list','id',$_GET['id']);
        $data = $this->model->get('news_group');
        $this ->assign['info'] = $info;
        $this ->assign['data'] = $data;
		return view('Admin.News_list.edit',$this->assign);
	}

	/**
	 * /	确认编辑
	 * @return [type] [description]
	 */
	public function doedit()
	{
        if (!empty($_FILES['icon']['name'])) {
        	// $icon = $this->model->uploads('icon');
            $icon = $this->model->qiniuyun($_FILES['icon']['tmp_name']);
        }else{
        	$icon = $this->model->find('news_list','id',$_POST['id'])->icon;
        }
        $data['title'] = $_POST['title'];
        $data['miaoshu'] = $_POST['miaoshu'];
        $data['gid'] = $_POST['news_group'];
        $data['status'] = $_POST['status'];
        $data['icon'] = $icon;
        $result = $this->model->edit('news_list',$_POST['id'],$data);
        return redirect($_SESSION['web']['houtai'].'news_list');
	}
}


















