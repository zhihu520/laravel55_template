<?php
namespace App\Http\Controllers\Admin\Banner;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{	
	public $assign = [];
	public $table = 'banner';
	public $sj = 'sj_banner';
	public $model = '';
	public $m = '';
	public $d = '';

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = new \App\Model\M();
       $this->m = new \App\Model\admin\Banner();
       $this->d = new \App\Model\admin\SJ_Banner();
       return true;
	}

	/**
	 * /	广告列表
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $info = $this->model->paginate($this->table);
        $this ->assign['info'] = $info;
		return view('Admin.Banner.index',$this->assign);
	}

	/**
	 * /	手机广告列表
	 * @return [type] [description]
	 */
	public function sj_index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $info = $this->model->paginate($this->sj);
        $this ->assign['info'] = $info;
		return view('Admin.Banner.sj_index',$this->assign);
	}

	/**
	 * /	新增
	 * @return [type] [description]
	 */
	public function add()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
		return view('Admin.Banner.add');
	}

	/**
	 * /	手机新增
	 * @return [type] [description]
	 */
	public function sj_add()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
		return view('Admin.Banner.sj_add');
	}

	/**
	 * /	确认新增
	 * @return [type] [description]
	 */
	public function doadd()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        if (!empty($_FILES['icon']['name'])) {
        	$icon = $this->model->uploads('icon');
        }else{
        	$icon = '';
        }
        $result = $this->m->add($icon,$_POST['status'],$_POST['lianjie']);
        return redirect($_SESSION['web']['houtai'].'banner');
	}

	/**
	 * /	sj_确认新增
	 * @return [type] [description]
	 */
	public function sj_doadd()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        if (!empty($_FILES['icon']['name'])) {
        	$icon = $this->model->uploads('icon');
        }else{
        	$icon = '';
        }
        $result = $this->d->add($icon,$_POST['status'],$_POST['lianjie']);
        return redirect($_SESSION['web']['houtai'].'sj_banner');
	}

	/**
	 * /	编辑
	 * @return [type] [description]
	 */
	public function edit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $info = $this->model->find($this->table,'id',$_GET['id']);
        $this ->assign['info'] = $info;
		return view('Admin.Banner.edit',$this->assign);
	}

	/**
	 * /	sj_编辑
	 * @return [type] [description]
	 */
	public function sj_edit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $info = $this->model->find($this->sj,'id',$_GET['id']);
        $this ->assign['info'] = $info;
		return view('Admin.Banner.sj_edit',$this->assign);
	}

	/**
	 * /	确认编辑
	 * @return [type] [description]
	 */
	public function doedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        if (!empty($_FILES['icon']['name'])) {
        	$icon = $this->model->uploads('icon');
        }else{
        	$icon = $this->model->find($this->table,'id',$_POST['id'])->icon;
        }
        if ($_POST['shanchu'] == 'shan') {
        	$icon = '';
        }
        $result = $this->m->update_list($_POST['id'],$icon,$_POST['status'],$_POST['lianjie']);
        return redirect($_SESSION['web']['houtai'].'banner');
	}

	/**
	 * /	sj_确认编辑
	 * @return [type] [description]
	 */
	public function sj_doedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        if (!empty($_FILES['icon']['name'])) {
        	$icon = $this->model->uploads('icon');
        }else{
        	$icon = $this->model->find($this->sj,'id',$_POST['id'])->icon;
        }
        if ($_POST['shanchu'] == 'shan') {
        	$icon = '';
        }
        $result = $this->d->update_list($_POST['id'],$icon,$_POST['status'],$_POST['lianjie']);
        return redirect($_SESSION['web']['houtai'].'sj_banner');
	}

//ajax----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * /	ajax 批量下架
	 * @return [type] [description]
	 */
	public function xia()
	{
		$reslut = $this->model->xia($this->table,$_POST['id']);
	}

	/**
	 * /	ajax 批量显示
	 * @return [type] [description]
	 */
	public function shang()
	{
		$reslut = $this->model->shang($this->table,$_POST['id']);
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function shang_xia()
	{
		$reslut = $this->model->shang_xia($this->table,$_POST['id']);
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function del_one()
	{
		$reslut = $this->model->del_one($this->table,$_POST['id']);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function del()
	{
		$reslut = $this->model->del($this->table,$_POST['id']);
	}
//ajax----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * /	ajax 批量下架
	 * @return [type] [description]
	 */
	public function sj_xia()
	{
		$reslut = $this->model->xia($this->sj,$_POST['id']);
	}

	/**
	 * /	ajax 批量显示
	 * @return [type] [description]
	 */
	public function sj_shang()
	{
		$reslut = $this->model->shang($this->sj,$_POST['id']);
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function sj_shang_xia()
	{
		$reslut = $this->model->shang_xia($this->sj,$_POST['id']);
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function sj_del_one()
	{
		$reslut = $this->model->del_one($this->sj,$_POST['id']);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function sj_del()
	{
		$reslut = $this->model->del($this->sj,$_POST['id']);
	}

}


















