<?php
namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class LiuyanController extends Controller
{	
	public $assign = [];


	/**
	 * /	留言首页
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect("login");
        }
		$model = new \App\Model\admin\liuyan();
		$info = $model->get();
		$this ->assign['info'] = $info;
		return view('Admin.liuyan',$this->assign);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function alldeleteleave()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\liuyan();
		$reslut = $model->alldeleteleave($ids);
	}

	/**
	 * /	前台存储留言
	 * @return [type] [description]
	 */
	public function liuyanguanli()
	{
		$name = $_POST['xingming'];
		$email = $_POST['youxiang'];
		$phone = $_POST['dianhua'];
		$main = $_POST['liuyanguanli'];
		$model = new \App\Model\admin\liuyan();
		$reslut = $model->add($name,$email,$phone,$main);
	}

	/**
	 * /	全部下架
	 * @return [type] [description]
	 */
	public function alllowerleave()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\liuyan();
		$reslut = $model->alllowerleave($ids);
	}

	/**
	 * /	全部显示
	 * @return [type] [description]
	 */
	public function allshowsizeleave()
	{
		$ids = $_POST['id'];
		$model = new \App\Model\admin\liuyan();
		$reslut = $model->allshowsizeleave($ids);
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function ajaxeditleave()
	{
		$model = new \App\Model\admin\liuyan();
		$id = $_POST['id'];
		$reslut = $model->gengxin($id);
		// var_dump($_POST);die;
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function ajaxdelleave()
	{
		$id = $_POST['id'];
		$model = new \App\Model\admin\liuyan();
		$reslut = $model->del($id);
	}

	/**
	 * /	查看详情
	 * @return [type] [description]
	 */
	public function ajaxshowleave()
	{
		$id = $_POST['id'];
		$model = new \App\Model\admin\liuyan();
		$reslut = $model->show($id);
		return $reslut;
	}

}


















