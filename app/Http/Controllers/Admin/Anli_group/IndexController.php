<?php
namespace App\Http\Controllers\Admin\Anli_group;

use App\Http\Controllers\Controller;
class IndexController extends Controller
{	
	public $assign = [];

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = $_SESSION['M'];
       if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login")->send();
        }
       return true;
	}

	/**
	 * /	新闻分类列表
	 * @return [type] [description]
	 */
	public function index()
	{
        $info = $this->model->paginate('anli_group');
        $this ->assign['info'] = $info;
		return view('Admin.Anli_group.index',$this->assign);
	}

	/**
	 * /	新增
	 * @return [type] [description]
	 */
	public function add()
	{
		return view('Admin.Anli_group.add');
	}

	/**
	 * /	确认新增
	 * @return [type] [description]
	 */
	public function doadd()
	{
		$data['title'] = $_POST['title'];
		$data['status'] = $_POST['status'];
        $id = $this->model->add('anli_group',$data);
        return redirect($_SESSION['web']['houtai'].'case_group');
	}

	/**
	 * /	编辑
	 * @return [type] [description]
	 */
	public function edit()
	{
        $info = $this->model->find('anli_group','id',$_GET['id']);
        $this ->assign['info'] = $info;
		return view('Admin.Anli_group.edit',$this->assign);
	}

	/**
	 * /	确认编辑
	 * @return [type] [description]
	 */
	public function doedit()
	{
		$data['title'] = $_POST['title'];
		$data['status'] = $_POST['status'];
        $result = $this->model->edit('anli_group',$_POST['id'],$data);
        return redirect($_SESSION['web']['houtai'].'case_group');
	}
}


















