<?php
namespace App\Http\Controllers\Admin\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{	
	public $assign = [];
	public $table = 'user';
	public $group = 'group';
	public $model = '';
	public $m = '';

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = new \App\Model\M();
       $this->m = new \App\Model\admin\User();
       return true;
	}

	/**
	 * /	角色列表
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
		$info = $this->model->paginate($this->table);
		$fenzu = $this->model->get($this->group);
		$this ->assign['info'] = $info;
		$this ->assign['fenzu'] = $fenzu;
		return view('Admin.User.index',$this->assign);
	}

	/**
	 * /	新增 或 编辑
	 * @return [type] [description]
	 */
	public function add_edit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
		$addoredit = $_POST['addoredit'];
		if ($addoredit == 'add') {
			$gid = $_POST['gid'];
			$username = $_POST['username'];
			$password = md5($_POST['password']);
			$status = $_POST['status'];
			$yongdefenzu = $this->model->find($this->group,'id',$gid);
			$fenzu = $yongdefenzu->title;
			$result = $this->m->add($gid,$username,$password,$status,$fenzu);
		}else{
			$id = $_POST['id'];
			$gid = $_POST['gid'];
			if (empty($_POST['password'])) {
				$yongdemima = $model->find($id);
				$_POST['password'] = $yongdemima['password'];
			}
			$username = $_POST['username'];
			$password = md5($_POST['password']);
			$status = $_POST['status'];
			if ($gid == '0') {
				$fenzu = '';
			}else{
				$yongdefenzu = $this->model->find($this->group,'id',$gid);
				$fenzu = $yongdefenzu->title;
			}
			$result = $this->m->updateuser($id,$gid,$username,$password,$status,$fenzu);
		}
		return redirect("user");
	}

//ajax----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * /	ajax 批量下架
	 * @return [type] [description]
	 */
	public function xia()
	{
		$reslut = $this->model->xia($this->table,$_POST['id']);
	}

	/**
	 * /	ajax 批量显示
	 * @return [type] [description]
	 */
	public function shang()
	{
		$reslut = $this->model->shang($this->table,$_POST['id']);
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function shang_xia()
	{
		$reslut = $this->model->shang_xia($this->table,$_POST['id']);
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function del_one()
	{
		$reslut = $this->model->del_one($this->table,$_POST['id']);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function del()
	{
		$reslut = $this->model->del($this->table,$_POST['id']);
	}

}


















