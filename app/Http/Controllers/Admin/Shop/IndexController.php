<?php
namespace App\Http\Controllers\Admin\Shop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;

class IndexController extends Controller
{	
	public $assign = [];
	public $table = 'shop';
	public $model = '';
	public $m = '';

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = new \App\Model\M();
       $this->m = new \App\Model\admin\Shop();
       return true;
	}

	/**
	 * /	品牌列表
	 * @return [type] [description]
	 */
	public function index()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $info = $this->model->paginate($this->table);
        $this ->assign['info'] = $info;
		return view('Admin.Shop.index',$this->assign);
	}

	/**
	 * /	新增
	 * @return [type] [description]
	 */
	public function add()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
		return view('Admin.Shop.add',$this->assign);
	}

	/**
	 * /	确认新增
	 * @return [type] [description]
	 */
	public function doadd()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        if (!empty($_FILES['icon']['name'])) {
        	$icon = $this->model->uploads('icon');
        }else{
        	$icon = '';
        }
        if (!empty($_FILES['logo']['name'])) {
        	$logo = $this->model->uploads('logo');
        }else{
        	$logo = '';
        }
        if (!empty($_FILES['list_icon']['name'])) {
        	$list_icon = $this->model->uploads('list_icon');
        }else{
        	$list_icon = '';
        }
        $result = $this->m->add($_POST['title_z'],$_POST['title_e'],$_POST['status'],$icon,$_POST['main'],$_POST['xiangxi'],$logo,$list_icon,$_POST['liebiao_jianjie']);
        return redirect($_SESSION['web']['houtai'].'shop');
	}

	/**
	 * /	编辑
	 * @return [type] [description]
	 */
	public function edit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $info = $this->model->find($this->table,'id',$_GET['id']);
        $this ->assign['info'] = $info;
		return view('Admin.Shop.edit',$this->assign);
	}

	/**
	 * /	确认编辑
	 * @return [type] [description]
	 */
	public function doedit()
	{
		if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login");
        }
        $file = $this->model->find($this->table,'id',$_POST['id'])->icon;
        $logo = $this->model->find($this->table,'id',$_POST['id'])->logo;
        $list = $this->model->find($this->table,'id',$_POST['id'])->list_icon;
        if (!empty($_FILES['icon']['name'])) {
        	$icon = $this->model->uploads('icon');
        	if(file_exists($file)){
		        unlink($file);
		    }
        }else{
        	$icon = $file;
        }
        if (!empty($_FILES['logo']['name'])) {
        	$llogo = $this->model->uploads('logo');
        	if(file_exists($logo)){
		        unlink($logo);
		    }
        }else{
        	$llogo = $logo;
        }
        if (!empty($_FILES['list_icon']['name'])) {
        	$list_icon = $this->model->uploads('list_icon');
        	if(file_exists($file)){
		        unlink($list_icon);
		    }
        }else{
        	$list_icon = $list;
        }
        if ($_POST['shanchu'] == 'shan') {
        	$icon = '';
        }
        if ($_POST['logo'] == 'shan') {
        	$llogo = '';
        }
        $result = $this->m->update_list($_POST['id'],$_POST['title_z'],$_POST['title_e'],$_POST['status'],$icon,$_POST['main'],$_POST['xiangxi'],$llogo,$list_icon,$_POST['liebiao_jianjie']);
        return redirect($_SESSION['web']['houtai'].'shop');
	}

//ajax----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	
	/**
	 * /	ajax 批量下架
	 * @return [type] [description]
	 */
	public function xia()
	{
		$reslut = $this->model->xia($this->table,$_POST['id']);
	}

	/**
	 * /	ajax 批量显示
	 * @return [type] [description]
	 */
	public function shang()
	{
		$reslut = $this->model->shang($this->table,$_POST['id']);
	}

	/**
	 * /	单个显示下架
	 * @return [type] [description]
	 */
	public function shang_xia()
	{
		$reslut = $this->model->shang_xia($this->table,$_POST['id']);
	}

	/**
	 * /	单个删除
	 * @return [type] [description]
	 */
	public function del_one()
	{
		$reslut = $this->model->del_one($this->table,$_POST['id']);
	}

	/**
	 * /	批量删除
	 * @return [type] [description]
	 */
	public function del()
	{
		$reslut = $this->model->del($this->table,$_POST['id']);
	}

}


















