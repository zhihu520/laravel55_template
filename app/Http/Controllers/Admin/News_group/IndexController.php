<?php
namespace App\Http\Controllers\Admin\News_group;

use App\Http\Controllers\Controller;
class IndexController extends Controller
{	
	public $assign = [];

	/**
	 * /	构造函数
	 * @return [type] [description]
	 */
	public function __construct() 
	{
       $this->model = $_SESSION['M'];
       if (empty($_COOKIE['TestCookie'])) {
            return redirect($_SESSION['web']['houtai']."login")->send();
        }
       return true;
	}

	/**
	 * /	新闻分类列表
	 * @return [type] [description]
	 */
	public function index()
	{
        $info = $this->model->paginate('news_group');
        $this ->assign['info'] = $info;
		return view('Admin.News_group.index',$this->assign);
	}

	/**
	 * /	新增
	 * @return [type] [description]
	 */
	public function add()
	{
		return view('Admin.News_group.add');
	}

	/**
	 * /	确认新增
	 * @return [type] [description]
	 */
	public function doadd()
	{
		$data['title'] = $_POST['title'];
		$data['status'] = $_POST['status'];
        $id = $this->model->add('news_group',$data);
        return redirect($_SESSION['web']['houtai'].'news_group');
	}

	/**
	 * /	编辑
	 * @return [type] [description]
	 */
	public function edit()
	{
        $info = $this->model->find('news_group','id',$_GET['id']);
        $this ->assign['info'] = $info;
		return view('Admin.News_group.edit',$this->assign);
	}

	/**
	 * /	确认编辑
	 * @return [type] [description]
	 */
	public function doedit()
	{
		$data['title'] = $_POST['title'];
		$data['status'] = $_POST['status'];
        $result = $this->model->edit('news_group',$_POST['id'],$data);
        return redirect($_SESSION['web']['houtai'].'news_group');
	}
}


















