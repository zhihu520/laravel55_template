<?php
namespace App\Http\Controllers\Home\Anli;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{   
    public $assign = [];

    /**
     * /    构造函数
     * @return [type] [description]
     */
    public function __construct() 
    {
       $this->model = $_SESSION['M'];
    }

    /**
     * /    案例
     * @return [type] [description]
     */
    public function index()
    {
        $group = $this->model->get('anli_group');
        $info = $this->model->get('anli');
        $this ->assign['group'] = $group;
        $this ->assign['info'] = $info;
        return view('Home.Anli.index',$this->assign);
    }

    /**
     * /    案例详情
     * @return [type] [description]
     */
    public function case_con()
    {
        $info = $this->model->find('anli','id',$_GET['id']);
        $this ->assign['info'] = $info;
        return view('Home.Anli.info',$this->assign);
    }

}