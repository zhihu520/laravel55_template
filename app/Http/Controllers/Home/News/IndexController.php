<?php
namespace App\Http\Controllers\Home\News;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{   
    public $assign = [];

    /**
     * /    构造函数
     * @return [type] [description]
     */
    public function __construct() 
    {
       $this->model = $_SESSION['M'];
    }

    /**
     * /    新闻
     * @return [type] [description]
     */
    public function index()
    {
        $group = $this->model->get('news_group');
        $list = $this->model->get('news_list');
        $this ->assign['list'] = $list;
        $this ->assign['group'] = $group;
        return view('Home.News.index',$this->assign);
    }

    /**
     * /    新闻详情
     * @return [type] [description]
     */
    public function news_con()
    {
        // echo "<pre>";
        $info = $this->model->find('news_list','id',$_GET['id']);
        // var_dump($info);die;
        $this ->assign['info'] = $info;
        return view('Home.News.info',$this->assign);
    }

}