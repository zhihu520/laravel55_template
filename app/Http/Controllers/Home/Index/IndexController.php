<?php
namespace App\Http\Controllers\Home\Index;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{   
    public $assign = [];

    /**
     * /    构造函数
     * @return [type] [description]
     */
    public function __construct() 
    {
       $this->model = $_SESSION['M'];
    }

    /**
     * /    首页
     * @return [type] [description]
     */
    public function index()
    {
        // echo "<pre>";
        $anli = $this->model->paginate('anli',8);
        // var_dump($anli);die;
        $this ->assign['anli'] = $anli;
        return view('Home.Index.index',$this->assign);
    }

}