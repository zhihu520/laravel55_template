<?php
namespace App\Http\Controllers\Home\Contact;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{   
    public $assign = [];

    /**
     * /    构造函数
     * @return [type] [description]
     */
    public function __construct() 
    {
       $this->model = $_SESSION['M'];
    }

    /**
     * /    首页
     * @return [type] [description]
     */
    public function index()
    {
        return view('Home.Contact.index');
    }

}