<?php
namespace App\Http\Controllers\Home\About;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{   
    public $assign = [];

    /**
     * /    构造函数
     * @return [type] [description]
     */
    public function __construct() 
    {
       $this->model = $_SESSION['M'];
    }

    /**
     * /    关于
     * @return [type] [description]
     */
    public function index()
    {
        $info = $this->model->find('system','id',1);
        $this ->assign['info'] = $info;
        return view('Home.About.index',$this->assign);
    }

}