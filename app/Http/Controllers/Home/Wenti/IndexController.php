<?php
namespace App\Http\Controllers\Home\Wenti;

use App\Http\Controllers\Controller;

class IndexController extends Controller
{   
    public $assign = [];

    /**
     * /    构造函数
     * @return [type] [description]
     */
    public function __construct() 
    {
       $this->model = $_SESSION['M'];
    }

    /**
     * /    问题
     * @return [type] [description]
     */
    public function index()
    {
        $info = $this->model->get('question');
        $this ->assign['info'] = $info;
        return view('Home.Wenti.index',$this->assign);
    }

    /**
     * /    问题详情
     * @return [type] [description]
     */
    public function wentishow()
    {
        // echo "<pre>";
        $info = $this->model->find('question','id',$_GET['id']);
        $this ->assign['info'] = $info;
        // var_dump($info);die;
        return view('Home.Wenti.info',$this->assign);
    }

}