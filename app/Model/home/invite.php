<?php
namespace App\Model\home;

use Illuminate\Database\Eloquent\Model;
class Invite extends Model
{
	//表名
	protected  $table = 'ly_talent';
	//白名单
	protected $fillable = ['id','group','create_time','update_time','icon','main','status','work','title','address','english']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;


	//数量查询
	public function get()
	{
		$data = $this->select('*')->orderBy('id','desc')->paginate();
		if(!empty($data)){
			return $data;
		}
		return [];
	}


	//根据id查询
	public function getById($id)
	{

		$data = $this->where('id',$id)->first();
		if(!empty($data)){
			return $data;
		}
		return '';
	}
}














