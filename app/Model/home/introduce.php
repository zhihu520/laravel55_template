<?php
namespace App\Model\home;

use Illuminate\Database\Eloquent\Model;
class Index extends Model
{
	//表名
	protected  $table = 'ly_admin_slider';
	//白名单
	protected $fillable = ['title','create_time','url','target','update_time','path']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

	public function getNews(){
		$data = $this->select('*')->orderBy('id','desc')->paginate(5);
		if(!empty($data)){
			return $data;
		}
		return [];

	}
	public function createNews($title='',$describe='',$content='',$createtime=''){
		$data = $this->create(['title'=>$title,'describe'=>$describe,'content'=>$content,
			'createtime'=>$createtime]);
		if (!empty($data)) {
			return $data;
		} else {
			return [];
		}
		
	}
	//根据id查询
	public function getNewById($id_news=0){

		$data = $this->where('id',$id_news)->first();
		if(!empty($data)){
			return $data;
		}
		return '';
	}
	//插入数据
	public function updateNews($id=0,$title='',$describe='',$content='',$createtime=''){
		$data = $this->where('id',$id)->update(['title'=>$title,'describe'=>$describe,'content'=>$content,
			'createtime'=>$createtime]);
		if($data == 1){
			return $data;
		}else{
			return '';
		}
	}
	//删除
	public function delOne($id=0){
		return $this->where('id',$id)->delete();
	}
}














