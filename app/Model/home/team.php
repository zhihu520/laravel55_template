<?php
namespace App\Model\home;

use Illuminate\Database\Eloquent\Model;
class Team extends Model
{
	//表名
	protected  $table = 'ly_team';
	//白名单
	protected $fillable = ['id','title','create_time','update_time','icon','main','status','paixu']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

	public function get()
	{
		$data = $this->select('*')->orderBy('id','desc')->paginate();
		if(!empty($data)){
			return $data;
		}
		return [];
	}
}














