<?php
namespace App\Model\home;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Family extends Model
{
	//表名
	protected  $table = 'shop';
	//白名单
	protected $fillable = ['title','create_time','url','target','update_time','path']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

	//根据品牌id获取所有产品
	public function get_all_pinpai_id($id)
	{
		$data = DB::table('chanpin')->select('*')->orderBy('update_time','asc')->where('pinpai_id',$id)->where('status','true')->paginate(10);
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//根据系列id或者品牌id获取产品
	public function get_all_pinpai_xilie_id($pinpai_id,$xid)
	{
		$data = DB::table('chanpin')->select('*')->orderBy('update_time','asc')->where('pinpai_id',$pinpai_id)->where('xilie_id',$xid)->where('status','true')->paginate(10);
		if(!empty($data)){
			return $data;
		}
		return [];

	}

	//根据系列id或者品牌id获取分类
	public function get_pinpai_id_xilie_id($pinpai_id,$xid)
	{
		$data = DB::table('product')->select('*')->orderBy('update_time','asc')->where('pinpai_id',$pinpai_id)->where('xilie_id',$xid)->where('status','true')->get();
		if(!empty($data)){
			return $data;
		}
		return [];

	}

	//根据系列id和者品牌id和分类id获取分类
	public function get_all_pinpai_xilie_fenlei_id($pinpai_id,$xid,$fid)
	{
		$data = DB::table('chanpin')->select('*')->orderBy('update_time','asc')->where('pinpai_id',$pinpai_id)->where('xilie_id',$xid)->where('fenlei_id',$fid)->where('status','true')->paginate(10);
		if(!empty($data)){
			return $data;
		}
		return [];

	}
}














