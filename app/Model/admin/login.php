<?php
namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Login extends Model
{
	//表名
	protected  $table = 'user';
	//白名单
	protected $fillable = ['username','password','id','email','create_time','status','mobile']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

	//获取数据
	public function get()
	{
		$data = $this->select('*')->orderBy('id','desc')->paginate(15);
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//更新数据
	public function gengxin($id)
	{
		$data = $this->where('id',$id)->first();
		if ($data['status'] == 'true') {
			$result = $this->where('id',$id)->update(['status'=>'false']);
		}else{
			$result = $this->where('id',$id)->update(['status'=>'true']);
		}
		return $result;
	}

	//下架所有数据
	public function alllowerleave($ids)
	{
		foreach ($ids as $k => $v) {
			$data = $this->where('id',$v)->first();
			if ($data['status'] == 'true') {
			$result = $this->where('id',$v)->update(['status'=>'false']);
			}else{
				//什么也不干
			}
		}
	}

	//显示所有数据
	public function allshowsizeleave($ids)
	{
		foreach ($ids as $k => $v) {
			$data = $this->where('id',$v)->first();
			if ($data['status'] == 'false') {
			$result = $this->where('id',$v)->update(['status'=>'true']);
			}else{
				//什么也不干
			}
		}
	}

	//批量删除
	public function alldeleteleave($ids)
	{
		foreach ($ids as $k => $v) {
			$this->where('id',$v)->delete();
		}
	}

	//删除一条数据
	public function del($id)
	{
		return $this->where('id',$id)->delete();
	}
	
	//查看详情
	public function show($name)
	{
		$result = $this->where('username',$name)->first();
		return $result;
	}
}














