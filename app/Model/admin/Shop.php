<?php
namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Shop extends Model
{
	//表名
	protected  $table = 'shop';
	//白名单
	protected $fillable = ['id','icon','title_e','title_z','update_time','create_time','main','xiangxi','logo','list_icon','liebiao_jianjie']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//新增数据
	public function add($title_z,$title_e,$status,$icon,$main,$xiangxi,$logo,$list_icon,$liebiao_jianjie)
	{
		$result = $this->create(['title_z'=>$title_z,'title_e'=>$title_e,'status'=>$status,'main'=>$main,'icon'=>$icon,'logo'=>$logo,'list_icon'=>$list_icon,'xiangxi'=>$xiangxi,'liebiao_jianjie'=>$liebiao_jianjie,'create_time'=>time(),'update_time'=>time()]);
		if(!empty($result)){
			return $result;
		}
		return [];
	}

	//编辑数据
	public function update_list($id,$title_z,$title_e,$status,$icon,$main,$xiangxi,$logo,$list_icon,$liebiao_jianjie)
	{
		$result = $this->where('id',$id)->update(['title_z'=>$title_z,'title_e'=>$title_e,'status'=>$status,'main'=>$main,'icon'=>$icon,'logo'=>$logo,'list_icon'=>$list_icon,'xiangxi'=>$xiangxi,'liebiao_jianjie'=>$liebiao_jianjie,'update_time'=>time()]);
	}

}














