<?php
namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Team extends Model
{
	//表名
	protected  $table = 'team';
	//白名单
	protected $fillable = ['title','create_time','id','update_time','icon','main','status']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

	//获取数据
	public function get()
	{
		$data = $this->select('*')->orderBy('id','desc')->paginate(15);
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//更新数据
	public function gengxin($id)
	{
		$data = $this->where('id',$id)->first();
		if ($data['status'] == 'true') {
			$result = $this->where('id',$id)->update(['status'=>'false','update_time'=>time()]);
		}else{
			$result = $this->where('id',$id)->update(['status'=>'true','update_time'=>time()]);
		}
		return $result;
	}

	//下架所有数据
	public function alllower($ids)
	{
		foreach ($ids as $k => $v) {
			$data = $this->where('id',$v)->first();
			if ($data['status'] == 'true') {
			$result = $this->where('id',$v)->update(['status'=>'false','update_time'=>time()]);
			}else{
				//什么也不干
			}
		}
	}

	//显示所有数据
	public function allshowsize($ids)
	{
		foreach ($ids as $k => $v) {
			$data = $this->where('id',$v)->first();
			if ($data['status'] == 'false') {
			$result = $this->where('id',$v)->update(['status'=>'true','update_time'=>time()]);
			}else{
				//什么也不干
			}
		}
	}

	//批量删除
	public function alldeletesize($ids)
	{
		foreach ($ids as $k => $v) {
			$this->where('id',$v)->delete();
		}
	}

	//删除一条数据
	public function del($id)
	{
		return $this->where('id',$id)->delete();

	}
	
	//新增数据
	public function add($title,$miaoshu,$status,$icon)
	{
		$this->create(['title'=>$title,'main'=>$miaoshu,'icon'=>$icon,'status'=>$status,'create_time'=>time(),'update_time'=>time()]);
	}

	//编辑数据
	public function edit($id,$title,$miaoshu,$main,$icon)
	{
		// var_dump($icon);die;
		$result = $this->where('id',$id)->update(['title'=>$title,'miaoshu'=>$miaoshu,'icon'=>$icon,'main'=>$main,'update_time'=>time()]);
	}

	//查看详情
	public function show($id)
	{
		$result = $this->where('id',$id)->first();
		$data['title'] = $result['title'];
		$data['status'] = $result['status'];
		$data['main'] = $result['main'];
		$data['miaoshu'] = $result['miaoshu'];
		$data['icon'] = $result['icon'];
		return $data;
	}

	//查看单个
	public function find($id)
	{
		$result = $this->where('id',$id)->first();
		return $result;
	}

	//编辑数据
	public function gaidongxi($id,$title,$miaoshu,$status,$icon)
	{
		$result = $this->where('id',$id)->update(['title'=>$title,'main'=>$miaoshu,'status'=>$status,'icon'=>$icon]);
	}
}














