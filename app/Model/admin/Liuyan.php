<?php
namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Liuyan extends Model
{
	//表名
	protected  $table = 'leave';
	//白名单
	protected $fillable = ['name','time','id','email','main','status','phone']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

	//新增数据
	public function add($name,$email,$phone,$main)
	{
		$result = $this->create(['name'=>$name,'email'=>$email,'phone'=>$phone,'main'=>$main,'create_time'=>time(),'time'=>time(),'update_time'=>time()]);
		return $result;
	}
}














