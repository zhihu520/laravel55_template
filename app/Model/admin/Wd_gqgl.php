<?php
namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Wd_gqgl extends Model
{
	//表名
	protected  $table = 'wd_gqgl';
	//白名单
	protected $fillable = ['id','wen','da','status','create_time','update_time']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//新增数据
	public function add($wen,$da,$status)
	{
		$result = $this->create(['wen'=>$wen,'da'=>$da,'status'=>$status,'create_time'=>time(),'update_time'=>time()]);
		if(!empty($result)){
			return $result;
		}
		return [];
	}

	//编辑数据
	public function update_($id,$wen,$da,$status)
	{
		$result = $this->where('id',$id)->update(['wen'=>$wen,'da'=>$da,'status'=>$status,'update_time'=>time()]);
		if(!empty($result)){
			return $result;
		}
		return [];
	}

}














