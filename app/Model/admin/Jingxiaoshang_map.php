<?php
namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Jingxiaoshang_map extends Model
{
	//表名
	protected  $table = 'jingxiaoshang_map';
	//白名单
	protected $fillable = ['id','city','address','lianjie','phone','sheng']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//新增数据
	public function add($city,$address,$phone,$lianjie,$status,$sheng)
	{
		$result = $this->create(['city'=>$city,'address'=>$address,'phone'=>$phone,'lianjie'=>$lianjie,'status'=>$status,'sheng'=>$sheng]);
		if(!empty($result)){
			return $result;
		}
		return [];
	}

	//更新数据
	public function update_($id,$city,$address,$phone,$lianjie,$status,$sheng)
	{
		$result = $this->where('id',$id)->update(['city'=>$city,'address'=>$address,'phone'=>$phone,'lianjie'=>$lianjie,'status'=>$status,'sheng'=>$sheng]);
	}

}














