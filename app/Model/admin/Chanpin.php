<?php
namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Chanpin extends Model
{
	//表名
	protected  $table = 'chanpin';
	//白名单
	protected $fillable = ['id','pinpai_id','xilie_id','fenlei_id','shuoming','guige','guige_shuzi','xinghao','yongcai','jiage','yangshi','status','main','update_time','create_time','icon']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//新增数据
	public function add($xinghao,$shuoming,$guige,$guige_shuzi,$yongcai,$jiage,$pinpai_id,$xilie_id,$fenlei_id,$status,$icon,$main)
	{
		$result = $this->create(['xinghao'=>$xinghao,'shuoming'=>$shuoming,'guige'=>$guige,'guige_shuzi'=>$guige_shuzi,'yongcai'=>$yongcai,'jiage'=>$jiage,'pinpai_id'=>$pinpai_id,'xilie_id'=>$xilie_id,'fenlei_id'=>$fenlei_id,'icon'=>$icon,'status'=>$status,'main'=>$main,'create_time'=>time(),'update_time'=>time()]);
		if(!empty($result)){
			return $result;
		}
		return [];
	}
	//编辑数据
	public function update_list($id,$xinghao,$shuoming,$guige,$guige_shuzi,$yongcai,$jiage,$pinpai_id,$xilie_id,$fenlei_id,$status,$icon,$main)
	{
		$result = $this->where('id',$id)->update(['xinghao'=>$xinghao,'shuoming'=>$shuoming,'guige'=>$guige,'guige_shuzi'=>$guige_shuzi,'yongcai'=>$yongcai,'jiage'=>$jiage,'pinpai_id'=>$pinpai_id,'xilie_id'=>$xilie_id,'fenlei_id'=>$fenlei_id,'icon'=>$icon,'status'=>$status,'main'=>$main,'create_time'=>time(),'update_time'=>time()]);
	}

	//根据品牌id获取系列信息
	public function get($id)
	{
		$data = $this->select('*')->orderBy('id','desc')->where('pinpai_id',$_SESSION['admin']['data']['shop']['id'])->where('xilie_id',$id)->paginate(5);
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//根据品牌id和样式获取系列信息
	public function get_yangshi($pinpai_id,$yangshi)
	{
		$data = $this->select('*')->orderBy('id','desc')->where('pinpai_id',$pinpai_id)->where('yangshi',$yangshi)->get();
		if(!empty($data)){
			return $data;
		}
		return [];
	}

}














