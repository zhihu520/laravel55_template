<?php
namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class News_group extends Model
{
	//表名
	protected  $table = 'news_group';
	//白名单
	protected $fillable = ['title','create_time','id','miaoshu','update_time','icon','main']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

	//获取数据
	public function get()
	{
		$data = $this->select('*')->orderBy('id','desc')->paginate(15);
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//获取未禁用的数据数据
	public function getwhere()
	{
		$data = $this->select('*')->orderBy('id','desc')->where('status','true')->paginate(10);
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//更新数据
	public function gengxin($id)
	{
		$data = $this->where('id',$id)->first();
		if ($data['status'] == 'true') {
			$result = $this->where('id',$id)->update(['status'=>'false','update_time'=>time()]);
		}else{
			$result = $this->where('id',$id)->update(['status'=>'true','update_time'=>time()]);
		}
		return $result;
	}

	//下架所有数据
	public function alllower($ids)
	{
		foreach ($ids as $k => $v) {
			$data = $this->where('id',$v)->first();
			if ($data['status'] == 'true') {
			$result = $this->where('id',$v)->update(['status'=>'false','update_time'=>time()]);
			}else{
				//什么也不干
			}
		}
	}

	//显示所有数据
	public function allshowsize($ids)
	{
		foreach ($ids as $k => $v) {
			$data = $this->where('id',$v)->first();
			if ($data['status'] == 'false') {
			$result = $this->where('id',$v)->update(['status'=>'true','update_time'=>time()]);
			}else{
				//什么也不干
			}
		}
	}

	//新增数据
	public function add1($title,$miaoshu,$main,$icon,$status)
	{
		$this->create(['title'=>$title,'miaoshu'=>$miaoshu,'icon'=>$icon,'main'=>$main,'status'=>$status,'create_time'=>time(),'update_time'=>time()]);
	}

	//编辑数据
	public function edit($id,$title,$miaoshu,$main,$icon)
	{
		// var_dump($icon);die;
		$result = $this->where('id',$id)->update(['title'=>$title,'miaoshu'=>$miaoshu,'icon'=>$icon,'main'=>$main,'update_time'=>time()]);
	}

	//查看详情
	public function show($id)
	{
		$result = $this->where('id',$id)->first();
		$data['title'] = $result['title'];
		$data['status'] = $result['status'];
		$data['main'] = $result['main'];
		$data['miaoshu'] = $result['miaoshu'];
		$data['icon'] = $result['icon'];
		return $data;
	}

	//查看单个
	public function find($id)
	{
		$result = $this->where('id',$id)->first();
		return $result;
	}

	//编辑数据
	public function updatenew($id,$title,$miaoshu,$main,$icon,$iconcon,$status)
	{
		$result = $this->where('id',$id)->update(['title'=>$title,'miaoshu'=>$miaoshu,'main'=>$main,'icon'=>$icon,'iconcon'=>$iconcon,'status'=>$status,'update_time'=>time()]);
	}

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//新增数据
	public function add($title,$miaoshu)
	{
		$result = $this->create(['title'=>$title,'miaoshu'=>$miaoshu,'create_time'=>time(),'update_time'=>time()]);
		if(!empty($result)){
			return $result;
		}
		return [];
	}

	//更新数据
	public function update_group($id,$title,$miaoshu)
	{
		$result = $this->where('id',$id)->update(['title'=>$title,'miaoshu'=>$miaoshu,'update_time'=>time()]);
	}

}














