<?php
namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class News_list extends Model
{
	//表名
	protected  $table = 'news_list';
	//白名单
	protected $fillable = ['title','create_time','id','miaoshu','update_time','icon','main','gid','zuozhe','tuijian']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//新增数据
	public function add($title,$miaoshu,$gid,$status,$main,$icon,$zuozhe,$tuijian)
	{
		$result = $this->create(['title'=>$title,'miaoshu'=>$miaoshu,'gid'=>$gid,'status'=>$status,'main'=>$main,'zuozhe'=>$zuozhe,'icon'=>$icon,'tuijian'=>$tuijian,'create_time'=>time(),'update_time'=>time()]);
		if(!empty($result)){
			return $result;
		}
		return [];
	}

	//编辑数据
	public function update_list($id,$title,$miaoshu,$gid,$status,$main,$icon,$zuozhe,$tuijian)
	{
		$result = $this->where('id',$id)->update(['title'=>$title,'miaoshu'=>$miaoshu,'gid'=>$gid,'zuozhe'=>$zuozhe,'status'=>$status,'main'=>$main,'icon'=>$icon,'tuijian'=>$tuijian,'update_time'=>time()]);
	}

	//给前台首页获取数据
	public function get()
	{
		$data = $this->select('*')->orderBy('id','desc')->where('status','true')->where('tuijian',1)->get();
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//给前台首页获取数据
	public function get_sj()
	{
		$data = $this->select('*')->orderBy('id','desc')->where('status','true')->where('tuijian',1)->paginate(4);
		if(!empty($data)){
			return $data;
		}
		return [];
	}

}














