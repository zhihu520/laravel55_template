<?php
namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Xilie extends Model
{
	//表名
	protected  $table = 'xilie';
	//白名单
	protected $fillable = ['id','title','main','status','update_time','create_time','pinpai_id']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//新增数据
	public function add($title,$status,$main)
	{
		$result = $this->create(['title'=>$title,'status'=>$status,'main'=>$main,'pinpai_id'=>$_SESSION['admin']['data']['shop']['id'],'create_time'=>time(),'update_time'=>time()]);
		if(!empty($result)){
			return $result;
		}
		return [];
	}

	//编辑数据
	public function update_list($id,$title,$status,$main)
	{
		$result = $this->where('id',$id)->update(['title'=>$title,'status'=>$status,'main'=>$main,'pinpai_id'=>$_SESSION['admin']['data']['shop']['id'],'create_time'=>time(),'update_time'=>time()]);
	}

	//根据品牌id获取系列信息
	public function get($id)
	{
		$data = $this->select('*')->orderBy('id','desc')->where('pinpai_id',$id)->paginate(5);
		if(!empty($data)){
			return $data;
		}
		return [];
	}

}














