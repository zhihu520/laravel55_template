<?php
namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Picture extends Model
{
	//表名
	protected  $table = 'picture';
	//白名单
	protected $fillable = ['id','product_id','icon','main','status','create_time','update_time']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//新增数据
	public function add($main,$status,$icon)
	{
		$result = $this->create(['main'=>$main,'status'=>$status,'icon'=>$icon,'product_id'=>$_SESSION['admin']['data']['chanpin']['id'],'create_time'=>time(),'update_time'=>time()]);
		if(!empty($result)){
			return $result;
		}
		return [];
	}

	//编辑数据
	public function update_list($id,$main,$status,$icon)
	{
		$result = $this->where('id',$id)->update(['main'=>$main,'status'=>$status,'icon'=>$icon,'update_time'=>time()]);
	}

	//根据品牌id获取系列信息
	public function get($id)
	{
		$data = $this->select('*')->orderBy('id','desc')->where('product_id',$id)->paginate(5);
		if(!empty($data)){
			return $data;
		}
		return [];
	}

}














