<?php
namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Product extends Model
{
	//表名
	protected  $table = 'product';
	//白名单
	protected $fillable = ['id','pinpai_id','xilie_id','shuoming','guige','guige_shuzi','xinghao','yongcai','jiage','yangshi','status','main','update_time','create_time','icon']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//新增数据
	public function add($xinghao,$status)
	{
		$result = $this->create(['xinghao'=>$xinghao,'status'=>$status,'pinpai_id'=>$_SESSION['admin']['data']['shop']['id'],'xilie_id'=>$_SESSION['admin']['data']['xilie']['id'],'create_time'=>time(),'update_time'=>time()]);
		if(!empty($result)){
			return $result;
		}
		return [];
	}

	//编辑数据
	public function update_list($id,$xinghao,$status)
	{
		$result = $this->where('id',$id)->update(['xinghao'=>$xinghao,'status'=>$status,'create_time'=>time(),'update_time'=>time()]);
	}

	//根据品牌id获取系列信息
	public function get($id)
	{
		$data = $this->select('*')->orderBy('id','desc')->where('pinpai_id',$_SESSION['admin']['data']['shop']['id'])->where('xilie_id',$id)->paginate(5);
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//根据品牌id和样式获取系列信息
	public function get_yangshi($pinpai_id,$yangshi)
	{
		$data = $this->select('*')->orderBy('id','desc')->where('pinpai_id',$pinpai_id)->where('yangshi',$yangshi)->get();
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//根据省份和城市查询店面信息
	public function get_city_sheng($sheng,$city)
	{
		$data = DB::table('jingxiaoshang_map')->select('*')->orderBy('id','desc')->where('sheng',$sheng)->where('city',$city)->get();
		if(!empty($data)){
			return $data;
		}
		return [];
	}

}














