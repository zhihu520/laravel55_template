<?php
namespace App\Model\admin;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class Banner extends Model
{
	//表名
	protected  $table = 'banner';
	//白名单
	protected $fillable = ['id','create_time','status','update_time','icon','lianjie']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
	//新增数据
	public function add($icon,$status,$lianjie)
	{
		$result = $this->create(['icon'=>$icon,'status'=>$status,'lianjie'=>$lianjie,'create_time'=>time(),'update_time'=>time()]);
		if(!empty($result)){
			return $result;
		}
		return [];
	}

	//编辑数据
	public function update_list($id,$icon,$status,$lianjie)
	{
		$result = $this->where('id',$id)->update(['icon'=>$icon,'status'=>$status,'lianjie'=>$lianjie,'create_time'=>time(),'update_time'=>time()]);
	}

}














