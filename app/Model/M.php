<?php
namespace App\Model;
ini_set('max_execution_time', '0');//首先对于超大型的文件来说,php脚本执行时间超过30s就会停止,所以这里设置php脚本执行时间最长为0,即无限制
require 'php-sdk-master/autoload.php';
use Qiniu\Auth;
use Qiniu\Storage\UploadManager;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class M extends Model
{
	//表名
	protected  $table = '';
	//白名单
	protected $fillable = ['']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

	//根据条件查找单条信息
	public function find($table,$ziduan,$zhi)
	{
		$data = DB::table($table)->where($ziduan,$zhi)->first();
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//根据条件查找多条信息
	public function find_all($table,$ziduan,$zhi)
	{
		$data = DB::table($table)->where($ziduan,$zhi)->get();
		if(!empty($data)){
			return $data;
		}
		return [];
	}
	
	//根据表格名称获取全部信息
	public function get($table)
	{
		$data = DB::table($table)->get();
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//根据表格名称获取全部信息 分页
	public function paginate($table,$num=15)
	{
		$data = DB::table($table)->orderBy('id','desc')->paginate($num);
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//插入数据
	public function add($table,$data)
	{
		$id = DB::table($table)->insertGetId($data);
		return $id;
	}

	//更新数据
	public function edit($table,$id,$data)
	{
		$result = DB::table($table)->where('id',$id)->update($data);
	}

	//上传图片 公共方法
	public function uploads($picname)
	{
		$file = $_FILES[$picname]["tmp_name"];//获取的上传的临时文件  
		$name = $_FILES[$picname]["name"];//获取上传文件的文件名 
		$time = time();
		$time = $time.rand(0,1000000000); 
		$time = md5($time);
		date_default_timezone_set('PRC');
		$shijian = time();
		$shijian = date('Y-m-d H',$shijian);
		$path="./file/amin/".$shijian.'/'.$time.'/';  
		if (is_dir($path)){    
		    echo "对不起！目录 " . $path . " 已经存在！";  
		    echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
		}else{  
		        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
		    $res=mkdir($path,0777,true); //递归创建文件目录  
		    if ($res){  
		        var_dump($res);//boolean true  
		            if (is_dir($path)){    
		                echo "目录 $path 创建成功";  
		                echo $path.$name;  
		                echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
		            }  
		    }else{  
		        echo "目录 $path 创建失败";  
		    }  
		}
		return $path.$name;
	}

	//七牛云上传功能
	public function qiniuyun($filePath)
	{
		// 用于签名的公钥和私钥
		$accessKey = 'cF5RQLnFiUKgi2-zvTSwIDxhG1iZgP9d-mz-DKhw';//AK
		$secretKey = 'jLEOI9qUxkRBik46AhepEry94cGwAlL6wPzdv2WI';//Sk
		// 初始化签权对象
		$auth = new Auth($accessKey, $secretKey);
		$bucket = 'jadeclass';//储存空间名
		// 生成上传Token
		$token = $auth->uploadToken($bucket);
		// 构建 UploadManager 对象
		$uploadMgr = new UploadManager();
		// $filePath = './1.mp4';//要上传的文件
		// $filePath = $_FILES['file']['name'];//要上传的文件
		// echo "<pre />";
		// var_dump($_FILES['file']['name']);die;
		$domain = 'video.jadeonline.net';//七牛云 外链默认域名
		//随机生成一串字符串
		$pattern = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
		$suijishu = '';  
		for($i=0;$i<60;$i++)   
		{   
			$suijishu .= $pattern{mt_rand(0,60)};    //生成php随机数   
		}
		$key = $suijishu;//文件的key,文件名
		$url = 'http://'.$domain.'/'.$key; //生成文件的外链
		// 调用 UploadManager 的 putFile 方法进行文件的上传。
		list($ret, $err) = $uploadMgr->putFile($token, $key, $filePath);
		return $url;
	}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------ajax操作公共方法--------------------------------------------------------------------------------------------------------
//批量下架
	public function lower($table,$ids)
	{
		foreach ($ids as $k => $v) {
			$result = DB::table($table)->where('id',$v)->update(['status'=>'false','update_time'=>time()]);
		}
	}

//批量显示
	public function show($table,$ids)
	{
		foreach ($ids as $k => $v) {
			$result = DB::table($table)->where('id',$v)->update(['status'=>'true','update_time'=>time()]);
		}
	}

//单个显示下架
	public function lower_show($table,$id)
	{
		$data = DB::table($table)->where('id',$id)->first();
		if ($data->status == 'true') {
			$result = DB::table($table)->where('id',$id)->update(['status'=>'false','update_time'=>time()]);
		}else{
			$result = DB::table($table)->where('id',$id)->update(['status'=>'true','update_time'=>time()]);
		}
		return $result;
	}

//批量删除
	public function del($table,$ids)
	{
		foreach ($ids as $k => $v) {
			DB::table($table)->where('id',$v)->delete();
		}
	}

//根据id删除一条数据
	public function del_one($table,$id)
	{
		DB::table($table)->where('id',$id)->delete();
	}
}














