<?php
namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
class H extends Model
{
	//表名
	protected  $table = '';
	//白名单
	protected $fillable = ['']; 
	//有时间字段不自动更新手动更新
	public $timestamps = false;

	//根据条件查找单条信息
	public function find($table,$ziduan,$zhi)
	{
		$data = DB::table($table)->orderBy('id','desc')->where($ziduan,$zhi)->first();
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//根据条件查找多条信息
	public function find_all($table,$ziduan,$zhi)
	{
		$data = DB::table($table)->orderBy('update_time','asc')->where($ziduan,$zhi)->get();
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//根据条件查找多条信息 主要是公共头部的信息
	public function find_all_top($table,$ziduan,$zhi)
	{
		$data = DB::table($table)->orderBy('update_time','asc')->where($ziduan,$zhi)->get();
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//根据条件查找多条信息  带分页
	public function paginate_all($table,$ziduan,$zhi,$num)
	{
		$data = DB::table($table)->orderBy('id','desc')->where($ziduan,$zhi)->paginate($num);
		if(!empty($data)){
			return $data;
		}
		return [];
	}
	
	//根据表格名称获取全部信息
	public function get($table)
	{
		$data = DB::table($table)->where('status','true')->get();
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//根据表格名称获取指定信息量  带分页
	public function paginate($table,$num)
	{
		$data = DB::table($table)->where('status','true')->paginate($num);
		if(!empty($data)){
			return $data;
		}
		return [];
	}

	//上传图片 公共方法
	public function uploads($picname)
	{
		$file = $_FILES[$picname]["tmp_name"];//获取的上传的临时文件  
		$name = $_FILES[$picname]["name"];//获取上传文件的文件名 
		$time = time();
		$time = $time.rand(0,1000000000); 
		$time = md5($time);
		date_default_timezone_set('PRC');
		$shijian = time();
		$shijian = date('Y-m-d H',$shijian);
		$path="./file/amin/".$shijian.'/'.$time.'/';  
		if (is_dir($path)){    
		    echo "对不起！目录 " . $path . " 已经存在！";  
		    echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
		}else{  
		        //第三个参数是“true”表示能创建多级目录，iconv防止中文目录乱码  
		    $res=mkdir($path,0777,true); //递归创建文件目录  
		    if ($res){  
		        var_dump($res);//boolean true  
		            if (is_dir($path)){    
		                echo "目录 $path 创建成功";  
		                echo $path.$name;  
		                echo  move_uploaded_file($file ,$path.$name)? 'ok' : 'false';  
		            }  
		    }else{  
		        echo "目录 $path 创建失败";  
		    }  
		}
		return $path.$name;
	}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------------------ajax操作公共方法--------------------------------------------------------------------------------------------------------
//批量下架
	public function xia($table,$ids)
	{
		foreach ($ids as $k => $v) {
			$data = DB::table($table)->where('id',$v)->first();
			$result = DB::table($table)->where('id',$v)->update(['status'=>'false','update_time'=>time()]);
		}
	}

//批量显示
	public function shang($table,$ids)
	{
		foreach ($ids as $k => $v) {
			$data = DB::table($table)->where('id',$v)->first();
			$result = DB::table($table)->where('id',$v)->update(['status'=>'true','update_time'=>time()]);
		}
	}

//单个显示下架
	public function shang_xia($table,$id)
	{
		$data = DB::table($table)->where('id',$id)->first();
		if ($data->status == 'true') {
			$result = DB::table($table)->where('id',$id)->update(['status'=>'false','update_time'=>time()]);
		}else{
			$result = DB::table($table)->where('id',$id)->update(['status'=>'true','update_time'=>time()]);
		}
		return $result;
	}

//批量删除
	public function del($table,$ids)
	{
		foreach ($ids as $k => $v) {
			DB::table($table)->where('id',$v)->delete();
		}
	}

//根据id删除一条数据
	public function del_one($table,$id)
	{
		DB::table($table)->where('id',$id)->delete();
	}
//--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------//------------------------------------------------------------------------------------------------------问答系统------------------------------------------------------------------------------------------------------------------------

}














