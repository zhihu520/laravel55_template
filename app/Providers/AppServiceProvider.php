<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Model\M;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $M = new M();
        $info = $M->find('system','id',1);
        $_SESSION['M'] = $M;
        $_SESSION['info'] = $info;
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        session_start();
        date_default_timezone_set('PRC');
    }
}
