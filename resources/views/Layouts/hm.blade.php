<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>新闻资讯_高端炫酷IT网络建站公司网站模板</title>
<meta name="keywords" content="" />
<meta name="description" content="" />
<meta http-equiv="X-UA-Compatible" content="IE=edge, chrome=1">
<link rel="stylesheet" type="text/css" href="style/css/style_2_common.css" />
<link rel="stylesheet" type="text/css" href="style/css/style_2_portal_list.css" />
<script src="style/js/common.js" type="text/javascript"></script>
<script src="style/js/jquery-1.7.2.js" type="text/javascript"></script>
<script src="style/js/pace.js" type="text/javascript"></script>
<link href="style/css/style.css" rel="stylesheet" type="text/css" />
</head>
<body id="nv_portal" class="pg_list pg_list_2">
<div id="append_parent"></div>
<div id="ajaxwaitid"></div>
<div id="toptb" class="cl" style="display:none;"> </div>
<div id="hd" style="background:#FFF; height:60px; border-bottom:1px solid #E6E6E6; ">
  <div class="clear"></div>
  <div id="week_nav">
    <div class="wk_navwp">
      <div class="wk_lonav">
        <div class="wk_logo">
          <h2><a href="/" title=""><img src="style/images/logo.png" alt="" border="0" /></a></h2>
        </div>
        <div class="wk_inav">
            <ul class="nav">
                <li><a href="/">网站首页</a></li>

                <li><a href="about">关于我们</a></li>

                <li><a href="news">新闻资讯</a></li>

                <li><a href="case">项目案例</a></li>

                <li><a href="wenti">常见问题</a></li>

                <li><a href="contact">联系我们</a></li>

            </ul>
        </div>
      </div>
      
    </div>
  </div>
</div>

@yield('content')

<div id="wk_ft">
  <div class="section fp-auto-height">
  <div class="wk_footer_side">
    <div class="wk_footer">Copyright © 2017 17sucai.com <a title="baidu" href="#" target="_blank">技术支持：</a> 备案号：<a href="http://www.miitbeian.gov.cn/" target="_blank" title="苏ICP12345678">苏ICP12345678</a> <br />
      友情链接：<a href='http://www.17sucai.com/' target='_blank'>网页特效</a> <a href='http://www.17sucai.com/' target='_blank'>中文模板</a> <a href='http://www.17sucai.com/' target='_blank'>织梦模板</a> <a href='http://www.17sucai.com/' target='_blank'>discuz模板</a> <a href='http://www.17sucai.com/' target='_blank'>手机网站模板</a> <a href='http://www.17sucai.com/' target='_blank'>网站模板</a> <a href='http://www.17sucai.com/' target='_blank'>网站源码</a> <a href='#' target='_blank'>图标下载</a> <a href='#' target='_blank'>仿站</a> </div>
  </div>
</div>

</div>