<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="charset=UTF-8">
    <title>克拉维克-星途后台管理系统</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="http://at.alicdn.com/t/font_672336_zp6n3tazqn.css">
    <link rel="apple-touch-icon" type="image/x-icon" href="/img/saipu.ico">
    <link rel="shortcut icon" type="image/x-icon" href="/img/saipu.ico">
    <script type="text/javascript" src="http://at.alicdn.com/t/font_672336_zp6n3tazqn.js"></script>
    <script type="text/javascript" src="js/jquery-1.7.3.min.js"></script>
    <script type="text/javascript" src="Ueditor/ueditor.config.js"></script>
    <script type="text/javascript" src="Ueditor/ueditor.all.js"></script>
    <script type="text/javascript" src="Ueditor/lang/zh-cn/zh-cn.js"></script>
    <script type="text/javascript">
        UE.getEditor('intro_detail',{  //intro_detail为要编辑的textarea的id
            initialFrameWidth: 1000,  //初始化宽度
            initialFrameHeight: 460,  //初始化高度
        });
    </script>
</head>
<body>
    <div class="header clearfix">
        <p class="fl logo">星途后台管理系统</p>
        <div class="fr uesr">
            <a href="admin"><i class="iconfont icon-shouye"></i>星途主页</a>
            <a href="http://www.k-clara.com" target="target"><i class="iconfont icon-qiehuan"></i>打开前台</a>
            <!-- 用户消息 -->
            <div class="uesrCen">
                <p class="one"><img src="img/user.png" alt="">{{$_COOKIE['admin']['username']}}</p>
                <div class="uesrBox">
                    <div class="uesrUp">
                        <p class="uesrPic"><img src="img/user.png" alt=""></p>
                        <p class="uesrMane ovhid">{{$_COOKIE['admin']['username']}}</p>
                        <p class="uesrTime ovhid">{{date('Y-m-d H:i:s',$_COOKIE['admin']['create_time'])}}</p>
                    </div>
                    <div class="uesrDown clearfix">
                        <a href="javascript:;" class="fl clear" onclick="clear1()"><i class="iconfont icon-shanchu"></i>清除缓存</a>
                        <a href="{{$_SESSION['web']['houtai']}}loginout" class="fr quit"><i class="iconfont icon-qiehuanzuhu"></i>退出</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="bottom clearfix">
        <div class="fl conLeft">
            <div class="uesr clearfix">
                <div class="picUser fl"><img src="img/user.png" alt=""></div>
                <div class="nameUser fl">
                    <p class="name ovhid">{{$_COOKIE['admin']['username']}}</p>
                    <p class="state"><span></span>在线</p>
                </div>
            </div>
            <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
            <div class="leftBox">
                <p class="xian"></p>
                <ul class="nav_0" id="nav">
                    <li class="nav_1">
                        <a class="ovhid" href="http://www.k-clara.com" target="target" id="guanwangshouye"><i class="iconfont icon-shouye"></i>官网首页</a>
                    </li>
                    <li class="nav_1">
                        <a class="ovhid" href="javascript:;" id="xinwenguanli"><i class="iconfont icon-xinwendongtai"></i>新闻管理</a>
                        <ul class="nav_2">
                            <li id="xinwenfenlei"><a href="{{$_SESSION['web']['houtai']}}news_group" class="ovhid"><i class="iconfont icon-caidan"></i>新闻分类</a></li>
                            <li id="xinwenliebiao"><a href="{{$_SESSION['web']['houtai']}}news_list" class="ovhid"><i class="iconfont icon-caidan"></i>新闻列表</a></li>
                        </ul>
                    </li> 
                    <li class="nav_1">
                        <a class="ovhid" href="javascript:;" id="question"><i class="iconfont icon-xinwendongtai"></i>常见问题</a>
                        <ul class="nav_2">
                            <li id="changjianwenti"><a href="{{$_SESSION['web']['houtai']}}question" class="ovhid"><i class="iconfont icon-caidan"></i>常见问题</a></li>
                        </ul>
                    </li> 
                    <li class="nav_1">
                        <a class="ovhid" href="javascript:;" id="case"><i class="iconfont icon-xinwendongtai"></i>常见案例</a>
                        <ul class="nav_2">
                            <li id="changjiananli_group"><a href="{{$_SESSION['web']['houtai']}}case_group" class="ovhid"><i class="iconfont icon-caidan"></i>案例分类</a></li>
                            <li id="changjiananli"><a href="{{$_SESSION['web']['houtai']}}case" class="ovhid"><i class="iconfont icon-caidan"></i>常见案例</a></li>
                        </ul>
                    </li> 
                    <li class="nav_1">
                        <a class="ovhid" href="javascript:;" id="system"><i class="iconfont icon-xinwendongtai"></i>系统设置</a>
                        <ul class="nav_2">
                            <li id="gongsijianjie"><a href="{{$_SESSION['web']['houtai']}}about" class="ovhid"><i class="iconfont icon-caidan"></i>公司简介</a></li>
                            <li id="gongsixinxi"><a href="{{$_SESSION['web']['houtai']}}info" class="ovhid"><i class="iconfont icon-caidan"></i>公司信息</a></li>
                        </ul>
                    </li>
                </ul>
            </div>
        </div>
        @yield('content')
    </div>
</body>
<script type="text/javascript" src="js/main.js"></script>
</html>





    