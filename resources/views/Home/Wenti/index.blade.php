@extends('Layouts.hm')

@section('content')

<div id="mu" class="cl">
  <div class="wp"> </div>
</div>
<script src="style/js/week_nav.js" type="text/javascript"></script>
<div id="wp" class="wp"> 

</div>
<div class="wk_list_box wk_list_box14"> </div>
<div class="clear"></div>
<div class="wk_ymbg">
  <div id="wp" class="wp">
    <div class="wp"> 
      
      <!--[diy=diy1]-->
      <div id="diy1" class="area"></div>
      <!--[/diy]--> 
      
    </div>
    <div id="ct" class="ct2 wp cl">
      <div class="mn">
        <div class="wk_c_right_name">
          <div class="wk_c_right_name_r">
            <ul>
              <li> <img src="style/images/right_wz.png" alt="" /> </li>
              <li> <span>您现在的位置：</span><a href='#'>主页</a> > <a href='#'>常见问题</a> >  </li>
            </ul>
          </div>
        </div>
        <!--[diy=listcontenttop]-->
        <div id="listcontenttop" class="area"></div>
        <!--[/diy]-->
        
        <div class="bm">
          <div class="wk_bm_wc cl">
            <ul class="wk_su">
            @foreach($info as $v)
              <li class="cl">
                <h2><a href="wentishow?id={{$v->id}}" target="_blank" class="xi2" title="{{$v->title}}"  style="">{{$v->title}}</a> </h2>
                <p class="wk_bm_sm">{!!$v->miaoshu!!}</p>
                <span class="xg1">
                <span class="xg1">时间: {{date('Y-m-d H:i:s',$v->create_time)}}</span> </span> </li>
            @endforeach
            </ul>
          </div>
          
          <!--[diy=listloopbottom]-->
          
          <div id="listloopbottom" class="area"></div>
          <!--[/diy]--> 
          
        </div>
        <div class="clear"></div>
        <div class="pagess">
        <ul>
         <!-- <li><span class="pageinfo">共 <strong>1</strong>页<strong>1</strong>条记录</span></li> -->

        </ul>
        </div>
        
        <!--[diy=diycontentbottom]-->
        <div id="diycontentbottom" class="area"></div>
        <!--[/diy]--> 
        
      </div>
      <div class="sd pph">
        
        <div class="clear"></div>
        
        <!--[diy=wk_zcyl1]-->
        <div id="wk_zcyl1" class="area"></div>
        <!--[/diy]-->
        
        <div class="clear"></div>
        <div class="wk_c_left_cont"> <span class="wk_c_left_cont1">联系方式</span><span class="wk_c_left_cont2">Contact</span> </div>

<div class="wk_left_contdiv"> 
    <span><strong>地 址：</strong>{{$_SESSION['info']->address}}</span> 
    <span><strong>公司名称：</strong>{{$_SESSION['info']->title}}</span> 
    <span><strong>售前咨询：</strong>{{$_SESSION['info']->sqzx}}</span> 
    <span><strong>售后技术：</strong>{{$_SESSION['info']->shjs}}</span> 
    <span><strong>咨询Q Q：</strong>{{$_SESSION['info']->ywzx}}</span> 
    <span><strong>邮 箱：</strong><a href="mailto:12345678@qq.com">{{$_SESSION['info']->email}}</a></span> 
</div>

        <div class="clear"></div>
        
        <!--[diy=wk_zcyl2]-->
        <div id="wk_zcyl2" class="area"></div>
        <!--[/diy]-->
        
        <div class="clear"></div>
      </div>
    </div>
  </div>
  <div class="wp mtn"> 
    
    <!--[diy=diy3]-->
    <div id="diy3" class="area"></div>
    <!--[/diy]--> 
    
  </div>
</div>
@endsection
