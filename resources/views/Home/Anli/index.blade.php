@extends('Layouts.hm')

@section('content')

<div id="mu" class="cl">
  <div class="wp"> </div>
</div>
<script src="style/js/week_nav.js" type="text/javascript"></script>
<div id="wp" class="wp">
  <style id="diy_style" type="text/css">
</style>
  <style>

.wp { width:1190px; margin:0 auto; } 

</style>
</div>
<div class="wk_list_box wk_list_box17"> </div>
<div class="wk_blog_tab">
  <ul>
    @foreach($group as $v)
      <li><a href="#" title="{{$v->title}}">{{$v->title}}</a></li>
    @endforeach
  </ul>
  <div class="clear"></div>
</div>
<div class="clear"></div>
<div class="wk_ymbg">
  <div id="wp" class="wp">
    <div class="wp"> 
      
      <!--[diy=diy1]-->
      <div id="diy1" class="area"></div>
      <!--[/diy]--> 
      
    </div>
    <div class="week-case"> 
      
      <!--[diy=listcontenttop]-->
      <div id="listcontenttop" class="area"></div>
      <!--[/diy]-->
      
      <div class="week-case_con">
        @foreach($info as $v)
          <dl>
            <dd style="background:url('{{$v->icon}}') no-repeat center center;background-size:cover"><a href="case_con?id={{$v->id}}" title="{{$v->title}}" target="_blank"></a></dd>
            <dt>
              <h3><a href="case_con?id={{$v->id}}" title="{{$v->title}}" target="_blank">{{$v->title}}</a></h3>
            </dt>
          </dl>
        @endforeach
        <div class="clear"></div>
      </div>
      <div class="clear"></div>
      <div class="pagess">
        <ul>
         <!-- <li><span class="pageinfo">共 <strong>1</strong>页<strong>9</strong>条记录</span></li> -->

        </ul>
        </div>
      
      <!--[diy=diycontentbottom]-->
      
      <div id="diycontentbottom" class="area"></div>
      <!--[/diy]--> 
      
    </div>
  </div>
  <div class="wp mtn"> 
    
    <!--[diy=diy3]-->
    <div id="diy3" class="area"></div>
    <!--[/diy]--> 
    
  </div>
</div>
@endsection