@extends('layouts.ad')
@section('content')
		<div class="fr conRight">
			<p class="breadcrumb"><span>主页</span>/<span>常见问题</span>/<span>新增</span></p>
			<!-- 操作区域 -->
			<div class="editBox">
				<ul class="editTab clearfix">
					<li class="fl lihover"><span>内容</span></li>
				</ul>
				<form action="{{$_SESSION['web']['houtai']}}doquestion_add" method="post" enctype="multipart/form-data" class="addBox" onsubmit="return zuzhitijiao()">
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
					<ul class="editCon">
						<li>
							<!-- type=text的名为textgroup -->
							<table class="textgroup">
								<tr>
									<td class="title required">标题<span>*</span>：</td>
									<td class="text"><input type="text" value="" name="title" id="biaoti"></td>
									<td class="prompt">标题为必填</td>
								</tr>
								<tr>
									<td class="title">描述：</td>
									<td class="text"><textarea name="miaoshu"></textarea></td>
								</tr>
							</table>
							<!-- type=radio的名为radiogroup -->
							<div class="radiogroup clearfix">
								<p class="title fl">状态：</p>
								<div class="radioBox">
									<p>
										<input type="radio" name="status" id="true" checked="" value="true" />
										<label for="true">正常</label>
									</p>
									<p>
										<input type="radio" name="status" id="false" value="false" />
										<label for="false">下架</label>
									</p>
								</div>
							</div>
						</li>
					</ul>
					<!-- 提交 -->
					<div class="closure">
						<input type="submit" class="closurestyle" value="确定" />
						<a href="javascript:history.go(-1);" class="closurestyle Close">取消</a>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
 