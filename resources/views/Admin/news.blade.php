@extends('layouts.ad')
@section('content')
	<div class="fr conRight">
			<p class="breadcrumb"><span>赛普主页</span>/<span>新闻列表</span></p>
			<div class="operationBox">
				<p class="operationButtom">
					<?php if (in_array("newadd",$_COOKIE['admin']['menu_auth'])): ?>
						<a href="{{url('newadd')}}" class="add"><i class="iconfont icon-plus"></i>新增</a>
                    <?php endif ?>
                    <?php if (in_array("newshow",$_COOKIE['admin']['menu_auth'])): ?>
						<span class="show"><i class="iconfont icon-xianshi"></i>显示</span>
                    <?php endif ?>
                    <?php if (in_array("newlow",$_COOKIE['admin']['menu_auth'])): ?>
						<span class="lower"><i class="iconfont icon-xiajia"></i>下架</span>
                    <?php endif ?>
                    <?php if (in_array("newdelete",$_COOKIE['admin']['menu_auth'])): ?>
						<span class="delete"><i class="iconfont icon-shanchu"></i>删除</span>
                    <?php endif ?>
				</p>
				<div class="operationTable">
					<table>
						<tr>
							<th width="5%"><input name="btSelectAll" type="checkbox" checked=""></th>
							<th width="5%">ID</th>
							<th width="18%">标题</th>
							<th width="18%">描述</th>
							<th width="15%">创建时间</th>
							<th width="15%">修改时间</th>
							<th width="10%">状态</th>
							<th width="16%">操作</th>
						</tr>
						@foreach($info as $v)
							<tr>
								<td><input name="btSelectItem" type="checkbox"></td>
								<td id="{{$v->id}}">{{$v->id}}</td>
								<td class="titleItem">{{$v->title}}</td>
								<td>{{$v->miaoshu}}</td>
								<td>{{date('Y-m-d',$v->create_time)}}</td>
								<td>{{date('Y-m-d',$v->update_time)}}</td>
								<td class="state" data-state="{{$v->status}}"><span class="one"></span><span class="two"></span></td>
								<td>
									
									<?php if (in_array("newedit",$_COOKIE['admin']['menu_auth'])): ?>
										<a href="newedit?id={{$v->id}}"><i class="iconfont icon-liuyan edit" data-shu="{{$v->id}}"></i></a>
				                    <?php endif ?>
				                    <?php if (in_array("newdelete",$_COOKIE['admin']['menu_auth'])): ?>
										<i class="iconfont icon-shanchu delete" data-shu="{{$v->id}}"></i>
				                    <?php endif ?>
				                    <?php if (in_array("newlow",$_COOKIE['admin']['menu_auth'])): ?>
										<i class="iconfont icon-xiajia lower" data-shu="{{$v->id}}"></i>
				                    <?php endif ?>
				                    <?php if (in_array("newshow",$_COOKIE['admin']['menu_auth'])): ?>
										<i class="iconfont icon-xianshi show" data-shu="{{$v->id}}"></i>
				                    <?php endif ?>
									
									
								</td>
								<!-- <a href="javascript:delNews({{$v->id}})"><input type="button" class="btn btn-danger" value="删除"></a> -->
							</tr>
						@endforeach
					</table>
					{{$info->links()}}
				</div>
			</div>
			<!-- 新增与修改的弹出框 -->
			<div class="AddEdit">
			    <div class="AddEditTop clearfix">
					<p class="boxTitle fl ovhid">新增</p>
					<p class="boxClose Close fr"><i class="iconfont icon-guanbi-"></i></p>
			    </div>
			    <!-- 新增的提交 -->
			    <form action="{{url('addimg')}}" method="post" enctype="multipart/form-data" class="addBox">
			        <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
			            <!-- 新增的提交内容 -->
			            <div class="AddEditBottom">
			                <!-- 字段 -->
			                <div class="field">
			                    <!-- type=text的名为textgroup -->
			                    <div class="textgroup clearfix">
			                        <p class="title fl">标题：</p>
			                        <input type="text" value="" name="title">
			                        <!-- <p class="prompt fr">分类名称为必填</p> -->
			                    </div>
			                    <div class="textgroup clearfix">
			                        <p class="title fl">描述：</p>
			                        <input type="text" value="" name="miaoshu">
			                        <!-- <p class="prompt fr">描述为必填</p> -->
			                    </div>
			                    <!-- type=radio的名为radiogroup -->
			                    <div class="radiogroup clearfix">
			                        <p class="title fl">状态：</p>
			                        <div class="radioBox">
			                            <p>
			                                <input type="radio" name="status" checked="checked" value="true" id="oneeeeee" />
			                                <label for="oneeeeee">正常</label>
			                            </p>
			                            <p>
			                                <input type="radio" name="status" value="false" id="onaaaaaa" />
			                                <label for="onaaaaaa">下架</label>
			                            </p>
			                        </div>
			                    </div>
			                </div>
			                <!-- 单张图片上传 -->
			                <div class="picUploading">
			                    <div class="inputPic clearfix">
			                        <p class="title fl">单张图片：</p>
			                        <p class="upload fl">
			                            <span><i class="iconfont icon-shangchuan"></i>上传图片</span>
			                            <input type="file" name="file" onchange="preview(this)" />
			                        </p>
			                        <p class="prompt fl">建议大小:340 x 256 px</p>
			                    </div>
			                    <div class="picShow">
			                    </div>
			                </div>
			                <!-- 详情 -->
			                <div id="tab3" class="tab-pane">
			                    <!-- <h1>百度编辑器</h1> -->
			                    <textarea name="main" id="intro_detail">baidu5s3f1ds321fds321fs32da1f2ds3a1f32dsa1f32dsa1f23dsa1f23ads1f32d1sa2f3a132ad1f32dsa1f23ads1f3dsa</textarea>
			                </div>
			            </div>
			            <!-- 提交 -->
			            <div class="closure">
			                <input type="submit" class="closurestyle" value="确定"/>
			                <span  class="closurestyle Close">取消</span>
			                <input type="reset" class="closurestyle" value="重置" onmousedown="return $EDITORUI[&quot;edui51&quot;]._onMouseDown(event, this);" onclick="return $EDITORUI[&quot;edui51&quot;]._onClick(event, this);"/>
			            </div>
			    </form>
			    <!-- 修改的提交 -->
			    <form action="{{url('editimg')}}" method="post" enctype="multipart/form-data" class="editBox">
			        <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
			        <input type="hidden" name="id" value="" id="yaoyong">
			        <input type="hidden" name="shanchu" value="bushan" id="shanbushan">
			            <!-- 修改的提交内容 -->
			            <div class="AddEditBottom">
			                <!-- 字段 -->
			                <div class="field">
			                    <!-- type=text的名为textgroup -->
			                    <div class="textgroup clearfix">
			                        <p class="title fl">标题：</p>
			                        <input type="text" id="title" value="" name="title">
			                        <!-- <p class="prompt fr">分类名称为必填</p> -->
			                    </div>
			                    <div class="textgroup clearfix">
			                        <p class="title fl">描述：</p>
			                        <input type="text" id="miaoshu" value="" name="miaoshu">
			                        <!-- <p class="prompt fr">描述为必填</p> -->
			                    </div>
			                    <!-- type=radio的名为radiogroup -->
			                    <div class="radiogroup clearfix">
			                        <p class="title fl">状态：</p>
			                        <div class="radioBox">
			                            <p>
			                                <input type="radio" name="status" id="true" checked="checked" value="true"/>
			                                <label for="true">正常</label>
			                            </p>
			                            <p>
			                                <input type="radio" name="status" id="false" value="false" />
			                                <label for="false">下架</label>
			                            </p>
			                        </div>
			                    </div>
			                </div>
			                <!-- 单张图片上传 -->
			                <div class="picUploading">
			                    <div class="inputPic clearfix">
			                        <p class="title fl">单张图片：</p>
			                        <p class="upload fl">
			                            <span><i class="iconfont icon-shangchuan"></i>修改图片</span>
			                            <input type="file" name="file" onchange="preview(this)" />
			                        </p>
			                        <p class="prompt fl">建议大小:340 x 256 px</p>
			                    </div>
			                    <div class="picShow">
			                        <p class="picBox">
			                            <img src="img/icon1.png" alt="" id="myimage">
			                            <i class="iconfont icon-shanchu"></i>
			                        </p>
			                    </div>
			                </div>
			                <!-- 详情 -->
			                <div id="tab3" class="tab-pane">
			                    <!-- <h1>百度编辑器</h1> -->
			                    <textarea name="main" id="intro_detail">baidu5s3f1ds321fds321fs32da1f2ds3a1f32dsa1f32dsa1f23dsa1f23ads1f32d1sa2f3a132ad1f32dsa1f23ads1f3dsa</textarea>
			                </div>
			            </div>
			            <!-- 提交 -->
			            <div class="closure">
			                <input type="submit" class="closurestyle" value="确定"/>
			                <span  class="closurestyle Close">取消</span>
			                <input type="reset" class="closurestyle" value="重置" onmousedown="return $EDITORUI[&quot;edui51&quot;]._onMouseDown(event, this);" onclick="return $EDITORUI[&quot;edui51&quot;]._onClick(event, this);"/>
			            </div>
			    </form>
			</div>
	</div>
	<script>



        // 删除按钮
        $(".operationButtom .delete").click(function () {
            var deletesize = new Array(); //想要删除的ID
            var i = 0
            $("body input[name='btSelectItem']").each(function(){
                if( $(this).prop("checked") == true){
                    deletesize[i]  = $(this).parent().siblings("td").attr("id");
                    i ++;
                }
            })
            if ( i > 0){
                var msg = "您真的确定要删除吗？"; 
                if (confirm(msg)==true){ 
                    for(var a = 0 ; a <= i ; a++){
                        var htmls = document.getElementById(deletesize[a]);
                        var $htmls=$(htmls);
                        $htmls.parents("tr").remove()
                    }
                    ids = deletesize;
                    // 定义参数
	                var data = {id:ids};
	                //开启token
	                data['_token'] = $('#token').val();
	                var url = 'alldeletesize';
	                $.post(url,data,function(data){
	                	console.log(data);
	                });
                }else{ 
                    return false; 
                }
            }else{
                alert("请选择第ID")
            }
        })
        // 下架按钮
        $(".operationButtom .lower").click(function () {
            var lowersize = new Array(); //想要下架的ID
            var i = 0
            $("body input[name='btSelectItem']").each(function(){
                if( $(this).prop("checked") == true){
                    lowersize[i]  = $(this).parent().siblings("td").attr("id");
                    i ++;
                }
            })
            if ( i > 0){
                var msg = "您真的确定要下架吗？"; 
                if (confirm(msg)==true){ 
                    for(var a = 0 ; a <= i ; a++){
                        var htmls = document.getElementById(lowersize[a]);
                        var $htmls=$(htmls);
                        $htmls.parents("tr").find("td").attr("data-state","false").find(".two").text("下架");
                        $htmls.parents("tr").find("td .lower").css("display","none");
                        $htmls.parents("tr").find("td .show").css("display","inline-block");
                        $("body input[type='checkbox']").each(function(){
                            $(this).prop("checked",false);
                        })
                    }
                    ids = lowersize;
                    // 定义参数
	                var data = {id:ids};
	                //开启token
	                data['_token'] = $('#token').val();
	                var url = 'alllower';
	                $.post(url,data,function(data){
	                	// console.log(data);
	                });
                }else{ 
                    return false; 
                }
            }else{
                alert("请选择第ID")
            }
        })
        // 显示按钮
        $(".operationButtom .show").click(function () {
            var showsize = new Array(); //想要显示的ID
            var i = 0
            $("body input[name='btSelectItem']").each(function(){
                if( $(this).prop("checked") == true){
                    showsize[i]  = $(this).parent().siblings("td").attr("id");
                    i ++;
                }
            })
            if ( i > 0){
                var msg = "您真的确定要显示吗？"; 
                if (confirm(msg)==true){ 
                    for(var a = 0 ; a <= i ; a++){
                        var htmls = document.getElementById(showsize[a]);
                        var $htmls=$(htmls);
                        $htmls.parents("tr").find("td").attr("data-state","true").find(".two").text("正常");
                        $htmls.parents("tr").find("td .show").css("display","none");
                        $htmls.parents("tr").find("td .lower").css("display","inline-block");
                        $("body input[type='checkbox']").each(function(){
                            $(this).prop("checked",false);
                        })
                    }
                    ids = showsize;
                    // 定义参数
	                var data = {id:ids};
	                //开启token
	                data['_token'] = $('#token').val();
	                var url = 'allshowsize';
	                $.post(url,data,function(data){
	                	// console.log(data);
	                });
                }else{ 
                    return false; 
                }
            }else{
                alert("请选择第ID")
            }
        })
        // 单张图片上传
        function preview(obj) {
            var $obj=$(obj);                //js转jQuery
            var $dom = $obj.parents(".inputPic").siblings(".picShow");
            var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
            if (obj.files && obj.files[0]) {
                var reader = new FileReader();
                reader.onload = function(evt) {
                    $dom.html("<p class='picBox'><img src='" + evt.target.result + "'><i class='iconfont icon-shanchu'></i></p>");
                }
                reader.readAsDataURL(obj.files[0]);
                $obj.siblings("span").html("<i class='iconfont icon-shangchuan'></i>修改图片")
            } else {
                $dom.find("span").remove();
            }
        }
        //多张图片上传
        function previews(obj){
            var $obj=$(obj);                //js转jQuery
            var $dom = $obj.parents(".inputPic").siblings(".picShow");
            var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
            if (obj.files) {
                for(i = 0; i < obj.files.length ; i ++) {
                    var reader = new FileReader();    
                    reader.readAsDataURL(obj.files[i]);  
                    reader.onload=function(e){  
                        //多图预览
                        $dom.append("<p class='picBox'><img src='" + this.result + "'><i class='iconfont icon-shanchu'></i></p>");
                    }
                }
            } else {
                $dom.html(domhtml);
            }
        }
        // 图片删除
        $(".picShow").on("click",".picBox i",function(){
        	document.getElementById('shanbushan').value = 'shan';
            $(this).parents(".picShow").siblings(".inputPic").find(".upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
            $(this).parent(".picBox").remove();
            $("input[name='photoone']").each(function () {
                $(this).after($(this).clone().val(""));
                $(this).remove();
            })
        })
        
        // // 新增按钮
        // $(".add").click(function () {
        //     $(".AddEdit .boxTitle").text("新增");
        //     $(".AddEdit .editBox").css("display","none");
        //     $(".AddEdit .addBox").css("display","block");
        //     $(".closure input[type='reset']").css("display"," inline-block");
        //     $(".AddEdit").css("opacity","1").css("width","800px").css("margin-left","-400px").css("height","700px").css("margin-top","-350px");
        // })
        // 单个显示
        $(".operationTable .show").click(function () {
        	id = $(this).attr("data-shu");
            var msg = "您真的确定要显示吗？"; 
            if (confirm(msg)==true){ 
                $(this).parents("tr").find("td .show").css("display","none");
                $(this).parents("tr").find("td .lower").css("display","inline-block");
                $(this).parents("tr").find("td").attr("data-state","true").find(".two").text("正常");
                // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'ajaxedit';
                $.post(url,data,function(data){
                	// console.log(data);
                });
            }else{ 
                return false; 
            }
        })
        // 单个下架
        $(".operationTable .lower").click(function () {
        	id = $(this).attr("data-shu");
            var msg = "您真的确定要下架吗？"; 
            if (confirm(msg)==true){ 
                $(this).parents("tr").find("td .lower").css("display","none");
                $(this).parents("tr").find("td .show").css("display","inline-block");
                $(this).parents("tr").find("td").attr("data-state","false").find(".two").text("下架");
                // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'ajaxedit';
                $.post(url,data,function(data){
                	// console.log(data);
                });
            }else{ 
                return false; 
            }
        })
        // 单个删除
        $(".operationTable .delete").click(function () {
        	id = $(this).attr('data-shu');
            var msg = "您真的确定要删除吗？"; 
            if (confirm(msg)==true){ 
                $(this).parents("tr").remove();
                // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'ajaxdel';
                $.post(url,data,function(data){
                	// console.log(data);
                });
            }else{ 
                return false; 
            }
        })
        // // 单个编辑
        // $(".operationTable .edit").click(function () {
        //     $(".AddEdit .boxTitle").text($(this).parents("tr").find(".titleItem").text());
        //     $(".AddEdit .addBox").css("display","none");
        //     $(".AddEdit .editBox").css("display","block");
        //     $(".closure input[type='reset']").css("display","none");
        //     $(".AddEdit").css("opacity","1").css("width","800px").css("margin-left","-400px").css("height","700px").css("margin-top","-350px");
        //     id = $(this).attr('data-shu');
        //     document.getElementById('yaoyong').value = id;
        //     // 定义参数
        //         var data = {id:id};
        //         //开启token
        //         data['_token'] = $('#token').val();
        //         var url = 'newsajaxedit';
        //         $.post(url,data,function(data){
        //             // console.log(data);
        //             if (data['status'] == 'false') {
        //                 $('#true').removeProp('checked');
        //                 $('#false').prop('checked', 'checked');
        //             }
        //             var element = document.getElementById('myimage');
        //             element.src = data['icon'];
        //             document.getElementById('title').value = data['title'];
        //             document.getElementById('miaoshu').value = data['miaoshu'];
        //             document.getElementById('phone').value = data['phone'];
        //         });
        // })
        
    </script>
@endsection
 
