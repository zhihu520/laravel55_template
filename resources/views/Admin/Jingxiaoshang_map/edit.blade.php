@extends('layouts.ad')
@section('content')
		<div class="fr conRight">
			<p class="breadcrumb"><span>赛普主页</span>/<span>新闻列表</span>/<span>新增</span></p>
			<!-- 操作区域 -->
			<div class="editBox">
				<ul class="editTab clearfix">
					<li class="fl lihover"><span>内容</span></li>
				</ul>
				<form action="{{$_SESSION['web']['houtai']}}dojingxiaoshang_map_edit" method="post" enctype="multipart/form-data" class="addBox" onsubmit="return zuzhitijiao()">
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
					<input type="hidden" name="id" value="{{$info->id}}">
					<input type="hidden" id="sheng" value="{{$info->sheng}}">
					<input type="hidden" id="city1" value="{{$info->city}}">
					<ul class="editCon">
						<li>
							<!-- type=text的名为textgroup -->
							<table class="textgroup">
								<tr> 
									<td class="title required">城市选择<span>*</span>：</td>
									<td class="text">
										<select id="province" size=1 onchange="getCity()" name="sheng">
											<?php if ($info->sheng == 0): ?>
											 	<option value="0" selected>北京</option>
											 <?php else: ?>
											 	<option value="0" >北京</option>
											 <?php endif ?> 
											 <?php if ($info->sheng == 1): ?>
											 	<option value="1" selected>上海</option>
											 <?php else: ?>
											 	<option value="1" >上海</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 2): ?>
											 	<option value="2" selected>天津</option> 
											 <?php else: ?>
											 	<option value="2" >天津</option> 
											 <?php endif ?>
											 <?php if ($info->sheng == 3): ?>
											 	<option value="3" selected>重庆</option>
											 <?php else: ?>
											 	<option value="3" >重庆</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 4): ?>
											 	<option value="4" selected>河北</option>
											 <?php else: ?>
											 	<option value="4" >河北</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 5): ?>
											 	<option value="5" selected>山西</option>
											 <?php else: ?>
											 	<option value="5" >山西</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 6): ?>
											 	<option value="6" selected>内蒙古</option>
											 <?php else: ?>
											 	<option value="6" >内蒙古</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 7): ?>
											 	<option value="7" selected>辽宁</option>
											 <?php else: ?>
											 	<option value="7" >辽宁</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 8): ?>
											 	<option value="8" selected>吉林</option>
											 <?php else: ?>
											 	<option value="8" >吉林</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 9): ?>
											 	<option value="9" selected>黑龙江</option>
											 <?php else: ?>
											 	<option value="9" >黑龙江</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 10): ?>
											 	<option value="10" selected>江苏</option>
											 <?php else: ?>
											 	<option value="10" >江苏</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 11): ?>
											 	<option value="11" selected>浙江</option>
											 <?php else: ?>
											 	<option value="11" >浙江</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 12): ?>
											 	<option value="12" selected>安徽</option> 
											 <?php else: ?>
											 	<option value="12" >安徽</option> 
											 <?php endif ?>
											 <?php if ($info->sheng == 13): ?>
											 	<option value="13" selected>福建</option>
											 <?php else: ?>
											 	<option value="13" >福建</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 14): ?>
											 	<option value="14" selected>江西</option>
											 <?php else: ?>
											 	<option value="14" >江西</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 15): ?>
											 	<option value="15" selected>山东</option>
											 <?php else: ?>
											 	<option value="15" >山东</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 16): ?>
											 	<option value="16" selected>河南</option>
											 <?php else: ?>
											 	<option value="16" >河南</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 17): ?>
											 	<option value="17" selected>湖北</option>
											 <?php else: ?>
											 	<option value="17" >湖北</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 18): ?>
											 	<option value="18" selected>湖南</option>
											 <?php else: ?>
											 	<option value="18" >湖南</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 19): ?>
											 	<option value="19" selected>广东</option> 
											 <?php else: ?>
											 	<option value="19" >广东</option> 
											 <?php endif ?>
											 <?php if ($info->sheng == 20): ?>
											 	<option value="20" selected>广西</option> 
											 <?php else: ?>
											 	<option value="20" >广西</option> 
											 <?php endif ?>
											 <?php if ($info->sheng == 21): ?>
											 	<option value="21" selected>海南</option>
											 <?php else: ?>
											 	<option value="21" >海南</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 22): ?>
											 	<option value="22" selected>四川</option>
											 <?php else: ?>
											 	<option value="22" >四川</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 23): ?>
											 	<option value="23" selected>贵州</option>
											 <?php else: ?>
											 	<option value="23" >贵州</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 24): ?>
											 	<option value="24" selected>云南</option>
											 <?php else: ?>
											 	<option value="24" >云南</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 25): ?>
											 	<option value="25" selected>西藏</option>
											 <?php else: ?>
											 	<option value="25" >西藏</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 26): ?>
											 	<option value="26" selected>陕西</option>
											 <?php else: ?>
											 	<option value="26" >陕西</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 27): ?>
											 	<option value="27" selected>甘肃</option> 
											 <?php else: ?>
											 	<option value="27" >甘肃</option> 
											 <?php endif ?>
											 <?php if ($info->sheng == 28): ?>
											 	<option value="28" selected>宁夏</option>
											 <?php else: ?>
											 	<option value="28" >宁夏</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 29): ?>
											 	<option value="29" selected>青海</option>
											 <?php else: ?>
											 	<option value="29" >青海</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 30): ?>
											 	<option value="30" selected>新疆</option>
											 <?php else: ?>
											 	<option value="30" >新疆</option>
											 <?php endif ?>
											 <?php if ($info->sheng == 31): ?>
											 	<option value="31" selected>香港</option> 
											 <?php else: ?>
											 	<option value="31" >香港</option> 
											 <?php endif ?>
											 <?php if ($info->sheng == 32): ?>
											 	<option value="32" selected>澳门</option> 
											 <?php else: ?>
											 	<option value="32" >澳门</option> 
											 <?php endif ?>
											 <?php if ($info->sheng == 33): ?>
											 	<option value="33" selected>台湾</option>
											 <?php else: ?>
											 	<option value="33" >台湾</option>
											 <?php endif ?>
										</select>
										<select id="city" style="width:60px" name="city"> 
										</select>
									</td>
								</tr>
								<tr>
									<td class="title required">地址<span>*</span>：</td>
									<td class="text"><input type="text" value="{{$info->address}}" name="address" id="biaoti"></td>
									<td class="prompt">地址为必填</td>
								</tr>
								<tr>
									<td class="title required">电话<span>*</span>：</td>
									<td class="text"><input type="text" value="{{$info->phone}}" name="phone" id="biaoti"></td>
									<td class="prompt">电话为必填</td>
								</tr>
								<tr>
									<td class="title required">连接<span>*</span>：</td>
									<td class="text"><input type="text" value="{{$info->lianjie}}" name="lianjie" id="biaoti"></td>
									<td class="prompt">连接为必填</td>
								</tr>
							</table>
							<!-- type=radio的名为radiogroup -->
								<?php if ($info->status == 'true'): ?>
									<div class="radiogroup clearfix">
										<p class="title fl">状态：</p>
										<div class="radioBox">
											<p>
												<input type="radio" name="status" id="true" checked="" value="true" />
												<label for="true">正常</label>
											</p>
											<p>
												<input type="radio" name="status" id="false" value="false" />
												<label for="false">下架</label>
											</p>
										</div>
									</div>
								<?php else: ?>
									<div class="radiogroup clearfix">
										<p class="title fl">状态：</p>
										<div class="radioBox">
											<p>
												<input type="radio" name="status" id="true" value="true" />
												<label for="true">正常</label>
											</p>
											<p>
												<input type="radio" name="status" id="false" value="false" checked="" />
												<label for="false">下架</label>
											</p>
										</div>
									</div>
								<?php endif ?>
						</li>
					</ul>
					<!-- 提交 -->
					<div class="closure">
						<input type="submit" class="closurestyle" value="确定" />
						<a href="javascript:history.go(-1);" class="closurestyle Close">取消</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script>
	// 操作区域的最大高度
	var boxheightqq = $(".editBox").outerHeight(true);
	var edittabheightqq = $(".editTab").outerHeight(true);
	var closureheightqq = $(".editBox .closure").outerHeight(true);
		// alert(boxheightqq-edittabheightqq-closureheightqq-55)
	$(".editBox .editCon").css("max-height",boxheightqq-edittabheightqq-closureheightqq-55);
		// 单张图片上传
		function preview(obj) {
			var $obj=$(obj);				//js转jQuery
			var $dom = $obj.parents(".inputPic").siblings(".picShow");
			var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
			if (obj.files && obj.files[0]) {
				var reader = new FileReader();
				reader.onload = function(evt) {
					$dom.html("<p class='picBox'><img src='" + evt.target.result + "'><i class='iconfont icon-shanchu'></i></p>");
				}
				reader.readAsDataURL(obj.files[0]);
				$obj.siblings("span").html("<i class='iconfont icon-shangchuan'></i>修改图片")
			} else {
				$dom.find("span").remove();
			}
		}
		//多张图片上传
			// 一个多图一份这个，picssrc进行改变；并且把函数放到点击事件里
			var picssrc = new Array();
			function picssrcfun(){
				var size =$(".picsUploading .picShow[name='picssrc'] .picBox").size();
				for (var a = 0; a < size; a++) {
					picssrc[a] = $(".picsUploading .picShow[name='picssrc'] .picBox").eq(a).find("img").attr("src");
				}
			}
			$(".editBox .closure .input[type='submit'] ").click(function(){
				picssrcfun()
			})
		function previews(obj){
			var $obj=$(obj);				//js转jQuery
			var $dom = $obj.parents(".inputPic").siblings(".picShow");
			var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
			if (obj.files) {
				for(i = 0; i < obj.files.length ; i ++) {
					var reader = new FileReader();    
					reader.readAsDataURL(obj.files[i]);  
					reader.onload=function(e){  
						//多图预览
						$dom.append("<p class='picBox'><img src='" + this.result + "'><i class='iconfont icon-shanchu'></i></p>");
					}
				}
			} else {
				$dom.html(domhtml);
			}
		}
		// 图片删除
		$(".picShow").on("click",".picBox i",function(){
			$(this).parents(".picShow").siblings(".inputPic").find(".upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
			$(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").after($(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").clone().val(""));
			$(this).parent(".picBox").remove();
		})
		// 重置
		$(".closure input[type='reset']").click(function(){
			$(".addPic .picShow").html("");
			$("input[type='file']").each(function () {
				$(this).after($(this).clone().val(""));
				// $(this).remove();
			})
			$(this).parent(".closure").siblings(".AddEditBottom").find(".addPic .upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
		})
		//提示的显示
		$("input").focusout(function(){
			if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
				$(this).parent("td").siblings(".prompt").css("opacity","1");
			}else{
				$(this).parent("td").siblings(".prompt").css("opacity","0");
			}
		})
		$("textarea").focusout(function(){
			if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
				$(this).parent("td").siblings(".prompt").css("opacity","1");
			}else{
				$(this).parent("td").siblings(".prompt").css("opacity","0");
			}
		})
		// 操作区域界面
		$(".editTab li").click(function(){
			$(this).addClass("lihover").siblings().removeClass("lihover");
			$(".editBox .editCon li").eq($(this).index()).css("display","block").siblings().css("display","none");
		})
		//表单提交
		function zuzhitijiao(){
			biaoti = document.getElementById('biaoti').value;
			if (biaoti) {
				return true;
			}else{
				alert('地址必填');
				return false;
			}
		}
	</script>
	</select> 
<script> 
	var arr = new  Array();
	arr[0 ]="东城,西城,崇文,宣武,朝阳,丰台,石景山,海淀,门头沟,房山,通州,顺义,昌平,大兴,平谷,怀柔,密云,延庆" 
	arr[1 ]="黄浦,卢湾,徐汇,长宁,静安,普陀,闸北,虹口,杨浦,闵行,宝山,嘉定,浦东,金山,松江,青浦,南汇,奉贤,崇明" 
	arr[2 ]="和平,东丽,河东,西青,河西,津南,南开,北辰,河北,武清,红挢,塘沽,汉沽,大港,宁河,静海,宝坻,蓟县" 
	arr[3 ]="万州,涪陵,渝中,大渡口,江北,沙坪坝,九龙坡,南岸,北碚,万盛,双挢,渝北,巴南,黔江,长寿,綦江,潼南,铜梁,大足,荣昌,壁山,梁平,城口,丰都,垫江,武隆,忠县,开县,云阳,奉节,巫山,巫溪,石柱,秀山,酉阳,彭水,江津,合川,永川,南川" 
	arr[4 ]="石家庄,邯郸,邢台,保定,张家口,承德,廊坊,唐山,秦皇岛,沧州,衡水" 
	arr[5 ]="太原,大同,阳泉,长治,晋城,朔州,吕梁,忻州,晋中,临汾,运城" 
	arr[6 ]="呼和浩特,包头,乌海,赤峰,呼伦贝尔盟,阿拉善盟,哲里木盟,兴安盟,乌兰察布盟,锡林郭勒盟,巴彦淖尔盟,伊克昭盟" 
	arr[7 ]="沈阳,大连,鞍山,抚顺,本溪,丹东,锦州,营口,阜新,辽阳,盘锦,铁岭,朝阳,葫芦岛" 
	arr[8 ]="长春,吉林,四平,辽源,通化,白山,松原,白城,延边" 
	arr[9 ]="哈尔滨,齐齐哈尔,牡丹江,佳木斯,大庆,绥化,鹤岗,鸡西,黑河,双鸭山,伊春,七台河,大兴安岭" 
	arr[10 ]="南京,镇江,苏州,南通,扬州,盐城,徐州,连云港,常州,无锡,宿迁,泰州,淮安" 
	arr[11 ]="杭州,宁波,温州,嘉兴,湖州,绍兴,金华,衢州,舟山,台州,丽水" 
	arr[12 ]="合肥,芜湖,蚌埠,马鞍山,淮北,铜陵,安庆,黄山,滁州,宿州,池州,淮南,巢湖,阜阳,六安,宣城,亳州" 
	arr[13 ]="福州,厦门,莆田,三明,泉州,漳州,南平,龙岩,宁德" 
	arr[14 ]="南昌市,景德镇,九江,鹰潭,萍乡,新馀,赣州,吉安,宜春,抚州,上饶" 
	arr[15 ]="济南,青岛,淄博,枣庄,东营,烟台,潍坊,济宁,泰安,威海,日照,莱芜,临沂,德州,聊城,滨州,菏泽" 
	arr[16 ]="郑州,开封,洛阳,平顶山,安阳,鹤壁,新乡,焦作,濮阳,许昌,漯河,三门峡,南阳,商丘,信阳,周口,驻马店,济源" 
	arr[17 ]="武汉,宜昌,荆州,襄樊,黄石,荆门,黄冈,十堰,恩施,潜江,天门,仙桃,随州,咸宁,孝感,鄂州" 
	arr[18 ]="长沙,常德,株洲,湘潭,衡阳,岳阳,邵阳,益阳,娄底,怀化,郴州,永州,湘西,张家界" 
	arr[19 ]="广州,深圳,珠海,汕头,东莞,中山,佛山,韶关,江门,湛江,茂名,肇庆,惠州,梅州,汕尾,河源,阳江,清远,潮州,揭阳,云浮" 
	arr[20 ]="南宁,柳州,桂林,梧州,北海,防城港,钦州,贵港,玉林,南宁地区,柳州地区,贺州,百色,河池" 
	arr[21 ]="海口,三亚" 
	arr[22 ]="成都,绵阳,德阳,自贡,攀枝花,广元,内江,乐山,南充,宜宾,广安,达川,雅安,眉山,甘孜,凉山,泸州" 
	arr[23 ]="贵阳,六盘水,遵义,安顺,铜仁,黔西南,毕节,黔东南,黔南" 
	arr[24 ]="昆明,大理,曲靖,玉溪,昭通,楚雄,红河,文山,思茅,西双版纳,保山,德宏,丽江,怒江,迪庆,临沧" 
	arr[25 ]="拉萨,日喀则,山南,林芝,昌都,阿里,那曲" 
	arr[26 ]="西安,宝鸡,咸阳,铜川,渭南,延安,榆林,汉中,安康,商洛" 
	arr[27 ]="兰州,嘉峪关,金昌,白银,天水,酒泉,张掖,武威,定西,陇南,平凉,庆阳,临夏,甘南" 
	arr[28 ]="银川,石嘴山,吴忠,固原" 
	arr[29 ]="西宁,海东,海南,海北,黄南,玉树,果洛,海西" 
	arr[30 ]="乌鲁木齐,石河子,克拉玛依,伊犁,巴音郭勒,昌吉,克孜勒苏柯尔克孜,博 尔塔拉,吐鲁番,哈密,喀什,和田,阿克苏" 
	arr[31 ]="香港" 
	arr[32 ]="澳门" 
	arr[33 ]="台北,高雄,台中,台南,屏东,南投,云林,新竹,彰化,苗栗,嘉义,花莲,桃园,宜兰,基隆,台东,金门,马祖,澎湖" 

	init();
	function init()
	{
	    var city = document.getElementById("city");
	    var sheng = document.getElementById("sheng");
	    var cityArr = arr[sheng.value].split(",");
	    for(var i=0;i<cityArr.length;i++)
	    {
	        city[i]=new Option(cityArr[i],cityArr[i]);
	    }
	}

	function getCity()
	{    
	    var pro = document.getElementById("province");
	    var city = document.getElementById("city");
	    var index = pro.selectedIndex;
	    var cityArr = arr[index].split(",");   
	    city.length = 0;
	    //将城市数组中的值填充到城市下拉框中
	    for(var i=0;i<cityArr.length;i++)
	    {
	        city[i]=new Option(cityArr[i],cityArr[i]);
	    }
	}
	var city1 = document.getElementById("city1");
	var city = document.getElementById("city");
	var option = city.childNodes;
	for (var i = option.length - 1; i > 0; i--) {
		if (option[i].value == city1.value) {
			option[i].setAttribute('selected','selected');
		}
	}
</script>
@endsection
 