@extends('layouts.ad')
@section('content')
		<div class="fr conRight">
			<p class="breadcrumb"><span>产品管理</span>/<span>产品列表</span>/<span>编辑</span></p>
			<!-- 操作区域 -->
			<div class="editBox">
				<ul class="editTab clearfix">
					<li class="fl lihover"><span>内容</span></li>
					<li class="fl"><span>图片管理</span></li>
					<li class="fl"><span>详细内容</span></li>
				</ul>
				<form action="{{$_SESSION['web']['houtai']}}doshop_list_add" method="post" enctype="multipart/form-data" class="addBox" onsubmit="return zuzhitijiao()">
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
					<ul class="editCon">
						<li>
							<!-- type=text的名为textgroup -->
							<table class="textgroup">
								<tr>
									<td class="title required">产品型号<span>*</span>：</td>
									<td class="text"><input type="text" value="" name="xinghao" id="biaoti"></td>
									<td class="prompt">产品型号为必填</td>
								</tr>
								<tr>
									<td class="title required">说明<span>*</span>：</td>
									<td class="text"><input type="text" value="" name="shuoming" id="biaoti"></td>
								</tr>
								<tr>
									<td class="title required">规格<span>*</span>：</td>
									<td class="text"><input type="text" value="" name="guige" id="biaoti"></td>
								</tr>
								<tr>
									<td class="title required">颜色<span>*</span>：</td>
									<td class="text"><input type="text" value="" name="guige_shuzi" id="biaoti"></td>
								</tr>
								<tr>
									<td class="title required">净重<span>*</span>：</td>
									<td class="text"><input type="text" value="" name="yongcai" id="biaoti"></td>
								</tr>
								<tr>
									<td class="title required">价格<span>*</span>：</td>
									<td class="text"><input type="number" value="" name="jiage" id="biaoti"></td>
								</tr>
							</table>
							<!-- type=radio的名为radiogroup -->
							<div class="radiogroup clearfix">
								<p class="title fl">品牌：</p>
								<div class="radioBox">
				                    <select name="pinpai_id" id="pinpai"  onchange="pinpaichange(this.value)">
				                    		<option value="0">请选择</option>
				                    	@foreach($pinpai as $v)
											<option value="{{$v->id}}">{{$v->title_z}}</option>
										@endforeach
				                    </select>
								</div>
							</div>
<script>
	function pinpaichange(id){
		$("#xilie").empty();
		var xilie_div = document.getElementById('xilie_div');
		xilie_div.style.display = 'block';
		// 定义参数
        var data = {id:id};
        //开启token
        data['_token'] = $('#token').val();
        var url = 'admin_shop_list_pinpaichange';
        $.post(url,data,function(data){
        	// console.log(data);
	        if (data == 0) {
	        	var option = document.createElement("option");    
	            option.text= '暂无数据';    
	            option.value= 0;  
	            xilie.add(option);
	        }else{
	        	for (var i = data.length; i >= 0; i--) {
	                if (i == data.length) {
	                    var option = document.createElement("option");    
	                    option.text= '请选择';    
	                    option.value= 0;  
	                    xilie.add(option);
	                }else{
	                    var option = document.createElement("option");    
	                    option.text= data[i]['title'];    
	                    option.value= data[i]['id'];  
	                    xilie.add(option); 
	                }
	            }
	        }
        });
	}
</script>
							<div class="radiogroup clearfix" style="display: none;" id="xilie_div">
								<p class="title fl">系列：</p>
								<div class="radioBox">
				                    <select name="xilie_id" id="xilie" onchange="xiliechange(this.value)">
										<option value="0">请选择</option>
				                    </select>
								</div>
							</div>
<script>
	function xiliechange(id){
		$("#fenlei").empty();
		var fenlei_div = document.getElementById('fenlei_div');
		fenlei_div.style.display = 'block';
		// 定义参数
        var data = {id:id};
        //开启token
        data['_token'] = $('#token').val();
        var url = 'admin_shop_list_fenleichange';
        $.post(url,data,function(data){
        	console.log(data);
	        if (data == 0) {
	        	var option = document.createElement("option");    
	            option.text= '暂无数据';    
	            option.value= 0;  
	            fenlei.add(option);
	        }else{
	        	for (var i = data.length; i >= 0; i--) {
	                if (i == data.length) {
	                    var option = document.createElement("option");    
	                    option.text= '请选择';    
	                    option.value= 0;  
	                    fenlei.add(option);
	                }else{
	                    var option = document.createElement("option");    
	                    option.text= data[i]['title'];    
	                    option.value= data[i]['id'];  
	                    fenlei.add(option); 
	                }
	            }
	        }
        });
	}
</script>
							<div class="radiogroup clearfix" style="display: none;" id="fenlei_div">
								<p class="title fl">分类：</p>
								<div class="radioBox">
				                    <select name="fenlei_id" id="fenlei">
										<option value="0">请选择</option>
				                    </select>
								</div>
							</div>
							<div class="radiogroup clearfix">
								<p class="title fl">状态：</p>
								<div class="radioBox">
									<p>
										<input type="radio" name="status" id="true" checked="" value="true" />
										<label for="true">正常</label>
									</p>
									<p>
										<input type="radio" name="status" id="false" value="false" />
										<label for="false">下架</label>
									</p>
								</div>
							</div>
						</li>
						<li>
							<!-- 新增的图片 -->
							<div class="addPic">
								<!-- 单张图片上传 -->
								<div class="picUploading">
									<div class="inputPic clearfix">
										<p class="title fl">单张图片：</p>
										<p class="upload fl">
											<span><i class="iconfont icon-shangchuan"></i>上传图片</span>
											<input type="file" date-shu="one" name="icon" onchange="preview(this)" />
										</p>
										<p class="prompt fl">建议大小:540 x 400 px</p>
									</div>
									<div class="picShow">
										<p class="picBox">
										</p>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="tab-pane">
								<!-- <h1>百度编辑器</h1> -->
								<textarea name="main" id="intro_detail"></textarea>
							</div>
						</li>
					</ul>
					<!-- 提交 -->
					<div class="closure">
						<input type="submit" class="closurestyle" value="确定" />
						<a href="javascript:history.go(-1);" class="closurestyle Close">取消</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script>
	// 操作区域的最大高度
	var boxheightqq = $(".editBox").outerHeight(true);
	var edittabheightqq = $(".editTab").outerHeight(true);
	var closureheightqq = $(".editBox .closure").outerHeight(true);
		// alert(boxheightqq-edittabheightqq-closureheightqq-55)
	$(".editBox .editCon").css("max-height",boxheightqq-edittabheightqq-closureheightqq-55);
		// 单张图片上传
		function preview(obj) {
			var $obj=$(obj);				//js转jQuery
			var $dom = $obj.parents(".inputPic").siblings(".picShow");
			var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
			if (obj.files && obj.files[0]) {
				var reader = new FileReader();
				reader.onload = function(evt) {
					$dom.html("<p class='picBox'><img src='" + evt.target.result + "'><i class='iconfont icon-shanchu'></i></p>");
				}
				reader.readAsDataURL(obj.files[0]);
				$obj.siblings("span").html("<i class='iconfont icon-shangchuan'></i>修改图片")
				shanbushan = document.getElementById('shanchu');
				shanbushan.value = 'bushan';
			} else {
				$dom.find("span").remove();
			}
		}
		//多张图片上传
			// 一个多图一份这个，picssrc进行改变；并且把函数放到点击事件里
			var picssrc = new Array();
			function picssrcfun(){
				var size =$(".picsUploading .picShow[name='picssrc'] .picBox").size();
				for (var a = 0; a < size; a++) {
					picssrc[a] = $(".picsUploading .picShow[name='picssrc'] .picBox").eq(a).find("img").attr("src");
				}
			}
			$(".editBox .closure .input[type='submit'] ").click(function(){
				picssrcfun()
			})
		function previews(obj){
			var $obj=$(obj);				//js转jQuery
			var $dom = $obj.parents(".inputPic").siblings(".picShow");
			var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
			if (obj.files) {
				for(i = 0; i < obj.files.length ; i ++) {
					var reader = new FileReader();    
					reader.readAsDataURL(obj.files[i]);  
					reader.onload=function(e){  
						//多图预览
						$dom.append("<p class='picBox'><img src='" + this.result + "'><i class='iconfont icon-shanchu'></i></p>");
					}
				}
			} else {
				$dom.html(domhtml);
			}
		}
		// 图片删除
		$(".picShow").on("click",".picBox i",function(){
			$(this).parents(".picShow").siblings(".inputPic").find(".upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
			$(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").after($(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").clone().val(""));
			$(this).parent(".picBox").remove();
			shanbushan = document.getElementById('shanchu');
			shanbushan.value = 'shan';
		})
		// 重置
		$(".closure input[type='reset']").click(function(){
			$(".addPic .picShow").html("");
			$("input[type='file']").each(function () {
				$(this).after($(this).clone().val(""));
				// $(this).remove();
			})
			$(this).parent(".closure").siblings(".AddEditBottom").find(".addPic .upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
		})
		//提示的显示
		$("input").focusout(function(){
			if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
				$(this).parent("td").siblings(".prompt").css("opacity","1");
			}else{
				$(this).parent("td").siblings(".prompt").css("opacity","0");
			}
		})
		$("textarea").focusout(function(){
			if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
				$(this).parent("td").siblings(".prompt").css("opacity","1");
			}else{
				$(this).parent("td").siblings(".prompt").css("opacity","0");
			}
		})
		// 操作区域界面
		$(".editTab li").click(function(){
			$(this).addClass("lihover").siblings().removeClass("lihover");
			$(".editBox .editCon li").eq($(this).index()).css("display","block").siblings().css("display","none");
		})
		//表单提交
		function zuzhitijiao(){
			biaoti = document.getElementById('biaoti').value;
			xilie = document.getElementById('xilie').value;
			fenlei = document.getElementById('fenlei').value;
			return true;
		}
	</script>
@endsection
 