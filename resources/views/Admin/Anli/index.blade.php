@extends('layouts.ad')
@section('content')
	<div class="fr conRight">
			<p class="breadcrumb"><span>主页</span>/<span>案例列表</span></p>
            <input type="hidden" name="table" id="table" value="anli">
			<div class="operationBox">
				<p class="operationButtom">
					<a href="{{$_SESSION['web']['houtai']}}anli_add" class="add"><i class="iconfont icon-plus"></i>新增</a>
					<span class="show"><i class="iconfont icon-xianshi"></i>显示</span>
					<span class="lower"><i class="iconfont icon-xiajia"></i>下架</span>
					<span class="delete"><i class="iconfont icon-shanchu"></i>删除</span>
				</p>
				<div class="operationTable">
					<table>
						<tr>
							<th width="5%"><input name="btSelectAll" type="checkbox" checked=""></th>
							<th width="5%">ID</th>
							<th width="18%">图片</th>
							<th width="5%">标题</th>
							<th width="15%">创建时间</th>
							<th width="15%">修改时间</th>
							<th width="10%">状态</th>
							<th width="16%">操作</th>
						</tr>
						@foreach($info as $v)
							<tr>
								<td><input name="btSelectItem" type="checkbox"></td>
								<td id="{{$v->id}}">{{$v->id}}</td>
								<td><img src="{{$v->icon}}" alt="" width="50" height="50"></td>
								<td class="titleItem">{{$v->title}}</td>
								<td>{{date('Y-m-d',$v->create_time)}}</td>
								<td>{{date('Y-m-d',$v->update_time)}}</td>
								<td class="state" data-state="{{$v->status}}"><span class="one"></span><span class="two"></span></td>
								<td>
									<a href="{{$_SESSION['web']['houtai']}}anli_edit?id={{$v->id}}"><i class="iconfont icon-liuyan edit" data-shu="{{$v->id}}"></i></a>
									<i class="iconfont icon-shanchu delete" onclick="del_one(this,{{$v->id}})"></i>
									<i class="iconfont icon-xiajia lower" onclick="lower_one(this,{{$v->id}})"></i>
									<i class="iconfont icon-xianshi show" onclick="show_one(this,{{$v->id}})"></i>
								</td>
							</tr>
						@endforeach
					</table>
					{{$info->links()}}
				</div>
			</div>
	</div>
@endsection
