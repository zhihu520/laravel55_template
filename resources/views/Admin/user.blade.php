@extends('layouts.ad')
@section('content')
	<div class="fr conRight">
            <p class="breadcrumb"><span>赛普主页</span>/<span>用户管理</span></p>
            <div class="operationBox">
                <p class="operationButtom">
                    <?php if (in_array("useradd",$_COOKIE['admin']['menu_auth'])): ?>
                        <span class="add"><i class="iconfont icon-plus"></i>新增</span>
                    <?php endif ?>
                    <?php if (in_array("usershow",$_COOKIE['admin']['menu_auth'])): ?>
                        <span class="show"><i class="iconfont icon-xianshi"></i>显示</span>
                    <?php endif ?>
                    <?php if (in_array("userlow",$_COOKIE['admin']['menu_auth'])): ?>
                        <span class="lower"><i class="iconfont icon-xiajia"></i>下架</span>
                    <?php endif ?>
                    <?php if (in_array("userdelete",$_COOKIE['admin']['menu_auth'])): ?>
                        <span class="delete"><i class="iconfont icon-shanchu"></i>删除</span>
                    <?php endif ?>
                </p>
                <div class="operationTable">
                    <table>
                        <tr>
                            <th width="5%"><input name="btSelectAll" type="checkbox" checked=""></th>
                            <th width="5%">ID</th>
                            <th width="18%">角色名称</th>
                            <th width="18%">用户名</th>
                            <th width="15%">创建时间</th>
                            <th width="15%">修改时间</th>
                            <th width="10%">状态</th>
                            <th width="16%">操作</th>
                        </tr>
                        @foreach($info as $v)
                            <tr>
                                <td><input name="btSelectItem" type="checkbox"></td>
                                <td id="{{$v->id}}">{{$v->id}}</td>
                                <td class="titleItem">{{$v->fenzu}}</td>
                                <td>{{$v->username}}</td>
                                <td>{{date('Y-m-d',$v->create_time)}}</td>
                                <td>{{date('Y-m-d',$v->update_time)}}</td>
                                <td class="state" data-state="{{$v->status}}"><span class="one"></span><span class="two"></span></td>
                                <td>
                                    <?php if (in_array("useredit",$_COOKIE['admin']['menu_auth'])): ?>
                                        <i class="iconfont icon-liuyan edit" data-shu="{{$v->id}}"></i>
                                    <?php endif ?>
                                    <?php if (in_array("userdelete",$_COOKIE['admin']['menu_auth'])): ?>
                                        <i class="iconfont icon-shanchu delete" data-shu="{{$v->id}}"></i>
                                    <?php endif ?>
                                    <?php if (in_array("userlow",$_COOKIE['admin']['menu_auth'])): ?>
                                        <i class="iconfont icon-xiajia lower" data-shu="{{$v->id}}"></i>
                                    <?php endif ?>
                                    <?php if (in_array("usershow",$_COOKIE['admin']['menu_auth'])): ?>
                                        <i class="iconfont icon-xianshi show" data-shu="{{$v->id}}"></i>
                                    <?php endif ?>
                                </td>
                                <!-- <a href="javascript:delNews({{$v->id}})"><input type="button" class="btn btn-danger" value="删除"></a> -->
                            </tr>
                        @endforeach
                    </table>
                    {{$info->links()}}
                </div>
            </div>
            <!-- 新增与修改的弹出框 -->
            <div class="AddEdit">
                <div class="AddEditTop clearfix">
                    <p class="boxTitle fl ovhid">新增</p>
                    <p class="boxClose Close fr"><i class="iconfont icon-guanbi-"></i></p>
                </div>
                <!-- 新增与修改的提交 -->
                <form action="{{url('useraddedit')}}" method="post" enctype="multipart/form-data" class="addBox" onsubmit="return kankan()">
                    <input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
                    <input type="hidden" name="addoredit" value="" id="addoredit">
                    <input type="hidden" name="id" value="" id="xuyaodeid">
                    <input type="hidden" name="yaoyongdeid" value="" id="yaoyongdeid">
                    <!-- 新增与修改的提交内容 -->
                    <div class="AddEditBottom">
                        <!-- 字段 -->
                        <div class="field">
                            <!-- type=text的名为textgroup -->
                            <div class="textgroup clearfix">
                                <p class="title fl">所属组：</p>
                                <select name="gid" id="" style="position:relative;left: 30px;width: 490px;height: 30px;">
                                    <option value="0" id="qingxuanze">请选择</option>
                                    @foreach($fenzu as $v)
                                        <option value="{{$v->id}}">{{$v->title}}</option> 
                                    @endforeach 
                                </select>
                            </div>
                            <div class="textgroup clearfix">
                                <p class="title fl">用户名：</p>
                                <input type="text" name="username" id="yonghuming">
                            </div>
                            <div class="textgroup clearfix">
                                <p class="title fl">密码：</p>
                                <input type="password" name="password" id="mima">
                            </div>
                            <!-- type=radio的名为radiogroup -->
                            <div class="radiogroup clearfix">
                                <p class="title fl">状态：</p>
                                <div class="radioBox">
                                    <p>
                                        <input type="radio" name="status" id="true" checked="" value="true" />
                                        <label for="true">正常</label>
                                    </p>
                                    <p>
                                        <input type="radio" name="status" id="false" value="false" />
                                        <label for="false">下架</label>
                                    </p>
                                </div>
                            </div>
                            
                        </div>
                        
                        
                    </div>
                    <!-- 提交 -->
                    <div class="closure">
                        <input type="submit" class="closurestyle" value="确定" />
                        <span  class="closurestyle Close">取消</span>
                        <input type="reset" class="closurestyle" value="重置"/>
                    </div>
                </form>
            </div>
        </div>
    </div>
    <script>
        // 删除按钮
        $(".operationButtom .delete").click(function () {
            var deletesize = new Array(); //想要删除的ID
            var i = 0
            $("body input[name='btSelectItem']").each(function(){
                if( $(this).prop("checked") == true){
                    deletesize[i]  = $(this).parent().siblings("td").attr("id");
                    i ++;
                }
            })
            if ( i > 0){
                var msg = "您真的确定要删除吗？"; 
                if (confirm(msg)==true){ 
                    for(var a = 0 ; a <= i ; a++){
                        var htmls = document.getElementById(deletesize[a]);
                        var $htmls=$(htmls);
                        $htmls.parents("tr").remove()
                    }
                    ids = deletesize;
                    // 定义参数
                    var data = {id:ids};
                    //开启token
                    data['_token'] = $('#token').val();
                    var url = 'alldeletesizeuser';
                    $.post(url,data,function(data){
                        // console.log(data);
                    });
                }else{ 
                    return false; 
                }
            }else{
                alert("请选择第ID")
            }
        })
        // 下架按钮
        $(".operationButtom .lower").click(function () {
            var lowersize = new Array(); //想要下架的ID
            var i = 0
            $("body input[name='btSelectItem']").each(function(){
                if( $(this).prop("checked") == true){
                    lowersize[i]  = $(this).parent().siblings("td").attr("id");
                    i ++;
                }
            })
            if ( i > 0){
                var msg = "您真的确定要下架吗？"; 
                if (confirm(msg)==true){ 
                    for(var a = 0 ; a <= i ; a++){
                        var htmls = document.getElementById(lowersize[a]);
                        var $htmls=$(htmls);
                        $htmls.parents("tr").find("td").attr("data-state","false").find(".two").text("下架");
                        $htmls.parents("tr").find("td .lower").css("display","none");
                        $htmls.parents("tr").find("td .show").css("display","inline-block");
                        $("body input[type='checkbox']").each(function(){
                            $(this).prop("checked",false);
                        })
                    }
                    ids = lowersize;
                    // 定义参数
                    var data = {id:ids};
                    //开启token
                    data['_token'] = $('#token').val();
                    var url = 'allloweruser';
                    $.post(url,data,function(data){
                        // console.log(data);
                    });
                }else{ 
                    return false; 
                }
            }else{
                alert("请选择第ID")
            }
        })
        // 显示按钮
        $(".operationButtom .show").click(function () {
            var showsize = new Array(); //想要显示的ID
            var i = 0
            $("body input[name='btSelectItem']").each(function(){
                if( $(this).prop("checked") == true){
                    showsize[i]  = $(this).parent().siblings("td").attr("id");
                    i ++;
                }
            })
            if ( i > 0){
                var msg = "您真的确定要显示吗？"; 
                if (confirm(msg)==true){ 
                    for(var a = 0 ; a <= i ; a++){
                        var htmls = document.getElementById(showsize[a]);
                        var $htmls=$(htmls);
                        $htmls.parents("tr").find("td").attr("data-state","true").find(".two").text("正常");
                        $htmls.parents("tr").find("td .show").css("display","none");
                        $htmls.parents("tr").find("td .lower").css("display","inline-block");
                        $("body input[type='checkbox']").each(function(){
                            $(this).prop("checked",false);
                        })
                    }
                    ids = showsize;
                    // 定义参数
                    var data = {id:ids};
                    //开启token
                    data['_token'] = $('#token').val();
                    var url = 'allshowsizeuser';
                    $.post(url,data,function(data){
                        // console.log(data);
                    });
                }else{ 
                    return false; 
                }
            }else{
                alert("请选择第ID")
            }
        })
        // 单张图片上传
        function preview(obj) {
            var $obj=$(obj);                //js转jQuery
            var $dom = $obj.parents(".inputPic").siblings(".picShow");
            var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
            if (obj.files && obj.files[0]) {
                var reader = new FileReader();
                reader.onload = function(evt) {
                    $dom.html("<p class='picBox'><img src='" + evt.target.result + "'><i class='iconfont icon-shanchu'></i></p>");
                }
                reader.readAsDataURL(obj.files[0]);
                $obj.siblings("span").html("<i class='iconfont icon-shangchuan'></i>修改图片")
                shanbushan = document.getElementById('shanbushan');
                shanbushan.value = 'bushan';
            } else {
                $dom.find("span").remove();
            }
        }
        // 图片删除
        $(".picShow").on("click",".picBox i",function(){
            shanbushan = document.getElementById('shanbushan');
            shanbushan.value = 'shan';
            $(this).parents(".picShow").siblings(".inputPic").find(".upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
            $(this).parent(".picBox").remove();
            $("input[name='photoone']").each(function () {
                $(this).after($(this).clone().val(""));
                $(this).remove();
            })
        })
        // 重置
        $(".closure input[type='reset']").click(function(){
            $(".addPic .picShow").html("");
            $("input[name='photoone']").each(function () {
                $(this).after($(this).clone().val(""));
                $(this).remove();
            })
            $(this).parent(".closure").siblings(".AddEditBottom").find(".addPic .upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
        })
        // 取消
        $(".Close").click(function(){
            $("input[type=reset]").trigger("click")
            // $(".addPic .picShow").html("");
            // $("input[name='photoone']").each(function () {
            //  $(this).after($(this).clone().val(""));
            //  $(this).remove();
            // })
            // $(this).parent(".closure").siblings(".AddEditBottom").find(".addPic .upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
            $(".AddEdit").css("opacity","0").css("width","600px").css("margin-left","-300px").css("height","400px").css("margin-top","-200px").css('z-index',"-1");;
        })
        // 新增按钮
        $(".add").click(function () {
            $(".AddEdit .boxTitle").text("新增");
            $(".AddEdit .editPic").css("display","none");
            $(".AddEdit .addPic").css("display","block");
            $(".closure input[type='reset']").css("display"," inline-block");
            $(".AddEdit").css("opacity","1").css("width","800px").css("margin-left","-400px").css("height","700px").css("margin-top","-350px").css('z-index',"9999");;
            addoredit = document.getElementById('addoredit');
            addoredit.value = 'add';
            // document.getElementById('qingxuanze').innerText = data['fenzu'];
        })
        // 单个显示
        $(".operationTable .show").click(function () {
            id = $(this).attr("data-shu");
            var msg = "您真的确定要显示吗？"; 
            if (confirm(msg)==true){ 
                $(this).parents("tr").find("td .show").css("display","none");
                $(this).parents("tr").find("td .lower").css("display","inline-block");
                $(this).parents("tr").find("td").attr("data-state","true").find(".two").text("正常");
                // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'ajaxedituser';
                $.post(url,data,function(data){
                    // console.log(data);
                });
            }else{ 
                return false; 
            }
        })
        // 单个下架
        $(".operationTable .lower").click(function () {
            id = $(this).attr("data-shu");
            var msg = "您真的确定要下架吗？"; 
            if (confirm(msg)==true){ 
                $(this).parents("tr").find("td .lower").css("display","none");
                $(this).parents("tr").find("td .show").css("display","inline-block");
                $(this).parents("tr").find("td").attr("data-state","false").find(".two").text("下架");
                // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'ajaxedituser';
                $.post(url,data,function(data){
                    // console.log(data);
                });
            }else{ 
                return false; 
            }
        })
        // 单个删除
        $(".operationTable .delete").click(function () {
            id = $(this).attr('data-shu');
            var msg = "您真的确定要删除吗？"; 
            if (confirm(msg)==true){ 
                $(this).parents("tr").remove();
                // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'ajaxdeluser';
                $.post(url,data,function(data){
                    // console.log(data);
                });
            }else{ 
                return false; 
            }
        })
        // 单个编辑
        $(".operationTable .edit").click(function () {
            id = $(this).attr('data-shu');
            if (id == 1) {alert('超级管理员不可以操作');return false;}
            document.getElementById('yaoyongdeid').value = id;
            $(".AddEdit .boxTitle").text($(this).parents("tr").find(".titleItem").text());
            $(".AddEdit .addPic").css("display","none");
            $(".AddEdit .editPic").css("display","block");
            $(".closure input[type='reset']").css("display","none");
            $(".AddEdit").css("opacity","1").css("width","800px").css("margin-left","-400px").css("height","700px").css("margin-top","-350px").css('z-index',"9999");;
            addoredit = document.getElementById('addoredit');
            addoredit.value = 'edit';
            xuyaodeid = document.getElementById('xuyaodeid');
            xuyaodeid.value = id;
            // console.log(id);
            // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'userbianji';
                $.post(url,data,function(data){
                    // console.log(data);
                    if (data['status'] == 'false') {
                        $('#true').removeProp('checked');
                        $('#false').prop('checked', 'checked');
                    }
                    document.getElementById('yonghuming').value = data['username'];
                    document.getElementById('qingxuanze').innerText = data['fenzu'];
                });
        })
        // 单选
        $(".operationTable input[name='btSelectItem']").click(function () {
            var size = $("body input[name='btSelectItem']").size()
            var i = 0
            $("body input[name='btSelectItem']").each(function(){
                if( $(this).prop("checked") == true){
                    i ++;
                }
            })
            if( i >= 1){
                $(".operationButtom span").css("opacity","1");
            }else{
                $(".operationButtom span").css("opacity","0.6");
                $(".operationButtom .add").css("opacity","1");
            }
            if( i == size){
                $("input[name='btSelectAll']").prop("checked",true);
            }else{
                $("input[name='btSelectAll']").prop("checked",false);
            }
        })
    </script>
    <script>
        function kankan(){
            mima = document.getElementById('mima').value;
            if (mima) {
                return true;
            }else{
                alert('请填写密码');
                return false;
            }
        }
    </script>
@endsection


