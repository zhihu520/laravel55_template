@extends('layouts.ad')
@section('content')
		<div class="fr conRight">
			<p class="breadcrumb"><span>主页</span>/<span>新闻列表</span>/<span>新增</span></p>
			<!-- 操作区域 -->
			<div class="editBox">
				<ul class="editTab clearfix">
					<li class="fl lihover"><span>内容</span></li>
					<li class="fl"><span>图片管理</span></li>
				</ul>
				<form action="{{$_SESSION['web']['houtai']}}donews_list_add" method="post" enctype="multipart/form-data" class="addBox" onsubmit="return zuzhitijiao()">
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
					<ul class="editCon">
						<li>
							<!-- type=text的名为textgroup -->
							<table class="textgroup">
								<tr>
									<td class="title required">标题<span>*</span>：</td>
									<td class="text"><input type="text" value="" name="title" id="biaoti"></td>
									<td class="prompt">标题为必填</td>
								</tr>
								<tr>
									<td class="title">描述：</td>
									<td class="text"><textarea name="miaoshu"></textarea></td>
								</tr>
							</table>
							
			                <div class="radiogroup clearfix">
								<p class="title fl">所属分类：</p>
								<div class="radioBox">
				                    <select name="news_group" id="">
				                    	@foreach($info as $v)
											<option value="{{$v->id}}">{{$v->title}}</option>
										@endforeach
				                    </select>
								</div>
							</div>
							<!-- type=radio的名为radiogroup -->
							<div class="radiogroup clearfix">
								<p class="title fl">状态：</p>
								<div class="radioBox">
									<p>
										<input type="radio" name="status" id="true" checked="" value="true" />
										<label for="true">正常</label>
									</p>
									<p>
										<input type="radio" name="status" id="false" value="false" />
										<label for="false">下架</label>
									</p>
								</div>
							</div>
						</li>
						<li>
							<!-- 新增的图片 -->
							<div class="addPic">
								<!-- 单张图片上传 -->
								<div class="picUploading">
									<div class="inputPic clearfix">
										<p class="title fl">单张图片：</p>
										<p class="upload fl">
											<span><i class="iconfont icon-shangchuan"></i>上传图片</span>
											<input type="file" date-shu="one" name="icon" onchange="preview(this)" />
										</p>
										<p class="prompt fl">建议大小:390 x 300 px</p>
									</div>
									<div class="picShow">
										<!-- <p class="picBox">
											<img src="img/icon1.png" alt="">
											<i class="iconfont icon-shanchu"></i>
										</p> -->
									</div>
								</div>
							</div>
						</li>
					</ul>
					<!-- 提交 -->
					<div class="closure">
						<input type="submit" class="closurestyle" value="确定" />
						<a href="javascript:history.go(-1);" class="closurestyle Close">取消</a>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
 