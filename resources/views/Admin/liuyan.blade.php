@extends('layouts.ad')
@section('content')
	<div class="fr conRight">
			<p class="breadcrumb"><span>赛普主页</span>/<span>留言列表</span></p>
			<div class="operationBox">
				<p class="operationButtom">
                    <?php if (in_array("leaveshow",$_COOKIE['admin']['menu_auth'])): ?>
                        <span class="show"><i class="iconfont icon-xianshi"></i>显示</span>
                    <?php endif ?>
                    <?php if (in_array("leavelow",$_COOKIE['admin']['menu_auth'])): ?>
                        <span class="lower"><i class="iconfont icon-xiajia"></i>下架</span>
                    <?php endif ?>
                    <?php if (in_array("leavedelete",$_COOKIE['admin']['menu_auth'])): ?>
                        <span class="delete"><i class="iconfont icon-shanchu"></i>删除</span>
                    <?php endif ?>
				</p>
				<div class="operationTable">
					<table>
						<tr>
							<th width="5%"><input name="btSelectAll" type="checkbox" checked=""></th>
							<th width="5%">ID</th>
							<th width="18%">姓名</th>
							<th width="18%">电话</th>
							<th width="15%">添加时间</th>
							<th width="10%">状态</th>
							<th width="16%">操作</th>
						</tr>
						@foreach($info as $v)
							<tr>
								<td><input name="btSelectItem" type="checkbox"></td>
								<td id="{{$v->id}}">{{$v->id}}</td>
								<td class="titleItem">{{$v->name}}</td>
								<td>{{$v->phone}}</td>
								<td>{{date('Y-m-d',$v->time)}}</td>
								<td class="state" data-state="{{$v->status}}"><span class="one"></span><span class="two"></span></td>
								<td>
                                    <?php if (in_array("leaveedit",$_COOKIE['admin']['menu_auth'])): ?>
                                        <i class="iconfont icon-liuyan edit" data-shu="{{$v->id}}"></i>
                                    <?php endif ?>
                                    <?php if (in_array("leavedelete",$_COOKIE['admin']['menu_auth'])): ?>
                                        <i class="iconfont icon-shanchu delete" data-shu="{{$v->id}}"></i>
                                    <?php endif ?>
                                    <?php if (in_array("leavelow",$_COOKIE['admin']['menu_auth'])): ?>
                                        <i class="iconfont icon-xiajia lower" data-shu="{{$v->id}}"></i>
                                    <?php endif ?>
                                    <?php if (in_array("leaveshow",$_COOKIE['admin']['menu_auth'])): ?>
                                        <i class="iconfont icon-xianshi show" data-shu="{{$v->id}}"></i>
                                    <?php endif ?>
								</td>
								<!-- <a href="javascript:delNews({{$v->id}})"><input type="button" class="btn btn-danger" value="删除"></a> -->
							</tr>
						@endforeach
					</table>
					{{$info->links()}}
				</div>
			</div>
			<!-- 新增与修改的弹出框 -->
			<div class="AddEdit">
				<div class="AddEditTop clearfix">
					<p class="boxTitle fl ovhid">查看详情</p>
					<p class="boxClose Close fr"><i class="iconfont icon-guanbi-"></i></p>
				</div>
				<!-- 新增与修改的提交 -->
				<form action="{{url('addimg')}}" method="post" enctype="multipart/form-data">
				<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
					<!-- 新增与修改的提交内容 -->
					<div class="AddEditBottom">
						<!-- 字段 -->
						<div class="field">
							<!-- type=text的名为textgroup -->
							<div class="textgroup clearfix">
								<p class="title fl">姓名：</p>
								<input type="text" name="title" value="null" id="name">
								<p class="prompt fr">分类名称为必填</p>
							</div>
							<div class="textgroup clearfix">
								<p class="title fl">邮箱：</p>
								<input type="text" name="title" id="email">
								<p class="prompt fr">分类名称为必填</p>
							</div>
							<div class="textgroup clearfix">
								<p class="title fl">手机号：</p>
								<input type="text" name="title" id="phone">
								<p class="prompt fr">分类名称为必填</p>
							</div>
							<div class="textgroup clearfix">
								<p class="title fl">留言信息：</p>
								<!-- <input type="text" name="title" id="main"> -->
                                <textarea name="title" id="main"></textarea>
								<p class="prompt fr">分类名称为必填</p>
							</div>
						</div>
						<!-- 修改的图片 -->
						<div class="editPic">
							
						</div>
					</div>
					
				</form>
			</div>
	</div>
	<script>
        // 删除按钮
        $(".operationButtom .delete").click(function () {
            var deletesize = new Array(); //想要删除的ID
            var i = 0
            $("body input[name='btSelectItem']").each(function(){
                if( $(this).prop("checked") == true){
                    deletesize[i]  = $(this).parent().siblings("td").attr("id");
                    i ++;
                }
            })
            if ( i > 0){
                var msg = "您真的确定要删除吗？"; 
                if (confirm(msg)==true){ 
                    for(var a = 0 ; a <= i ; a++){
                        var htmls = document.getElementById(deletesize[a]);
                        var $htmls=$(htmls);
                        $htmls.parents("tr").remove()
                    }
                    ids = deletesize;
                    // 定义参数
	                var data = {id:ids};
	                //开启token
	                data['_token'] = $('#token').val();
	                var url = 'alldeleteleave';
	                $.post(url,data,function(data){
	                	// console.log(data);
	                });
                }else{ 
                    return false; 
                }
            }else{
                alert("请选择第ID")
            }
        })
        // 下架按钮
        $(".operationButtom .lower").click(function () {
            var lowersize = new Array(); //想要下架的ID
            var i = 0
            $("body input[name='btSelectItem']").each(function(){
                if( $(this).prop("checked") == true){
                    lowersize[i]  = $(this).parent().siblings("td").attr("id");
                    i ++;
                }
            })
            if ( i > 0){
                var msg = "您真的确定要下架吗？"; 
                if (confirm(msg)==true){ 
                    for(var a = 0 ; a <= i ; a++){
                        var htmls = document.getElementById(lowersize[a]);
                        var $htmls=$(htmls);
                        $htmls.parents("tr").find("td").attr("data-state","false").find(".two").text("下架");
                        $htmls.parents("tr").find("td .lower").css("display","none");
                        $htmls.parents("tr").find("td .show").css("display","inline-block");
                        $("body input[type='checkbox']").each(function(){
                            $(this).prop("checked",false);
                        })
                    }
                    ids = lowersize;
                    // 定义参数
	                var data = {id:ids};
	                //开启token
	                data['_token'] = $('#token').val();
	                var url = 'alllowerleave';
	                $.post(url,data,function(data){
	                	// console.log(data);
	                });
                }else{ 
                    return false; 
                }
            }else{
                alert("请选择第ID")
            }
        })
        // 显示按钮
        $(".operationButtom .show").click(function () {
            var showsize = new Array(); //想要显示的ID
            var i = 0
            $("body input[name='btSelectItem']").each(function(){
                if( $(this).prop("checked") == true){
                    showsize[i]  = $(this).parent().siblings("td").attr("id");
                    i ++;
                }
            })
            if ( i > 0){
                var msg = "您真的确定要显示吗？"; 
                if (confirm(msg)==true){ 
                    for(var a = 0 ; a <= i ; a++){
                        var htmls = document.getElementById(showsize[a]);
                        var $htmls=$(htmls);
                        $htmls.parents("tr").find("td").attr("data-state","true").find(".two").text("正常");
                        $htmls.parents("tr").find("td .show").css("display","none");
                        $htmls.parents("tr").find("td .lower").css("display","inline-block");
                        $("body input[type='checkbox']").each(function(){
                            $(this).prop("checked",false);
                        })
                    }
                    ids = showsize;
                    // 定义参数
	                var data = {id:ids};
	                //开启token
	                data['_token'] = $('#token').val();
	                var url = 'allshowsizeleave';
	                $.post(url,data,function(data){
	                	// console.log(data);
	                });
                }else{ 
                    return false; 
                }
            }else{
                alert("请选择第ID")
            }
        })
        // 单个显示
        $(".operationTable .show").click(function () {
        	id = $(this).attr("data-shu");
            var msg = "您真的确定要显示吗？"; 
            if (confirm(msg)==true){ 
                $(this).parents("tr").find("td .show").css("display","none");
                $(this).parents("tr").find("td .lower").css("display","inline-block");
                $(this).parents("tr").find("td").attr("data-state","true").find(".two").text("正常");
                // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'ajaxeditleave';
                $.post(url,data,function(data){
                	// console.log(data);
                });
            }else{ 
                return false; 
            }
        })
        // 单个下架
        $(".operationTable .lower").click(function () {
        	id = $(this).attr("data-shu");
            var msg = "您真的确定要下架吗？"; 
            if (confirm(msg)==true){ 
                $(this).parents("tr").find("td .lower").css("display","none");
                $(this).parents("tr").find("td .show").css("display","inline-block");
                $(this).parents("tr").find("td").attr("data-state","false").find(".two").text("下架");
                // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'ajaxeditleave';
                $.post(url,data,function(data){
                	// console.log(data);
                });
            }else{ 
                return false; 
            }
        })
        // 单个删除
        $(".operationTable .delete").click(function () {
        	id = $(this).attr('data-shu');
            var msg = "您真的确定要删除吗？"; 
            if (confirm(msg)==true){ 
                $(this).parents("tr").remove();
                // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'ajaxdelleave';
                $.post(url,data,function(data){
                	console.log(data);
                });
            }else{ 
                return false; 
            }
        })
        // 单个编辑
        $(".operationTable .edit").click(function () {
            $(".AddEdit .boxTitle").text($(this).parents("tr").find(".titleItem").text());
            $(".AddEdit .addPic").css("display","none");
            $(".AddEdit .editPic").css("display","block");
            $(".closure input[type='reset']").css("display","none");
            $(".AddEdit").css("opacity","1").css("width","800px").css("margin-left","-400px").css("height","700px").css("margin-top","-350px").css('z-index',"9999");
            id = $(this).attr('data-shu');
            // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'ajaxshowleave';
                $.post(url,data,function(data){
                	document.getElementById('name').value = data['name'];
                	document.getElementById('email').value = data['email'];
                	document.getElementById('phone').value = data['phone'];
                	document.getElementById('main').value = data['main'];
                	document.getElementById('name').value = data['name'];
                });
        })
        // 取消
        $(".Close").click(function(){
            $("input[type=reset]").trigger("click")
            // $(".addPic .picShow").html("");
            // $("input[name='photoone']").each(function () {
            //  $(this).after($(this).clone().val(""));
            //  $(this).remove();
            // })
            // $(this).parent(".closure").siblings(".AddEditBottom").find(".addPic .upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
            $(".AddEdit").css("opacity","0").css("width","600px").css("margin-left","-300px").css("height","400px").css("margin-top","-200px").css('z-index',"-1");
        })
        
    </script>
@endsection


