@extends('layouts.ad')
@section('content')
		<div class="fr conRight">
			<p class="breadcrumb"><span>主页</span>/<span>系统设置</span>/<span>公司简介</span></p> 
			<!-- 操作区域 -->
			<div class="editBox">
				<ul class="editTab clearfix">
					<li class="fl"><span>公司简介</span></li>
				</ul>
				<form action="{{$_SESSION['web']['houtai']}}doabout" method="post" enctype="multipart/form-data" class="addBox">
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
					<ul class="editCon">
						<li>
							<div class="tab-pane">
								<!-- <h1>百度编辑器</h1> -->
								<textarea name="about" id="intro_detail" cols="30" rows="10">{{$_SESSION['info']->about}}</textarea>
							</div>
						</li>
					</ul>
					<!-- 提交 -->
					<div class="closure">
						<input type="submit" class="closurestyle" value="确定" />
						<a href="javascript:history.go(-1);" class="closurestyle Close">取消</a>
						<input type="reset" class="closurestyle" value="重置" onmousedown="return $EDITORUI[&quot;edui51&quot;]._onMouseDown(event, this);" onclick="return $EDITORUI[&quot;edui51&quot;]._onClick(event, this);"/>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
 