@extends('layouts.ad')
@section('content')
		<div class="fr conRight">
			<p class="breadcrumb"><span>主页</span>/<span>系统设置</span>/<span>公司信息</span></p>
			<!-- 操作区域 -->
			<div class="editBox">
				<ul class="editTab clearfix">
					<li class="fl lihover"><span>内容</span></li>
				</ul>
				<form action="{{$_SESSION['web']['houtai']}}doinfo" method="post" enctype="multipart/form-data" class="addBox" onsubmit="return zuzhitijiao()">
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
					<ul class="editCon">
						<li>
							<!-- type=text的名为textgroup -->
							<table class="textgroup">
								<tr>
									<td class="title required">公司名称<span>*</span>：</td>
									<td class="text"><input type="text" value="{{$_SESSION['info']->title}}" name="title" id="biaoti" placeholder="请正确输入公司名称"></td>
									<td class="prompt">公司名称为必填</td>
								</tr>
								<tr>
									<td class="title required">业务咨询QQ<span></span>：</td>
									<td class="text"><input type="text" value="{{$_SESSION['info']->ywzx}}" name="ywzx" placeholder="请正确输入qq号"></td>
								</tr>
								<tr>
									<td class="title required">技术服务QQ<span></span>：</td>
									<td class="text"><input type="text" value="{{$_SESSION['info']->jsfw}}" name="jsfw" placeholder="请正确输入qq号"></td>
								</tr>
								<tr>
									<td class="title required">售前咨询电话<span></span>：</td>
									<td class="text"><input type="text" value="{{$_SESSION['info']->sqzx}}" name="sqzx" placeholder="请正确输入电话"></td>
								</tr>
								<tr>
									<td class="title required">售后技术电话<span></span>：</td>
									<td class="text"><input type="text" value="{{$_SESSION['info']->shjs}}" name="shjs" placeholder="请正确输入电话"></td>
								</tr>
								<tr>
									<td class="title required">邮箱<span></span>：</td>
									<td class="text"><input type="text" value="{{$_SESSION['info']->email}}" name="email" placeholder="请正确输入邮箱"></td>
								</tr>
								<tr>
									<td class="title required">公司地址<span></span>：</td>
									<td class="text"><input type="text" value="{{$_SESSION['info']->address}}" name="address" placeholder="请正确输入公司地址"></td>
								</tr>
							</table>
						</li>
					</ul>
					<!-- 提交 -->
					<div class="closure">
						<input type="submit" class="closurestyle" value="确定"/>
						<a href="javascript:history.go(-1);" class="closurestyle Close">取消</a>
					</div>
				</form>
			</div>
		</div>
	</div>
@endsection
 