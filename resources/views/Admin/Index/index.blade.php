@extends('layouts.ad')
@section('content')
	<div class="fr conRight">
			<p class="breadcrumb"><span>主页</span>/<span>后台主页</span></p>
			<div class="ContentBox">
				<div class="ContentBoxTop">
					<p>欢迎登陆</p>
					<table>
						<tr>
							<td>
								<img src="img/icon1.png" alt="">
								<h1>高端网站建设服务</h1>
								<p>品牌网站创意设计</p>
								<p>电商平台建设</p>
								<p>门户/社区网站建设</p>
								<p>营销站点设计</p>
								<p>活动专题设计</p>
							</td>
							<td>
								<img src="img/icon2.png" alt="">
								<h1>高端网站建设服务</h1>
								<p>iOS/Android APP应用开发</p>
								<p>手机网站建设 </p>
								<p>HTML5设计开发</p>
								<p>微信公众号搭建及相关功能开发</p>
								<p>微信小程序开发 </p>
							</td>
							<td>
								<img src="img/icon3.png" alt="">
								<h1>品牌及平面设计服务</h1>
								<p>企业标识设计</p>
								<p>名片设计</p>
								<p>海报/宣传册/包装设计</p>
								<p>广告设计</p>
								<p>淘宝/天猫店铺设计</p>
							</td>
							<td>
								<img src="img/icon4.png" alt="">
								<h1>办公应用系统开发</h1>
								<p>协同办公系统</p>
								<p>客户关系管理系统</p>
								<p>企业信息化系统 </p>
							</td>
						</tr>
					</table>
				</div>
				<div class="ContentBoxBottom clearfix">
					<div class="ContentBoxBottomLeft fl">
						<table>
							<tr>
								<td><img src="img/icon5.png" alt=""></td>
								<td>
									<p class="one">上海</p>
									<p class="oness">shanghai</p>
								</td>
								<td>
									<table>
										<tr>
											<td class="bt">地址：</td>
											<td>上海市浦东新区1369号15A</td>
										</tr>
										<tr>
											<td class="bt">电话：</td>
											<td>021-68390098</td>
										</tr>
										<tr>
											<td class="bt">手机：</td>
											<td>136-0179-1589</td>
										</tr>
										<tr>
											<td class="bt">邮箱：</td>
											<td>ben-zhao@xing-t.com</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr><td><div></div></td></tr>
							<tr>
								<td><img src="img/icon6.png" alt=""></td>
								<td>
									<p class="one">合肥</p>
									<p class="oness">hefei</p>
								</td>
								<td>
									<table>
										<tr>
											<td class="bt">地址：</td>
											<td>安徽省合肥市高新区望江西路539号鲲鹏国际广场6#208</td>
										</tr>
										<tr>
											<td class="bt">电话：</td>
											<td>0551-65305883</td>
										</tr>
										<tr>
											<td class="bt">手机：</td>
											<td>139-6511-9814</td>
										</tr>
									</table>
								</td>
							</tr>
						</table>
					</div>
					<div class="ContentBoxBottomRight fr">
						<div class="up"><img src="img/icon7.png" alt=""></div>
						<div class="down clearfix">
							<p class="rightTexts">核心团队均来自于互联网行业多年经验，服务过的客户有近百余个，涵盖的客户类型包括：金融、IT、旅游、美食、工业制作、酒店、互联网、科技等众多领域，积累了大量丰富的经验，其中不乏500强企业及知名品牌，同时也获得了客户的一致好评！</p>
							<a href="http://www.xing-t.com" class="rightMore" target="_blank">了解更多</a>
						</div>
					</div>
				</div>
			</div>
	</div>
@endsection
