@extends('layouts.ad')
@section('content')
		<div class="fr conRight">
			<p class="breadcrumb"><span>主页</span>/<span>新闻列表</span>/<span>新增</span></p>
			<!-- 操作区域 -->
			<div class="editBox">
				<ul class="editTab clearfix">
					<li class="fl lihover"><span>内容</span></li>
					<li class="fl"><span>图片管理</span></li>
					<li class="fl"><span>简介</span></li>
					<li class="fl"><span>列表简介</span></li>
					<li class="fl"><span>详细内容</span></li>
				</ul>
				<form action="{{$_SESSION['web']['houtai']}}doshop_edit" method="post" enctype="multipart/form-data" class="addBox" onsubmit="return zuzhitijiao()">
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
					<input type="hidden" name="id" value="{{$info->id}}">
					<input type="hidden" name="shanchu" value="" id="shanchu">
					<input type="hidden" name="logo" value="" id="logo">
					<input type="hidden" name="list_icon" value="" id="list_icon">
					<ul class="editCon">
						<li>
							<!-- type=text的名为textgroup -->
							<table class="textgroup">
								<tr>
									<td class="title required">标题<span>*</span>：</td>
									<td class="text"><input type="text" value="{{$info->title_z}}" name="title_z" id="biaoti"></td>
									<td class="prompt">标题为必填</td>
								</tr>
								<tr>
									<td class="title required">English<span>*</span>：</td>
									<td class="text"><input type="text" value="{{$info->title_e}}" name="title_e" id="biaoti"></td>
									<td class="prompt">English为必填</td>
								</tr>
							</table>
							<!-- type=radio的名为radiogroup -->
							<?php if ($info->status == 'true'): ?>
								<div class="radiogroup clearfix">
									<p class="title fl">状态：</p>
									<div class="radioBox">
										<p>
											<input type="radio" name="status" id="true" checked="" value="true" />
											<label for="true">正常</label>
										</p>
										<p>
											<input type="radio" name="status" id="false" value="false" />
											<label for="false">下架</label>
										</p>
									</div>
								</div>
							<?php else: ?>
								<div class="radiogroup clearfix">
									<p class="title fl">状态：</p>
									<div class="radioBox">
										<p>
											<input type="radio" name="status" id="true" value="true" />
											<label for="true">正常</label>
										</p>
										<p>
											<input type="radio" name="status" id="false" value="false"  checked=""  />
											<label for="false">下架</label>
										</p>
									</div>
								</div>
							<?php endif ?>
						</li>
						<li>
							<!-- 新增的图片 -->
							<div class="addPic">
								<!-- 单张图片上传 -->
								<div class="picUploading">
									<div class="inputPic clearfix">
										<p class="title fl">图片：</p>
										<p class="upload fl">
											<span class="1"><i class="iconfont icon-shangchuan" value="2"></i>上传图片</span>
											<input type="file" date-shu="one" name="icon" onchange="preview(this)" />
										</p>
										<p class="prompt fl">建议大小:240 x 240 px</p>
									</div>
									<div class="picShow">
										<p class="picBox 1">
											<img src="{{$info->icon}}" alt="">
											<i class="iconfont icon-shanchu"></i>
										</p>
									</div>
								</div>
							</div>
							<div class="addPic">
								<!-- 单张图片上传 -->
								<div class="picUploading">
									<div class="inputPic clearfix">
										<p class="title fl">LOGO：</p>
										<p class="upload fl">
											<span class="2"><i class="iconfont icon-shangchuan" value="2"></i>上传图片</span>
											<input type="file" date-shu="one" name="logo" onchange="preview(this)" />
										</p>
										<p class="prompt fl">建议大小:240 x 240 px</p>
									</div>
									<div class="picShow">
										<p class="picBox 2">
											<img src="{{$info->logo}}" alt="">
											<i class="iconfont icon-shanchu"></i>
										</p>
									</div>
								</div>
								<!-- 单张图片上传 -->
								<div class="picUploading">
									<div class="inputPic clearfix">
										<p class="title fl">列表图片：</p>
										<p class="upload fl">
											<span class="3"><i class="iconfont icon-shangchuan" value="3"></i>上传图片</span>
											<input type="file" date-shu="one" name="list_icon" onchange="preview(this)" />
										</p>
										<p class="prompt fl">建议大小:300 x 240 px</p>
									</div>
									<div class="picShow">
										<p class="picBox 3">
											<img src="{{$info->list_icon}}" alt="">
											<i class="iconfont icon-shanchu"></i>
										</p>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="tab-pane">
								<!-- <h1>百度编辑器</h1> -->
								<textarea name="main" id="intro_detail">{{$info->main}}</textarea>
							</div>
						</li>
						<li>
							<div class="tab-pane">
								<!-- <h1>百度编辑器</h1> -->
								<textarea name="liebiao_jianjie" id="intro_detail2">{{$info->liebiao_jianjie}}</textarea>
							</div>
						</li>
						<li>
							<div class="tab-pane">
								<!-- <h1>百度编辑器</h1> -->
								<textarea name="xiangxi" id="intro_detail1">{{$info->xiangxi}}</textarea>
							</div>
						</li>
					</ul>
					<!-- 提交 -->
					<div class="closure">
						<input type="submit" class="closurestyle" value="确定" />
						<a href="javascript:history.go(-1);" class="closurestyle Close">取消</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script>
	// 操作区域的最大高度
	var boxheightqq = $(".editBox").outerHeight(true);
	var edittabheightqq = $(".editTab").outerHeight(true);
	var closureheightqq = $(".editBox .closure").outerHeight(true);
		// alert(boxheightqq-edittabheightqq-closureheightqq-55)
	$(".editBox .editCon").css("max-height",boxheightqq-edittabheightqq-closureheightqq-55);
		// 单张图片上传
		function preview(obj) {
			var $obj=$(obj);				//js转jQuery
			var $dom = $obj.parents(".inputPic").siblings(".picShow");
			var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
			if (obj.files && obj.files[0]) {
				var reader = new FileReader();
				reader.onload = function(evt) {
					$dom.html("<p class='picBox'><img src='" + evt.target.result + "'><i class='iconfont icon-shanchu'></i></p>");
				}
				reader.readAsDataURL(obj.files[0]);
				$obj.siblings("span").html("<i class='iconfont icon-shangchuan'></i>修改图片")
				if ($obj.siblings("span")[0]['className'] == 1) {
					shanbushan = document.getElementById('shanchu');
					shanbushan.value = 'bushan';
				}
				if ($obj.siblings("span")[0]['className'] == 2) {
					shanbushan = document.getElementById('logo');
					shanbushan.value = 'bushan';
				}
				if ($obj.siblings("span")[0]['className'] == 3) {
					shanbushan = document.getElementById('list_icon');
					shanbushan.value = 'bushan';
				}
			} else {
				$dom.find("span").remove();
			}
		}
		//多张图片上传
			// 一个多图一份这个，picssrc进行改变；并且把函数放到点击事件里
			var picssrc = new Array();
			function picssrcfun(){
				var size =$(".picsUploading .picShow[name='picssrc'] .picBox").size();
				for (var a = 0; a < size; a++) {
					picssrc[a] = $(".picsUploading .picShow[name='picssrc'] .picBox").eq(a).find("img").attr("src");
				}
			}
			$(".editBox .closure .input[type='submit'] ").click(function(){
				picssrcfun()
			})
		function previews(obj){
			var $obj=$(obj);				//js转jQuery
			var $dom = $obj.parents(".inputPic").siblings(".picShow");
			var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
			if (obj.files) {
				for(i = 0; i < obj.files.length ; i ++) {
					var reader = new FileReader();    
					reader.readAsDataURL(obj.files[i]);  
					reader.onload=function(e){  
						//多图预览
						$dom.append("<p class='picBox'><img src='" + this.result + "'><i class='iconfont icon-shanchu'></i></p>");
					}
				}
			} else {
				$dom.html(domhtml);
			}
		}
		// 图片删除
		$(".picShow").on("click",".picBox i",function(){
			$(this).parents(".picShow").siblings(".inputPic").find(".upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
			$(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").after($(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").clone().val(""));
			var shui = $(this).parent(".picBox")[0]['className'];
			if (shui == 'picBox 1') {
				shanbushan = document.getElementById('shanchu');
				shanbushan.value = 'shan';
			}
			if (shui == 'picBox 2') {
				shanlogo = document.getElementById('logo');
				shanlogo.value = 'shan';
			}
			if (shui == 'picBox 3') {
				shanlogo = document.getElementById('list_icon');
				shanlogo.value = 'shan';
			}
			$(this).parent(".picBox").remove();
		})
		// 重置
		$(".closure input[type='reset']").click(function(){
			$(".addPic .picShow").html("");
			$("input[type='file']").each(function () {
				$(this).after($(this).clone().val(""));
				// $(this).remove();
			})
			$(this).parent(".closure").siblings(".AddEditBottom").find(".addPic .upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
		})
		//提示的显示
		$("input").focusout(function(){
			if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
				$(this).parent("td").siblings(".prompt").css("opacity","1");
			}else{
				$(this).parent("td").siblings(".prompt").css("opacity","0");
			}
		})
		$("textarea").focusout(function(){
			if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
				$(this).parent("td").siblings(".prompt").css("opacity","1");
			}else{
				$(this).parent("td").siblings(".prompt").css("opacity","0");
			}
		})
		// 操作区域界面
		$(".editTab li").click(function(){
			$(this).addClass("lihover").siblings().removeClass("lihover");
			$(".editBox .editCon li").eq($(this).index()).css("display","block").siblings().css("display","none");
		})
		//表单提交
		function zuzhitijiao(){
			biaoti = document.getElementById('biaoti').value;
			if (biaoti) {
				return true;
			}else{
				alert('标题必填');
				return false;
			}
		}
	</script>
@endsection
 