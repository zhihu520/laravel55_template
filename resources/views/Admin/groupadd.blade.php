@extends('layouts.ad')
@section('content')
		<div class="fr conRight">
			<p class="breadcrumb"><span>赛普主页</span>/<span>角色列表</span>/<span>编辑</span></p>
			<!-- 操作区域 -->
			<div class="editBox">
				<ul class="editTab clearfix">
					<li class="fl lihover"><span>内容</span></li>
					<li class="fl"><span>权限</span></li>
				</ul>
				<form action="{{url('dogroupadd')}}" method="post" enctype="multipart/form-data" class="addBox" onsubmit="return zuzhitijiao()">
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
					<ul class="editCon">
						<li>
							<!-- type=text的名为textgroup -->
							<table class="textgroup">
								<tr>
									<td class="title required">角色名称<span>*</span>：</td>
									<td class="text"><input type="text" value="" name="title" id="biaoti"></td>
									<td class="prompt">标题为必填</td>
								</tr>
								<tr>
									<td class="title">描述：</td>
									<td class="text"><textarea name="main"></textarea></td>
									<td class="prompt">分类名称为必填</td>
								</tr>
							</table>
							<!-- type=radio的名为radiogroup -->
							<div class="radiogroup clearfix">
								<p class="title fl">状态：</p>
								<div class="radioBox">
									<p>
										<input type="radio" name="status" id="true" checked="" value="true" />
										<label for="true">正常</label>
									</p>
									<p>
										<input type="radio" name="status" id="false" value="false" />
										<label for="false">下架</label>
									</p>
								</div>
							</div>
						</li>
						<li>
							<div class="checkboxgroup clearfix power_parent">
								<p class="title fl">权限：</p>
								<div class="checkboxBox">
									<p><input type="checkbox" name="quanxian[]" id="addnew" value="new"/>动态管理</p>
									<p><input type="checkbox" name="quanxian[]" id="addslider" value="slider"/>广告管理</p>
									<p><input type="checkbox" name="quanxian[]" id="addleave" value="leave"/>留言管理</p>
									<p><input type="checkbox" name="quanxian[]" id="adduser" value="user"/>角色管理员管理</p>
									<p><input type="checkbox" name="quanxian[]" id="addabout" value="about"/>荣誉资质</p>
									<p><input type="checkbox" name="quanxian[]" id="addcaselist" value="caselist"/>案例管理</p>
									<p><input type="checkbox" name="quanxian[]" id="addtalent" value="talent"/>人才招聘</p>
								</div>
							</div>
							<div class="checkboxgroup clearfix power_chlid" data-power="new">
								<p class="title fl">动态管理权限：</p>
								<div class="checkboxBox">
									<p><input type="checkbox" name="quanxian[]" id="addnewadd" value="newadd"/>新增</p>
									<p><input type="checkbox" name="quanxian[]" id="addnewdelete" value="newdelete"/>删除</p>
									<p><input type="checkbox" name="quanxian[]" id="addnewedit" value="newedit"/>编辑</p>
									<p><input type="checkbox" name="quanxian[]" id="addnewshow" value="newshow"/>显示</p>
									<p><input type="checkbox" name="quanxian[]" id="addnewlow" value="newlow"/>下架</p>
								</div>
							</div>
							<div class="checkboxgroup clearfix power_chlid" data-power="slider">
								<p class="title fl">广告管理权限：</p>
								<div class="checkboxBox">
									<p><input type="checkbox" name="quanxian[]" id="addslideradd" value="slideradd"/>新增</p>
									<p><input type="checkbox" name="quanxian[]" id="addsliderdelete" value="sliderdelete"/>删除</p>
									<p><input type="checkbox" name="quanxian[]" id="addslideredit" value="slideredit"/>编辑</p>
									<p><input type="checkbox" name="quanxian[]" id="addslidershow" value="slidershow"/>显示</p>
									<p><input type="checkbox" name="quanxian[]" id="addsliderlow" value="sliderlow"/>下架</p>
								</div>
							</div>
							<div class="checkboxgroup clearfix power_chlid" data-power="leave">
								<p class="title fl">留言管理权限：</p>
								<div class="checkboxBox">
									<p><input type="checkbox" name="quanxian[]" id="addleaveadd" value="leaveadd"/>新增</p>
									<p><input type="checkbox" name="quanxian[]" id="addleavedelete" value="leavedelete"/>删除</p>
									<p><input type="checkbox" name="quanxian[]" id="addleaveedit" value="leaveedit"/>编辑</p>
									<p><input type="checkbox" name="quanxian[]" id="addleaveshow" value="leaveshow"/>显示</p>
									<p><input type="checkbox" name="quanxian[]" id="addleavelow" value="leavelow"/>下架</p>
								</div>
							</div>
							<div class="checkboxgroup clearfix power_chlid" data-power="user">
								<p class="title fl">角色管/理员管理权限：</p>
								<div class="checkboxBox">
									<p><input type="checkbox" name="quanxian[]" id="adduseradd" value="useradd"/>新增</p>
									<p><input type="checkbox" name="quanxian[]" id="adduserdelete" value="userdelete"/>删除</p>
									<p><input type="checkbox" name="quanxian[]" id="adduseredit" value="useredit"/>编辑</p>
									<p><input type="checkbox" name="quanxian[]" id="addusershow" value="usershow"/>显示</p>
									<p><input type="checkbox" name="quanxian[]" id="adduserlow" value="userlow"/>下架</p>
								</div>
							</div>
							<div class="checkboxgroup clearfix power_chlid" data-power="about">
								<p class="title fl">荣誉资质管理权限：</p>
								<div class="checkboxBox">
									<p><input type="checkbox" name="quanxian[]" id="addaboutadd" value="aboutadd"/>新增</p>
									<p><input type="checkbox" name="quanxian[]" id="addaboutdelete" value="aboutdelete"/>删除</p>
									<p><input type="checkbox" name="quanxian[]" id="addaboutedit" value="aboutedit"/>编辑</p>
									<p><input type="checkbox" name="quanxian[]" id="addaboutshow" value="aboutshow"/>显示</p>
									<p><input type="checkbox" name="quanxian[]" id="addaboutlow" value="aboutlow"/>下架</p>
								</div>
							</div>
							<div class="checkboxgroup clearfix power_chlid" data-power="caselist">
								<p class="title fl">案例管理权限：</p>
								<div class="checkboxBox">
									<p><input type="checkbox" name="quanxian[]" id="addcaselistadd" value="caselistadd"/>新增</p>
									<p><input type="checkbox" name="quanxian[]" id="addcaselistdelete" value="caselistdelete"/>删除</p>
									<p><input type="checkbox" name="quanxian[]" id="addcaselistedit" value="caselistedit"/>编辑</p>
									<p><input type="checkbox" name="quanxian[]" id="addcaselistshow" value="caselistshow"/>显示</p>
									<p><input type="checkbox" name="quanxian[]" id="addcaselistlow" value="caselistlow"/>下架</p>
								</div>
							</div>
							<div class="checkboxgroup clearfix power_chlid" data-power="talent">
								<p class="title fl">人才招聘管理权限：</p>
								<div class="checkboxBox">
									<p><input type="checkbox" name="quanxian[]" id="addtalentadd" value="talentadd"/>新增</p>
									<p><input type="checkbox" name="quanxian[]" id="addtalentdelete" value="talentdelete"/>删除</p>
									<p><input type="checkbox" name="quanxian[]" id="addtalentedit" value="talentedit"/>编辑</p>
									<p><input type="checkbox" name="quanxian[]" id="addtalentshow" value="talentshow"/>显示</p>
									<p><input type="checkbox" name="quanxian[]" id="addtalentlow" value="talentlow"/>下架</p>
								</div>
							</div>
						</li>
						
					</ul>
					<!-- 提交 -->
					<div class="closure">
						<input type="submit" class="closurestyle" value="确定" />
						<a href="javascript:history.go(-1);" class="closurestyle Close">取消</a>
						<input type="reset" class="closurestyle" value="重置" onmousedown="return $EDITORUI[&quot;edui51&quot;]._onMouseDown(event, this);" onclick="return $EDITORUI[&quot;edui51&quot;]._onClick(event, this);"/>
					</div>
				</form>
			</div>
		</div>
	</div>
	<style>
		.power_chlid{display: none;}
	</style>
	<script>
	// 操作区域的最大高度
	var boxheightqq = $(".editBox").outerHeight(true);
	var edittabheightqq = $(".editTab").outerHeight(true);
	var closureheightqq = $(".editBox .closure").outerHeight(true);
		// alert(boxheightqq-edittabheightqq-closureheightqq-55)
	$(".editBox .editCon").css("max-height",boxheightqq-edittabheightqq-closureheightqq-55);
		// 单张图片上传
		function preview(obj) {
			var $obj=$(obj);				//js转jQuery
			var $dom = $obj.parents(".inputPic").siblings(".picShow");
			var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
			if (obj.files && obj.files[0]) {
				var reader = new FileReader();
				reader.onload = function(evt) {
					$dom.html("<p class='picBox'><img src='" + evt.target.result + "'><i class='iconfont icon-shanchu'></i></p>");
				}
				reader.readAsDataURL(obj.files[0]);
				$obj.siblings("span").html("<i class='iconfont icon-shangchuan'></i>修改图片")
			} else {
				$dom.find("span").remove();
			}
		}
		//多张图片上传
			// 一个多图一份这个，picssrc进行改变；并且把函数放到点击事件里
			var picssrc = new Array();
			function picssrcfun(){
				var size =$(".picsUploading .picShow[name='picssrc'] .picBox").size();
				for (var a = 0; a < size; a++) {
					picssrc[a] = $(".picsUploading .picShow[name='picssrc'] .picBox").eq(a).find("img").attr("src");
				}
			}
			$(".editBox .closure .input[type='submit'] ").click(function(){
				picssrcfun()
			})
		function previews(obj){
			var $obj=$(obj);				//js转jQuery
			var $dom = $obj.parents(".inputPic").siblings(".picShow");
			var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
			if (obj.files) {
				for(i = 0; i < obj.files.length ; i ++) {
					var reader = new FileReader();    
					reader.readAsDataURL(obj.files[i]);  
					reader.onload=function(e){  
						//多图预览
						$dom.append("<p class='picBox'><img src='" + this.result + "'><i class='iconfont icon-shanchu'></i></p>");
					}
				}
			} else {
				$dom.html(domhtml);
			}
		}
		// 图片删除
		$(".picShow").on("click",".picBox i",function(){
			$(this).parents(".picShow").siblings(".inputPic").find(".upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
			$(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").after($(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").clone().val(""));
			$(this).parent(".picBox").remove();
		})
		// 重置
		$(".closure input[type='reset']").click(function(){
			$(".addPic .picShow").html("");
			$("input[type='file']").each(function () {
				$(this).after($(this).clone().val(""));
				// $(this).remove();
			})
			$(this).parent(".closure").siblings(".AddEditBottom").find(".addPic .upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
		})
		//提示的显示
		$("input").focusout(function(){
			if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
				$(this).parent("td").siblings(".prompt").css("opacity","1");
			}else{
				$(this).parent("td").siblings(".prompt").css("opacity","0");
			}
		})
		$("textarea").focusout(function(){
			if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
				$(this).parent("td").siblings(".prompt").css("opacity","1");
			}else{
				$(this).parent("td").siblings(".prompt").css("opacity","0");
			}
		})
		// 操作区域界面
		$(".editTab li").click(function(){
			$(this).addClass("lihover").siblings().removeClass("lihover");
			$(".editBox .editCon li").eq($(this).index()).css("display","block").siblings().css("display","none");
		})
		//表单提交
		function zuzhitijiao(){
			biaoti = document.getElementById('biaoti').value;
			if (biaoti) {
				return true;
			}else{
				alert('标题必填');
				return false;
			}
		}
		// 表单的显示与隐藏
		$(".power_parent .checkboxBox p").click(function(){
			var power = $(this).find("input").val();
			$(".power_chlid[data-power='"+power+"'] p").each(function(){
				$(this).find("input").prop("checked",false);
			})
			if( $(this).find("input").is(':checked')){
				$(".power_chlid[data-power='"+power+"']").css("display","block");
			}else{
				$(".power_chlid[data-power='"+power+"']").css("display","none");
			}
		})
	</script>
@endsection
 