@extends('layouts.ad')
@section('content')
	<div class="fr conRight">
			<p class="breadcrumb"><span>星途主页</span>/<span>星途主页</span></p>
			<div class="operationBox">
				<p class="operationButtom">
                    <?php if (in_array("aboutadd",$_COOKIE['admin']['menu_auth'])): ?>
                        <a href="{{url('teamadd')}}" class="add"><i class="iconfont icon-plus"></i>新增</a>
                    <?php endif ?>
                    <?php if (in_array("aboutshow",$_COOKIE['admin']['menu_auth'])): ?>
                        <span class="show"><i class="iconfont icon-xianshi"></i>显示</span>
                    <?php endif ?>
                    <?php if (in_array("aboutlow",$_COOKIE['admin']['menu_auth'])): ?>
                        <span class="lower"><i class="iconfont icon-xiajia"></i>下架</span>
                    <?php endif ?>
                    <?php if (in_array("aboutdelete",$_COOKIE['admin']['menu_auth'])): ?>
                        <span class="delete"><i class="iconfont icon-shanchu"></i>删除</span>
                    <?php endif ?>
				</p>
				<div class="operationTable">
					<table>
						<tr>
							<th width="5%"><input name="btSelectAll" type="checkbox" checked=""></th>
							<th width="5%">ID</th>
							<th width="18%">标题</th>
							<th width="15%">创建时间</th>
							<th width="15%">修改时间</th>
							<th width="10%">状态</th>
							<th width="16%">操作</th>
						</tr>
						@foreach($info as $v)
							<tr>
								<td><input name="btSelectItem" type="checkbox"></td>
								<td id="{{$v->id}}">{{$v->id}}</td>
								<td class="titleItem">{{$v->title}}</td>
								<td>{{date('Y-m-d',$v->create_time)}}</td>
								<td>{{date('Y-m-d',$v->update_time)}}</td>
								<td class="state" data-state="{{$v->status}}"><span class="one"></span><span class="two"></span></td>
								<td>
                                    <?php if (in_array("aboutedit",$_COOKIE['admin']['menu_auth'])): ?>
                                        <a href="teamedit?id={{$v->id}}"><i class="iconfont icon-liuyan edit" data-shu="{{$v->id}}"></i></a>
                                    <?php endif ?>
                                    <?php if (in_array("aboutdelete",$_COOKIE['admin']['menu_auth'])): ?>
                                        <i class="iconfont icon-shanchu delete" data-shu="{{$v->id}}"></i>
                                    <?php endif ?>
                                    <?php if (in_array("aboutlow",$_COOKIE['admin']['menu_auth'])): ?>
                                        <i class="iconfont icon-xiajia lower" data-shu="{{$v->id}}"></i>
                                    <?php endif ?>
                                    <?php if (in_array("aboutshow",$_COOKIE['admin']['menu_auth'])): ?>
                                        <i class="iconfont icon-xianshi show" data-shu="{{$v->id}}"></i>
                                    <?php endif ?>
								</td>
								<!-- <a href="javascript:delNews({{$v->id}})"><input type="button" class="btn btn-danger" value="删除"></a> -->
							</tr>
						@endforeach
					</table>
					{{$info->links()}}
				</div>
			</div>
				<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
			</div>
	</div>
	<script>



        // 删除按钮
        $(".operationButtom .delete").click(function () {
            var deletesize = new Array(); //想要删除的ID
            var i = 0
            $("body input[name='btSelectItem']").each(function(){
                if( $(this).prop("checked") == true){
                    deletesize[i]  = $(this).parent().siblings("td").attr("id");
                    i ++;
                }
            })
            if ( i > 0){
                var msg = "您真的确定要删除吗？"; 
                if (confirm(msg)==true){ 
                    for(var a = 0 ; a <= i ; a++){
                        var htmls = document.getElementById(deletesize[a]);
                        var $htmls=$(htmls);
                        $htmls.parents("tr").remove()
                    }
                    ids = deletesize;
                    // 定义参数
	                var data = {id:ids};
	                //开启token
	                data['_token'] = $('#token').val();
	                var url = 'alldeletesizeteam';
	                $.post(url,data,function(data){
	                	// console.log(data);
	                });
                }else{ 
                    return false; 
                }
            }else{
                alert("请选择第ID")
            }
        })
        // 下架按钮
        $(".operationButtom .lower").click(function () {
            var lowersize = new Array(); //想要下架的ID
            var i = 0
            $("body input[name='btSelectItem']").each(function(){
                if( $(this).prop("checked") == true){
                    lowersize[i]  = $(this).parent().siblings("td").attr("id");
                    i ++;
                }
            })
            if ( i > 0){
                var msg = "您真的确定要下架吗？"; 
                if (confirm(msg)==true){ 
                    for(var a = 0 ; a <= i ; a++){
                        var htmls = document.getElementById(lowersize[a]);
                        var $htmls=$(htmls);
                        $htmls.parents("tr").find("td").attr("data-state","false").find(".two").text("下架");
                        $htmls.parents("tr").find("td .lower").css("display","none");
                        $htmls.parents("tr").find("td .show").css("display","inline-block");
                        $("body input[type='checkbox']").each(function(){
                            $(this).prop("checked",false);
                        })
                    }
                    ids = lowersize;
                    // 定义参数
	                var data = {id:ids};
	                //开启token
	                data['_token'] = $('#token').val();
	                var url = 'alllowerteam';
	                $.post(url,data,function(data){
	                	// console.log(data);
	                });
                }else{ 
                    return false; 
                }
            }else{
                alert("请选择第ID")
            }
        })
        // 显示按钮
        $(".operationButtom .show").click(function () {
            var showsize = new Array(); //想要显示的ID
            var i = 0
            $("body input[name='btSelectItem']").each(function(){
                if( $(this).prop("checked") == true){
                    showsize[i]  = $(this).parent().siblings("td").attr("id");
                    i ++;
                }
            })
            if ( i > 0){
                var msg = "您真的确定要显示吗？"; 
                if (confirm(msg)==true){ 
                    for(var a = 0 ; a <= i ; a++){
                        var htmls = document.getElementById(showsize[a]);
                        var $htmls=$(htmls);
                        $htmls.parents("tr").find("td").attr("data-state","true").find(".two").text("正常");
                        $htmls.parents("tr").find("td .show").css("display","none");
                        $htmls.parents("tr").find("td .lower").css("display","inline-block");
                        $("body input[type='checkbox']").each(function(){
                            $(this).prop("checked",false);
                        })
                    }
                    ids = showsize;
                    // 定义参数
	                var data = {id:ids};
	                //开启token
	                data['_token'] = $('#token').val();
	                var url = 'allshowsizeteam';
	                $.post(url,data,function(data){
	                	// console.log(data);
	                });
                }else{ 
                    return false; 
                }
            }else{
                alert("请选择第ID")
            }
        })
        // 单个显示
        $(".operationTable .show").click(function () {
        	id = $(this).attr("data-shu");
            var msg = "您真的确定要显示吗？"; 
            if (confirm(msg)==true){ 
                $(this).parents("tr").find("td .show").css("display","none");
                $(this).parents("tr").find("td .lower").css("display","inline-block");
                $(this).parents("tr").find("td").attr("data-state","true").find(".two").text("正常");
                // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'ajaxeditteam';
                $.post(url,data,function(data){
                	// console.log(data);
                });
            }else{ 
                return false; 
            }
        })
        // 单个下架
        $(".operationTable .lower").click(function () {
        	id = $(this).attr("data-shu");
            var msg = "您真的确定要下架吗？"; 
            if (confirm(msg)==true){ 
                $(this).parents("tr").find("td .lower").css("display","none");
                $(this).parents("tr").find("td .show").css("display","inline-block");
                $(this).parents("tr").find("td").attr("data-state","false").find(".two").text("下架");
                // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'ajaxeditteam';
                $.post(url,data,function(data){
                	// console.log(data);
                });
            }else{ 
                return false; 
            }
        })
        // 单个删除
        $(".operationTable .delete").click(function () {
        	id = $(this).attr('data-shu');
            var msg = "您真的确定要删除吗？"; 
            if (confirm(msg)==true){ 
                $(this).parents("tr").remove();
                // 定义参数
                var data = {id:id};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'ajaxdelteam';
                $.post(url,data,function(data){
                	// console.log(data);
                });
            }else{ 
                return false; 
            }
        })
    </script>
@endsection


