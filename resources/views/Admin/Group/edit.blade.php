@extends('layouts.ad')
@section('content')
		<div class="fr conRight">
			<p class="breadcrumb"><span>主页</span>/<span>角色列表</span>/<span>编辑</span></p>
			<!-- 操作区域 -->
			<div class="editBox">
				<ul class="editTab clearfix">
					<li class="fl lihover"><span>内容</span></li>
					<li class="fl"><span>权限</span></li>
				</ul>
				<form action="dogroupedit" method="post" enctype="multipart/form-data" class="addBox" onsubmit="return zuzhitijiao()">
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
					<input type="hidden" name="id" value="{{$info->id}}">
					<ul class="editCon">
						<li>
							<!-- type=text的名为textgroup -->
							<table class="textgroup">
								<tr>
									<td class="title required">角色名称<span>*</span>：</td>
									<td class="text"><input type="text" value="{{$info->title}}" name="title" id="biaoti"></td>
									<td class="prompt">角色名称不能为空</td>
								</tr>
								<tr>
									<td class="title">描述：</td>
									<td class="text"><textarea name="main">{{$info->main}}</textarea></td>
								</tr>
							</table>
							<!-- type=radio的名为radiogroup -->
							<div class="radiogroup clearfix">
								<p class="title fl">状态：</p>
								<?php if ($info->status == 'true'): ?>
									<div class="radioBox">
										<p>
											<input type="radio" name="status" id="true" checked="checked" value="true" />
											<label for="true">正常</label>
										</p>
										<p>
											<input type="radio" name="status" id="false" value="false" />
											<label for="false">下架</label>
										</p>
									</div>
								<?php else: ?>
									<div class="radioBox">
										<p>
											<input type="radio" name="status" id="true" value="true" />
											<label for="true">正常</label>
										</p>
										<p>
											<input type="radio" name="status" id="false" checked="checked" value="false" />
											<label for="false">下架</label>
										</p>
									</div>
								<?php endif ?>
							</div>
						</li>
						<li>
							<?php if ($info->id == '1'): ?>
								<div>超级管理员拥有所有的权限</div>
							<?php else: ?>
								<div class="checkboxgroup clearfix power_parent">
									<p class="title fl">权限：</p>
									<div class="checkboxBox">
										<?php if (in_array("new",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addnew" checked value="new"/>新闻管理</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addnew" value="new"/>新闻管理</p>
										<?php endif ?>

										<?php if (in_array("wenda",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addslider" checked value="wenda"/>问答管理</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addslider" value="wenda"/>问答管理</p>
										<?php endif ?>
										
										<?php if (in_array("banner",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addleave" value="banner" checked/>广告管理</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addleave" value="banner"/>广告管理</p>
										<?php endif ?>
										
										<?php if (in_array("leave",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="adduser" value="leave" checked/>留言管理</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="adduser" value="leave"/>留言管理</p>
										<?php endif ?>
										
										<?php if (in_array("usergroup",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addabout" value="usergroup" checked/>角色管理员管理</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addabout" value="usergroup"/>角色管理员管理</p>
										<?php endif ?>

										<?php if (in_array("jingxiaoshang",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addcaselist" value="jingxiaoshang" checked/>全国经销商管理</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addcaselist" value="jingxiaoshang"/>全国经销商管理</p>
										<?php endif ?>

										<?php if (in_array("shop",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalent" value="shop" checked/>产品管理</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalent" value="shop"/>产品管理</p>
										<?php endif ?>
									</div>
								</div>
								<div class="checkboxgroup clearfix power_chlid" data-power="new">
									<p class="title fl">新闻管理权限：</p>
									<div class="checkboxBox">
										<b>分类</b><br>
										<?php if (in_array("new_fenlei_add",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewadd" value="new_fenlei_add" checked/>新增</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewadd" value="new_fenlei_add"/>新增</p>
										<?php endif ?>

										<?php if (in_array("new_fenlei_del",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewdelete" value="new_fenlei_del" checked/>删除</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewdelete" value="new_fenlei_del"/>删除</p>
										<?php endif ?>

										<?php if (in_array("new_fenlei_edit",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewedit" value="new_fenlei_edit" checked/>编辑</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewedit" value="new_fenlei_edit"/>编辑</p>
										<?php endif ?>

										<?php if (in_array("new_fenlei_show",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewshow" value="new_fenlei_show" checked/>显示</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewshow" value="new_fenlei_show"/>显示</p>
										<?php endif ?>

										<?php if (in_array("new_fenlei_low",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewlow" value="new_fenlei_low" checked/>下架</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewlow" value="new_fenlei_low"/>下架</p>
										<?php endif ?>
										<br><b>列表</b><br>
										<?php if (in_array("new_liebiao_add",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewadd" value="new_liebiao_add" checked/>新增</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewadd" value="new_liebiao_add"/>新增</p>
										<?php endif ?>

										<?php if (in_array("new_liebiao_del",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewdelete" value="new_liebiao_del" checked/>删除</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewdelete" value="new_liebiao_del"/>删除</p>
										<?php endif ?>

										<?php if (in_array("new_liebiao_edit",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewedit" value="new_liebiao_edit" checked/>编辑</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewedit" value="new_liebiao_edit"/>编辑</p>
										<?php endif ?>

										<?php if (in_array("new_liebiao_show",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewshow" value="new_liebiao_show" checked/>显示</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewshow" value="new_liebiao_show"/>显示</p>
										<?php endif ?>

										<?php if (in_array("new_liebiao_low",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewlow" value="new_liebiao_low" checked/>下架</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addnewlow" value="new_liebiao_low"/>下架</p>
										<?php endif ?>
									</div>
								</div>
								<div class="checkboxgroup clearfix power_chlid" data-power="wenda">
									<p class="title fl">问答管理权限：</p>
									<div class="checkboxBox">
										<b>经销商服务</b><br>
										<?php if (in_array("wenda_jxs_add",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addslideradd" value="wenda_jxs_add" checked />新增</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addslideradd" value="wenda_jxs_add"/>新增</p>
										<?php endif ?>

										<?php if (in_array("wenda_jxs_del",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addsliderdelete" value="wenda_jxs_del" checked />删除</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addsliderdelete" value="wenda_jxs_del"/>删除</p>
										<?php endif ?>

										<?php if (in_array("wenda_jxs_edit",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addslideredit" value="wenda_jxs_edit" checked />编辑</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addslideredit" value="wenda_jxs_edit"/>编辑</p>
										<?php endif ?>

										<?php if (in_array("wenda_jxs_show",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addslidershow" value="wenda_jxs_show" checked />显示</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addslidershow" value="wenda_jxs_show"/>显示</p>
										<?php endif ?>

										<?php if (in_array("wenda_jxs_low",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addsliderlow" value="wenda_jxs_low" checked />下架</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addsliderlow" value="wenda_jxs_low"/>下架</p>
										<?php endif ?>
										<br><b>钢琴选购指南</b><br>
										<?php if (in_array("wenda_gqxg_add",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addslideradd" value="wenda_gqxg_add" checked />新增</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addslideradd" value="wenda_gqxg_add"/>新增</p>
										<?php endif ?>

										<?php if (in_array("wenda_gqxg_del",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addsliderdelete" value="wenda_gqxg_del" checked />删除</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addsliderdelete" value="wenda_gqxg_del"/>删除</p>
										<?php endif ?>

										<?php if (in_array("wenda_gqxg_edit",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addslideredit" value="wenda_gqxg_edit" checked />编辑</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addslideredit" value="wenda_gqxg_edit"/>编辑</p>
										<?php endif ?>

										<?php if (in_array("wenda_gqxg_show",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addslidershow" value="wenda_gqxg_show" checked />显示</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addslidershow" value="wenda_gqxg_show"/>显示</p>
										<?php endif ?>

										<?php if (in_array("wenda_gqxg_low",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addsliderlow" value="wenda_gqxg_low" checked />下架</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addsliderlow" value="wenda_gqxg_low"/>下架</p>
										<?php endif ?>
										<br><b>常见问题</b><br>
										<?php if (in_array("wenda_wenti_add",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addslideradd" value="wenda_wenti_add" checked />新增</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addslideradd" value="wenda_wenti_add"/>新增</p>
										<?php endif ?>

										<?php if (in_array("wenda_wenti_del",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addsliderdelete" value="wenda_wenti_del" checked />删除</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addsliderdelete" value="wenda_wenti_del"/>删除</p>
										<?php endif ?>

										<?php if (in_array("wenda_wenti_edit",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addslideredit" value="wenda_wenti_edit" checked />编辑</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addslideredit" value="wenda_wenti_edit"/>编辑</p>
										<?php endif ?>

										<?php if (in_array("wenda_wenti_show",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addslidershow" value="wenda_wenti_show" checked />显示</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addslidershow" value="wenda_wenti_show"/>显示</p>
										<?php endif ?>

										<?php if (in_array("wenda_wenti_low",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addsliderlow" value="wenda_wenti_low" checked />下架</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addsliderlow" value="wenda_wenti_low"/>下架</p>
										<?php endif ?>
									</div>
								</div>
								<div class="checkboxgroup clearfix power_chlid" data-power="banner">
									<p class="title fl">广告管理权限：</p>
									<div class="checkboxBox">
										<?php if (in_array("banner_add",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addleaveadd" value="banner_add" checked/>新增</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addleaveadd" value="banner_add"/>新增</p>
										<?php endif ?>

										<?php if (in_array("banner_del",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addleavedelete" value="banner_del" checked/>删除</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addleavedelete" value="banner_del"/>删除</p>
										<?php endif ?>

										<?php if (in_array("banner_edit",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addleaveedit" value="banner_edit" checked/>编辑</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addleaveedit" value="banner_edit"/>编辑</p>
										<?php endif ?>

										<?php if (in_array("banner_show",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addleaveshow" value="banner_show" checked/>显示</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addleaveshow" value="banner_show"/>显示</p>
										<?php endif ?>

										<?php if (in_array("banner_low",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addleavelow" value="banner_low" checked/>下架</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addleavelow" value="banner_low"/>下架</p>
										<?php endif ?>
									</div>
								</div>
								<div class="checkboxgroup clearfix power_chlid" data-power="leave">
									<p class="title fl">留言管理权限：</p>
									<div class="checkboxBox">
										<?php if (in_array("leave_del",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="adduserdelete" value="leave_del" checked />删除</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="adduserdelete" value="leave_del"/>删除</p>
										<?php endif ?>
									</div>
								</div>
								<div class="checkboxgroup clearfix power_chlid" data-power="usergroup">
									<p class="title fl">角色管理员管理权限：</p>
									<div class="checkboxBox">
										<b>角色</b><br>
										<?php if (in_array("group_add",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutadd" value="group_add" checked />新增</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutadd" value="group_add"/>新增</p>
										<?php endif ?>

										<?php if (in_array("group_del",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutdelete" value="group_del" checked />删除</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutdelete" value="group_del"/>删除</p>
										<?php endif ?>

										<?php if (in_array("group_edit",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutedit" value="group_edit" checked />编辑</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutedit" value="group_edit"/>编辑</p>
										<?php endif ?>

										<?php if (in_array("group_show",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutshow" value="group_show" checked />显示</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutshow" value="group_show"/>显示</p>
										<?php endif ?>

										<?php if (in_array("group_low",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutlow" value="group_low" checked />下架</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutlow" value="group_low"/>下架</p>
										<?php endif ?>
										<br><b>用户</b><br>
										<?php if (in_array("user_add",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutadd" value="user_add" checked />新增</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutadd" value="user_add"/>新增</p>
										<?php endif ?>

										<?php if (in_array("user_del",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutdelete" value="user_del" checked />删除</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutdelete" value="user_del"/>删除</p>
										<?php endif ?>

										<?php if (in_array("user_edit",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutedit" value="user_edit" checked />编辑</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutedit" value="user_edit"/>编辑</p>
										<?php endif ?>

										<?php if (in_array("user_show",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutshow" value="user_show" checked />显示</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutshow" value="user_show"/>显示</p>
										<?php endif ?>

										<?php if (in_array("user_low",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutlow" value="user_low" checked />下架</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addaboutlow" value="user_low"/>下架</p>
										<?php endif ?>
									</div>
								</div>
								<div class="checkboxgroup clearfix power_chlid" data-power="jingxiaoshang">
									<p class="title fl">全国经销商管理权限：</p>
									<div class="checkboxBox">
										<?php if (in_array("jingxiaoshang_add",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addcaselistadd" value="jingxiaoshang_add" checked/>新增</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addcaselistadd" value="jingxiaoshang_add"/>新增</p>
										<?php endif ?>

										<?php if (in_array("jingxiaoshang_del",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addcaselistdelete" value="jingxiaoshang_del" checked/>删除</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addcaselistdelete" value="jingxiaoshang_del"/>删除</p>
										<?php endif ?>

										<?php if (in_array("jingxiaoshang_edit",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addcaselistedit" value="jingxiaoshang_edit" checked/>编辑</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addcaselistedit" value="jingxiaoshang_edit"/>编辑</p>
										<?php endif ?>

										<?php if (in_array("jingxiaoshang_show",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addcaselistshow" value="jingxiaoshang_show" checked/>显示</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addcaselistshow" value="jingxiaoshang_show"/>显示</p>
										<?php endif ?>

										<?php if (in_array("jingxiaoshang_low",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addcaselistlow" value="jingxiaoshang_low" checked/>下架</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addcaselistlow" value="jingxiaoshang_low"/>下架</p>
										<?php endif ?>
									</div>
								</div>
								<div class="checkboxgroup clearfix power_chlid" data-power="shop">
									<p class="title fl">产品管理权限：</p>
									<div class="checkboxBox">
										<b>品牌分类列表</b><br>
										<?php if (in_array("shop_pinpai_add",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentadd" value="shop_pinpai_add" checked/>新增</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentadd" value="shop_pinpai_add"/>新增</p>
										<?php endif ?>

										<?php if (in_array("shop_pinpai_del",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentdelete" value="shop_pinpai_del" checked/>删除</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentdelete" value="shop_pinpai_del"/>删除</p>
										<?php endif ?>

										<?php if (in_array("shop_pinpai_edit",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentedit" value="shop_pinpai_edit" checked/>编辑</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentedit" value="shop_pinpai_edit"/>编辑</p>
										<?php endif ?>

										<?php if (in_array("shop_pinpai_show",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentshow" value="shop_pinpai_show" checked/>显示</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentshow" value="shop_pinpai_show"/>显示</p>
										<?php endif ?>

										<?php if (in_array("shop_pinpai_low",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentlow" value="shop_pinpai_low" checked/>下架</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentlow" value="shop_pinpai_low"/>下架</p>
										<?php endif ?>
										<br><b>产品列表</b><br>
										<?php if (in_array("shop_chanpin_add",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentadd" value="shop_chanpin_add" checked/>新增</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentadd" value="shop_chanpin_add"/>新增</p>
										<?php endif ?>

										<?php if (in_array("shop_chanpin_del",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentdelete" value="shop_chanpin_del" checked/>删除</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentdelete" value="shop_chanpin_del"/>删除</p>
										<?php endif ?>

										<?php if (in_array("shop_chanpin_edit",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentedit" value="shop_chanpin_edit" checked/>编辑</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentedit" value="shop_chanpin_edit"/>编辑</p>
										<?php endif ?>

										<?php if (in_array("shop_chanpin_show",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentshow" value="shop_chanpin_show" checked/>显示</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentshow" value="shop_chanpin_show"/>显示</p>
										<?php endif ?>

										<?php if (in_array("shop_chanpin_low",$info->menu_auth)): ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentlow" value="shop_chanpin_low" checked/>下架</p>
										<?php else: ?>
											<p><input type="checkbox" name="quanxian[]" id="addtalentlow" value="shop_chanpin_low"/>下架</p>
										<?php endif ?>
									</div>
								</div>
							<?php endif ?>
						</li>
						
					</ul>
					<!-- 提交 -->
					<div class="closure">
						<input type="submit" class="closurestyle" value="确定" />
						<a href="javascript:history.go(-1);" class="closurestyle Close">取消</a>
					</div>
				</form>
			</div>
		</div>
	</div>
	<style>
		.power_chlid{display: none;}
	</style>
	<script>
		// 操作区域的最大高度
		var boxheightqq = $(".editBox").outerHeight(true);
		var edittabheightqq = $(".editTab").outerHeight(true);
		var closureheightqq = $(".editBox .closure").outerHeight(true);
		// alert(boxheightqq-edittabheightqq-closureheightqq-55)
		$(".editBox .editCon").css("max-height",boxheightqq-edittabheightqq-closureheightqq-55);
		// 单张图片上传
		function preview(obj) {
			var $obj=$(obj);				//js转jQuery
			var $dom = $obj.parents(".inputPic").siblings(".picShow");
			var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
			if (obj.files && obj.files[0]) {
				var reader = new FileReader();
				reader.onload = function(evt) {
					$dom.html("<p class='picBox'><img src='" + evt.target.result + "'><i class='iconfont icon-shanchu'></i></p>");
				}
				reader.readAsDataURL(obj.files[0]);
				$obj.siblings("span").html("<i class='iconfont icon-shangchuan'></i>修改图片")
			} else {
				$dom.find("span").remove();
			}
		}
		//多张图片上传
			// 一个多图一份这个，picssrc进行改变；并且把函数放到点击事件里
			var picssrc = new Array();
			function picssrcfun(){
				var size =$(".picsUploading .picShow[name='picssrc'] .picBox").size();
				for (var a = 0; a < size; a++) {
					picssrc[a] = $(".picsUploading .picShow[name='picssrc'] .picBox").eq(a).find("img").attr("src");
				}
			}
			$(".editBox .closure .input[type='submit'] ").click(function(){
				picssrcfun()
			})
		function previews(obj){
			var $obj=$(obj);				//js转jQuery
			var $dom = $obj.parents(".inputPic").siblings(".picShow");
			var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
			if (obj.files) {
				for(i = 0; i < obj.files.length ; i ++) {
					var reader = new FileReader();    
					reader.readAsDataURL(obj.files[i]);  
					reader.onload=function(e){  
						//多图预览
						$dom.append("<p class='picBox'><img src='" + this.result + "'><i class='iconfont icon-shanchu'></i></p>");
					}
				}
			} else {
				$dom.html(domhtml);
			}
		}
		// 图片删除
		$(".picShow").on("click",".picBox i",function(){
			$(this).parents(".picShow").siblings(".inputPic").find(".upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
			$(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").after($(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").clone().val(""));
			$(this).parent(".picBox").remove();
		})
		// 重置
		$(".closure input[type='reset']").click(function(){
			$(".addPic .picShow").html("");
			$("input[type='file']").each(function () {
				$(this).after($(this).clone().val(""));
				// $(this).remove();
			})
			$(this).parent(".closure").siblings(".AddEditBottom").find(".addPic .upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
		})
		//提示的显示
		$("input").focusout(function(){
			if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
				$(this).parent("td").siblings(".prompt").css("opacity","1");
			}else{
				$(this).parent("td").siblings(".prompt").css("opacity","0");
			}
		})
		$("textarea").focusout(function(){
			if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
				$(this).parent("td").siblings(".prompt").css("opacity","1");
			}else{
				$(this).parent("td").siblings(".prompt").css("opacity","0");
			}
		})
		// 操作区域界面
		$(".editTab li").click(function(){
			$(this).addClass("lihover").siblings().removeClass("lihover");
			$(".editBox .editCon li").eq($(this).index()).css("display","block").siblings().css("display","none");
		})
		//表单提交
		function zuzhitijiao(){
			biaoti = document.getElementById('biaoti').value;
			if (biaoti) {
				return true;
			}else{
				alert('角色名称不能为空');
				return false;
			}
		}
		// 判断是否隐藏
		$("#.power_parent .checkboxBox p").each(function(){
			if($(this).find("input").is(':checked')){
				$(".power_chlid[data-power='"+$(this).find("input").val()+"']").css("display","block");
			}
		})
		// 表单的显示与隐藏
		$(".power_parent .checkboxBox p").click(function(){
			var power = $(this).find("input").val();
			$(".power_chlid[data-power='"+power+"'] p").each(function(){
				$(this).find("input").prop("checked",false);
			})
			if( $(this).find("input").is(':checked')){
				$(".power_chlid[data-power='"+power+"']").css("display","block");
			}else{
				$(".power_chlid[data-power='"+power+"']").css("display","none");
			}
		})
	</script>
@endsection
 