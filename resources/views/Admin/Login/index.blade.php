<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
<meta charset="UTF-8" />
<meta http-equiv="X-UA-Compatible" content="IE=edge"> 
<meta name="viewport" content="width=device-width, initial-scale=1"> 
<title>login</title>
<link rel="stylesheet" type="text/css" href="./login_admin/css/normalize.css" />
<link rel="stylesheet" type="text/css" href="./login_admin/css/demo.css" />
<link rel="apple-touch-icon" type="image/x-icon" href="/img/saipu.ico">
<link rel="shortcut icon" type="image/x-icon" href="/img/saipu.ico">
<!--必要样式-->
<link rel="stylesheet" type="text/css" href="./login_admin/css/component.css" />
<!-- <link rel="apple-touch-icon" type="image/x-icon" href="./xingtu.ico"> -->
<!-- <link rel="shortcut icon" type="image/x-icon" href="./xingtu.ico"> -->
<!--[if IE]>
<script src="js/html5.js"></script>
<![endif]-->
<style type="text/css">
	.logo_box{ background-color:rgba(0,0,0,0.2)}
    .input_outer{margin-bottom: 20px;}
    .x_code{width:210px;float:left;}
    .x_codes{float:right;height: 46px;
    padding: 0 5px;
    margin-bottom: 30px;
    border-radius: 50px;
    position: relative;
	text-align: center;
	-webkit-box-flex: 1;
    line-height: 46px;
    background: #fff;
    border-radius: inherit;
    margin-left: 10px;}
    .x_logoin{background:url(./login_admin/img/5.png) repeat;overflow: hidden;clear: both;}
    .x_box{display:-webkit-box;}
    .x_text{font-size:12px;}
</style>
</head>
<body>
		<div class="container demo-1">
			<div class="content">
				<div id="large-header" class="large-header">
					<canvas id="demo-canvas"></canvas>
					<div class="logo_box">
						<img src="./login_admin/img/1.png">
						<h3 style="margin:0px 0px 10px 0px;">后台管理系统</h3>
						<form action="dologin" name="f" method="post" onSubmit="return false;">
						<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
							<div class="input_outer">
								<image src="./login_admin/img/login.png" class="user"></image>
								<input  class="texts dian"  type="text"   placeholder="请输入账户名" name="username" id="username">
								<!-- <input name="logname" class="text" style="color: #FFFFFF !important" type="text" placeholder="请输入账户"> -->
							</div>
							<div class="input_outer">
								<image src="./login_admin/img/password.png" class="user2"></image>
								<input  class="texts dian"  type="password"   placeholder="请输入密码" name="password" id="password">
								<!-- <input name="logname" class="text" style="color: #FFFFFF !important" type="text" placeholder="请输入账户"> -->
							</div>
							<div class="x_box">
								<div class="input_outer x_code s">
									<image src="./login_admin/img/login_y.png" class="user1"></image>
									<input name="yzm" class="yzm" type="text" placeholder="请输入验证码" style="line-height: 55px;height: 55px;padding: 0;margin:0 0 0 65px;box-sizing: border-box;" id="shuzi">
									<input type="hidden" value="123" name="num" id="test1">
								</div>
								<!-- <div class="x_codes" style="width: 112px;height: 46px;"><a href="#" style="display:block;"><img src="" onclick="changeyzm()" id="yzmimg" style="width: 112px;height: 46px;"></a></div> -->
								<a href="#" id="changeImg"><canvas id="canvas" width="120" height="40" class="yanzhengma"></canvas></a>
    							<!-- <a href="#" id="changeImg">看不清，换一张</a> -->
							</div>
							<!-- <div class="mb2 x_logoin"><a class="act-but submit" href="javascript:;" style="background:none;color: #FFFFFF">登录</a></div> -->
							<div class="mb2 x_logoin"><input type="submit" value="登录" class="act-but submit" style="background:none;border:0px;color:#FFFFFF;margin:20px auto;" onclick="dianji()"></div>
							<div class="x_text" style="margin-top: -26px;"><span>请不要在公共场合登录本系统</span> <span>请保管好您的密码</span></div>
						</form>
					</div>
				</div>
			</div>
		</div><!-- /container -->
		<script src="./login_admin/js/TweenLite.min.js"></script>
		<script src="./login_admin/js/EasePack.min.js"></script>
		<script src="./login_admin/js/rAF.js"></script>
		<script src="./login_admin/js/demo-1.js"></script>
		<script type="text/javascript" src="js/jquery-1.7.3.min.js"></script>
		<!-- <script type="text/javascript" src="./login_admin/js/md5.js"></script> -->
		<script>
        /**生成一个随机数**/
        function randomNum(min,max){
            return Math.floor( Math.random()*(max-min)+min);
        }
        /**生成一个随机色**/
        function randomColor(min,max){
            var r = randomNum(min,max);
            var g = randomNum(min,max);
            var b = randomNum(min,max);
            return "rgb("+r+","+g+","+b+")";
        }
        drawPic();
        document.getElementById("changeImg").onclick = function(e){
            e.preventDefault();
            drawPic();
        }

        /**绘制验证码图片**/
        function drawPic(){
            var canvas=document.getElementById("canvas");
            // console.log(canvas);
            var width=canvas.width;
            var height=canvas.height;
            var ctx = canvas.getContext('2d');
            ctx.textBaseline = 'bottom';
            // console.log(ctx.textBaseline);

            /**绘制背景色**/
            ctx.fillStyle = randomColor(360,360); //颜色若太深可能导致看不清
            ctx.fillRect(0,0,width,height);
            /**绘制文字**/
            str = '1234567890';
            shu = '';
            for(var i=0; i<4; i++){
                var txt = str[randomNum(0,str.length)];
                shu = shu + txt;
                ctx.fillStyle = randomColor(50,160);  //随机生成字体颜色
                ctx.font = randomNum(15,40)+'px SimHei'; //随机生成字体大小
                var x = 10+i*25;
                var y = randomNum(25,45);
                var deg = randomNum(-45, 45);
                //修改坐标原点和旋转角度
                ctx.translate(x,y);
                ctx.rotate(deg*Math.PI/180);
                ctx.fillText(txt, 0,0);
                //恢复坐标原点和旋转角度
                ctx.rotate(-deg*Math.PI/180);
                ctx.translate(-x,-y);
            }
            // console.log(shu);
            var test = document.getElementById("test1");
            test.value = shu;
            // console.log(test.value);

            // /**绘制干扰线**/
            // for(var i=0; i<8; i++){
            //     ctx.strokeStyle = randomColor(40,180);
            //     ctx.beginPath();
            //     ctx.moveTo( randomNum(0,width), randomNum(0,height) );
            //     ctx.lineTo( randomNum(0,width), randomNum(0,height) );
            //     ctx.stroke();
            // }
            // /**绘制干扰点**/
            // for(var i=0; i<100; i++){
            //     ctx.fillStyle = randomColor(0,255);
            //     ctx.beginPath();
            //     ctx.arc(randomNum(0,width),randomNum(0,height), 1, 0, 2*Math.PI);
            //     ctx.fill();
            // }
        }
        function dianji(){
        	username = document.getElementById('username').value;
        	password = document.getElementById('password').value;
        	shuzi = document.getElementById('shuzi').value;
        	// 定义参数
            var data = {username:username,password:password};
            //开启token
            data['_token'] = $('#token').val();
            var url = 'admin_dologin';
            $.post(url,data,function(data){
            	console.log(data);
	            if (data == '0') {
	            	alert('用户名不存在或者被禁用');return false;
	            }
	            if (data['password'] == '0') {
	            	alert('密码错误');return false;
	            }
	            if (shuzi !== shu) {
	            	alert('验证码不正确');return false;
	            }
                if (data['status'] == 'false') {
                    alert('账户被禁用,请联系管理员');return false;
                }
            	// document.write('登录成功,将在三秒后跳转');
            	var str = window.location.href;
                var str = str.split('/');
                var dijige = str.length - 1;
                str[dijige] = 'admin_login_true';
                str = str.join('/');
                window.location.href = str;
            });
        }
    </script>
	</body>
</html>