@extends('layouts.ad')
@section('content')
		<div class="fr conRight">
			<p class="breadcrumb"><span>赛普主页</span>/<span>案例列表</span>/<span>编辑</span></p>
			<!-- 操作区域 -->
			<div class="editBox">
				<ul class="editTab clearfix">
					<li class="fl lihover"><span>内容</span></li>
					<li class="fl"><span>图片管理</span></li>
					<li class="fl"><span>多图片管理</span></li>
					<li class="fl"><span>详情</span></li>
				</ul>
				<form action="{{url('docaselistedit')}}" method="post" enctype="multipart/form-data" class="addBox" onsubmit="return zuzhitijiao()">
					<input type="hidden" name="_token" value="{{csrf_token()}}" id="token">
					<input type="hidden" name="id" value="{{$info->id}}">
					<input type="hidden" name="shanchu" value="bushan" id="shanchu">
					<ul class="editCon">
						<li>
							<!-- type=text的名为textgroup -->
							<table class="textgroup">
								<tr>
									<td class="title required">标题<span>*</span>：</td>
									<td class="text"><input type="text" value="{{$info->title}}" name="title" id="biaoti" placeholder="请填写标题"></td>
									<td class="prompt">标题不能为空</td>
								</tr>
								<tr>
									<td class="title required">颜色<span></span>：</td>
									<td class="text"><input type="text" value="{{$info->yanse}}" name="yanse" id="biaoti" placeholder="请填写HTML颜色值"></td>
								</tr>
								<tr>
									<td class="title required">英文名称<span></span>：</td>
									<td class="text"><input type="text" value="{{$info->english}}" name="english" id="biaoti" placeholder="请填写英文名"></td>
								</tr>
								<tr>
									<td class="title">描述：</td>
									<td class="text"><textarea name="main" placeholder="字数请控制在50字以内">{{$info->main}}</textarea></td>
								</tr>
								<!-- <tr>
									<td class="title required">设计师头像<span></span>：</td>
									<td class="text"><input type="text" value="{{$info->touxiang}}" name="touxiang" id="biaoti" placeholder="请填写设计师头像"></td>
								</tr> -->
								<tr>
									<td class="title required">设计师名字<span></span>：</td>
									<td class="text"><input type="text" value="{{$info->shejishi}}" name="shejishi" id="biaoti" placeholder="请填写设计师名字"></td>
								</tr>
								<tr>
									<td class="title required">设计师经验<span></span>：</td>
									<td class="text"><input type="text" value="{{$info->jianjie}}" name="jianjie" id="biaoti" placeholder="请填写设计师简介"></td>
								</tr>
							</table>
							<!-- type=radio的名为radiogroup -->
							<div class="radiogroup clearfix">
								<p class="title fl">状态：</p>
								<?php if ($info['status'] == 'true'): ?>
									<div class="radioBox">
										<p>
											<input type="radio" name="status" id="true" checked="checked" value="true" />
											<label for="true">正常</label>
										</p>
										<p>
											<input type="radio" name="status" id="false" value="false" />
											<label for="false">下架</label>
										</p>
									</div>
								<?php else: ?>
									<div class="radioBox">
										<p>
											<input type="radio" name="status" id="true" value="true" />
											<label for="true">正常</label>
										</p>
										<p>
											<input type="radio" name="status" id="false" checked="checked" value="false" />
											<label for="false">下架</label>
										</p>
									</div>
								<?php endif ?>
						</li>
						<li>
							<!-- 新增的图片 -->
							<div class="addPic">
								<!-- 单张图片上传 -->
								<div class="picUploading">
									<div class="inputPic clearfix">
										<p class="title fl">单张图片：</p>
										<p class="upload fl">
											<span><i class="iconfont icon-shangchuan"></i>上传图片</span>
											<input type="file" date-shu="one" name="icon" onchange="preview(this)" />
										</p>
										<p class="prompt fl">建议大小:340 x 256 px</p>
									</div>
									<div class="picShow">
										<p class="picBox">
											<?php if (empty($info['icon'])): ?>
											<?php else: ?>
												<img src="{{$info->icon}}" alt="">
												<i class="iconfont icon-shanchu" data-shande="icon"></i>
											<?php endif ?>
										</p>
									</div>
								</div>
								<div class="picUploading">
									<div class="inputPic clearfix">
										<p class="title fl">人头：</p>
										<p class="upload fl">
											<span><i class="iconfont icon-shangchuan"></i>上传图片</span>
											<input type="file" date-shu="one" name="touxiang" onchange="preview(this)" />
										</p>
										<p class="prompt fl">建议大小:340 x 256 px</p>
									</div>
									<div class="picShow">
										<p class="picBox">
											<?php if (empty($info['touxiang'])): ?>
												
											<?php else: ?>
												<img src="{{$info->touxiang}}" alt="">
												<i class="iconfont icon-shanchu" data-shande="touxiang"></i>
											<?php endif ?>
											
										</p>
									</div>
								</div>
							</div>
						</li>
						<li>
							<div class="picsUploading">
								<div class="inputPic clearfix">
									<p class="title fl">多张图片：</p>
									<p class="upload fl">
										<span><i class="iconfont icon-shangchuan"></i>上传图片</span>
										<input type="file" date-shu="more" multiple="multiple" name="duotu[]" onchange="previews(this)" />
									</p>
									<p class="prompt fl">建议大小:340 x 256 px</p>
								</div>
								<div class="picShow">
									@foreach($img as $v)
										<p class="picBox">
											<img src="{{$v->icon}}" alt="">
											<i class="iconfont icon-shanchu" data-shu="{{$v->id}}"></i>
										</p>
									@endforeach
								</div>
							</div>
						</li>
						<li>
							<div class="tab-pane">
								<!-- <h1>百度编辑器</h1> -->
								<textarea name="baidu" id="intro_detail">{{$info->miaoshu}}</textarea>
							</div>
						</li>
					</ul>
					<!-- 提交 -->
					<div class="closure">
						<input type="submit" class="closurestyle" value="确定" onclick="xianyibutijiao()" />
						<a href="javascript:history.go(-1);" class="closurestyle Close">取消</a>
						<input type="reset" class="closurestyle" value="重置" onmousedown="return $EDITORUI[&quot;edui51&quot;]._onMouseDown(event, this);" onclick="return $EDITORUI[&quot;edui51&quot;]._onClick(event, this);"/>
					</div>
				</form>
			</div>
		</div>
	</div>
	<script>
	// 操作区域的最大高度
	var boxheightqq = $(".editBox").outerHeight(true);
	var edittabheightqq = $(".editTab").outerHeight(true);
	var closureheightqq = $(".editBox .closure").outerHeight(true);
		// alert(boxheightqq-edittabheightqq-closureheightqq-55)
	$(".editBox .editCon").css("max-height",boxheightqq-edittabheightqq-closureheightqq-55);
		// 单张图片上传
		function preview(obj) {
			var $obj=$(obj);				//js转jQuery
			var $dom = $obj.parents(".inputPic").siblings(".picShow");
			var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
			if (obj.files && obj.files[0]) {
				var reader = new FileReader();
				reader.onload = function(evt) {
					$dom.html("<p class='picBox'><img src='" + evt.target.result + "'><i class='iconfont icon-shanchu'></i></p>");
				}
				reader.readAsDataURL(obj.files[0]);
				$obj.siblings("span").html("<i class='iconfont icon-shangchuan'></i>修改图片")
				shanbushan = document.getElementById('shanchu');
				shanbushan.value = 'bushan';
			} else {
				$dom.find("span").remove();
			}
		}
		//多张图片上传
			// 一个多图一份这个，picssrc进行改变；并且把函数放到点击事件里
			var picssrc = new Array();
			function picssrcfun(){
				var size =$(".picsUploading .picShow[name='picssrc'] .picBox").size();
				for (var a = 0; a < size; a++) {
					picssrc[a] = $(".picsUploading .picShow[name='picssrc'] .picBox").eq(a).find("img").attr("src");
				}
			}
			$(".editBox .closure .input[type='submit'] ").click(function(){
				picssrcfun()
			})
		function previews(obj){
			var $obj=$(obj);				//js转jQuery
			var $dom = $obj.parents(".inputPic").siblings(".picShow");
			var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
			if (obj.files) {
				for(i = 0; i < obj.files.length ; i ++) {
					var reader = new FileReader();    
					reader.readAsDataURL(obj.files[i]);  
					reader.onload=function(e){  
						//多图预览
						$dom.append("<p class='picBox'><img src='" + this.result + "'><i class='iconfont icon-shanchu'></i></p>");
					}
				}
			} else {
				$dom.html(domhtml);
			}
		}
		// 图片删除
		quanjubianliang = 0;
		shanchuid = new Array();
		$(".picShow").on("click",".picBox i",function(){
			$(this).parents(".picShow").siblings(".inputPic").find(".upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
			$(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").after($(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").clone().val(""));
			$(this).parent(".picBox").remove();
			shanbushan = document.getElementById('shanchu');
			shanbushan.value = $(this).attr("data-shande");
			shanchuid[quanjubianliang] = $(this).attr("data-shu");
			quanjubianliang = quanjubianliang + 1;
			// console.log(shanchuid);
		})
		// 重置
		$(".closure input[type='reset']").click(function(){
			$(".addPic .picShow").html("");
			$("input[type='file']").each(function () {
				$(this).after($(this).clone().val(""));
				// $(this).remove();
			})
			$(this).parent(".closure").siblings(".AddEditBottom").find(".addPic .upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
		})
		//提示的显示
		$("input").focusout(function(){
			if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
				$(this).parent("td").siblings(".prompt").css("opacity","1");
			}else{
				$(this).parent("td").siblings(".prompt").css("opacity","0");
			}
		})
		$("textarea").focusout(function(){
			if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
				$(this).parent("td").siblings(".prompt").css("opacity","1");
			}else{
				$(this).parent("td").siblings(".prompt").css("opacity","0");
			}
		})
		// 操作区域界面
		$(".editTab li").click(function(){
			$(this).addClass("lihover").siblings().removeClass("lihover");
			$(".editBox .editCon li").eq($(this).index()).css("display","block").siblings().css("display","none");
		})
		//表单提交
		function zuzhitijiao(){
			biaoti = document.getElementById('biaoti').value;
			if (biaoti) {
				return true;
			}else{
				alert('标题必填');
				return false;
			}
		}
	</script>
	<script>
		function xianyibutijiao(){
			id = shanchuid;
			// 定义参数
            var data = {id:id};
            //开启token
            data['_token'] = $('#token').val();
            var url = 'xianyibutijiao';
            $.post(url,data,function(data){
            	// console.log(data);
            });
		}
	</script>
@endsection
 