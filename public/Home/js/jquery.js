$(function(){
	// var Height =$(window).height()
	var Widthnew =$(window).width();
	$(window).resize(function(){
		if(Widthnew != $(window).width()){
			window.location.reload()
		}
	});
	if( Widthnew >1000 ){
		$(".nybanner").css("min-height", ($(window).width()/1920)*400+"px");
		if( document.body.scrollHeight < $(window).height()){
			$("footer").css("margin-top",( $(window).height()-document.body.scrollHeight )+"px")
		}
	}else{
		$(".nybanner").css("min-height", ($(window).width()/750)*400+"px");
	}
	// console.log(($(window).width()/1920)*400,document.body.scrollHeight,$(window).height())

	$(".gototop").click(function () {
		jQuery('html, body').animate({
			scrollTop:0
		}, 1000); 	
	})

	$(window).scroll(function(){
		if($("header").offset().top > $(window).height()/2  &&  Widthnew>1000){
			$(".gototop").css("display","block");
		}else{
			$(".gototop").css("display","none");
		}
	})
	// 导航
		// pc导航
			// (语言)
			$("header .select_box p").click(function(){
				if($(this).siblings("ul").css("opacity") != 1){
					$(this).siblings("ul").css("opacity","1").css("height","50px");
				}else{
					$(this).siblings("ul").css("opacity","0").css("height","0");
				}
			})
			$("header .select_box").hover(function(){
			},function(){
				$("header .select_box ul").css("opacity","0").css("height","0");
			})
		// 手机导航
			//公共导航点击下拉
			$(".hasLi>a").click(function(){
				$(".hasLi").find("span").css("transform","rotate(90deg)");
				$(".hasLi").css("height","15vw");
				$(".hasLi").find("ul").css("height","0rem");
				if( $(this).attr("date-state") != 1 ){
					$(this).find("span").css("transform","rotate(270deg)");
					$(this).parent("li").css("height",15*($(this).siblings("ul").find("li").size()+1)+"vw");
					$(this).siblings("ul").css("height",15*$(this).siblings("ul").find("li").size()+"vw");
					$(this).css("border-left","3px solid #dab866").attr("date-state","1").parent(".hasLi").siblings().find("a").css("border-left","none").attr("date-state","0");
				}else{
					$(this).find("span").css("transform","rotate(90deg)");
					$(this).parent("li").css("height","15vw");
					$(this).siblings("ul").css("height","0rem");
					$(this).css("border-left","none").attr("date-state","0")
				}
			})
			//公共导航的出现隐藏(点击菜单)
			$(".mune").click(function(){
				if( $(this).attr("date-state") != 1 ){
					$(".zhezhao").css("display","block");
					$(".navs").css("transition","width 0.5s,opacity 0.4s").css("width","70vw").css("opacity","1");
					$(this).attr("date-state","1");
					$(this).find("img").attr("src","Home/img/close.png");
				}else{
					$(".zhezhao").css("display","none");
					$(".navs").css("transition","width 0.5s,opacity 0.4s 0.1s").css("width","0%").css("opacity","0");
					$(this).attr("date-state","0");
					$(this).find("img").attr("src","Home/img/mune.png");
				}
			})
			//公共导航的隐藏(点击黑色区域)
			$(".zhezhao").click(function(){
				$(".mune").attr("date-state","0");
				$(".mune").find("img").attr("src","Home/img/mune.png");
				$(".navs").css("transition","width 0.5s,opacity 0.4s 0.1s").css("width","0%").css("opacity","0");
				$(this).css("display","none");
			})
})