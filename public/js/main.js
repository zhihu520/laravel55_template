//首页操作start
    // 菜单栏
    var str = document.location.href;
    var tmparr1 = str.split("/");
    g = tmparr1['3'];
    // console.log(g);
    var tmparr2 = str.split("?");
    var tmparr3 = tmparr2[0].split("/");
    b = tmparr3['3'];
    // console.log(b);
    if (b == 'admin_news_group') {
        $("#xinwenguanli").attr("date-state","true");
        $("#xinwenfenlei").attr("class","liclick");
    }else if (b == 'admin_news_group_add') {
        $("#xinwenguanli").attr("date-state","true");
        $("#xinwenfenlei").attr("class","liclick");
    }else if (b == 'admin_news_group_edit') {
        $("#xinwenguanli").attr("date-state","true");
        $("#xinwenfenlei").attr("class","liclick");
    }else if (b == 'admin_news_list') {
        $("#xinwenguanli").attr("date-state","true");
        $("#xinwenliebiao").attr("class","liclick");
    }else if (b == 'admin_news_list_add') {
        $("#xinwenguanli").attr("date-state","true");
        $("#xinwenliebiao").attr("class","liclick");
    }else if (b == 'admin_news_list_edit') {
        $("#xinwenguanli").attr("date-state","true");
        $("#xinwenliebiao").attr("class","liclick");
    }else if (b == 'admin_question') {
        $("#question").attr("date-state","true");
        $("#changjianwenti").attr("class","liclick");
    }else if (b == 'admin_question_add') {
        $("#question").attr("date-state","true");
        $("#changjianwenti").attr("class","liclick");
    }else if (b == 'admin_question_edit') {
        $("#question").attr("date-state","true");
        $("#changjianwenti").attr("class","liclick");
    }else if (b == 'admin_case') {
        $("#case").attr("date-state","true");
        $("#changjiananli").attr("class","liclick");
    }
    else if (b == 'admin_anli_add') {
        $("#case").attr("date-state","true");
        $("#changjiananli").attr("class","liclick");
    }else if (b == 'admin_anli_edit') {
        $("#case").attr("date-state","true");
        $("#changjiananli").attr("class","liclick");
    }else if (b == 'admin_case_group') {
        $("#case").attr("date-state","true");
        $("#changjiananli_group").attr("class","liclick");
    }else if (b == 'admin_anli_group_add') {
        $("#case").attr("date-state","true");
        $("#changjiananli_group").attr("class","liclick");
    }else if (b == 'admin_anli_group_edit') {
        $("#case").attr("date-state","true");
        $("#changjiananli_group").attr("class","liclick");
    }else if (b == 'admin_about') {
        $("#system").attr("date-state","true");
        $("#gongsijianjie").attr("class","liclick");
    }else if (b == 'admin_info') {
        $("#system").attr("date-state","true");
        $("#gongsixinxi").attr("class","liclick");
    }
    //页面设置
    // bottom nav_0的高
    var windowheight = $(window).height();
    var windowwidth = $(window).width();
    var headerheight = $(".header").outerHeight(true);
    var uesrheight = $(".conLeft .uesr").outerHeight(true);
    var formheight = $(".conLeft form").outerHeight(true);
    var leftwidth = $(".conLeft").outerWidth(true);
    if($(window).width() > 700){
        $(".bottom").height(windowheight-headerheight);
        $(".leftBox").height(windowheight-headerheight-uesrheight-formheight);
        $(".conRight").width(windowwidth-leftwidth);
    }
    // nav_0下拉(开始就要打开的.nav_1 加 date-state="true")
        var navheight = new Array();
        var navsize = $(".nav_0 .nav_1").size()
        for( var i= 0 ; i < navsize ; i++ ){
            navheight[i] = $(".nav_0 .nav_1").eq(i).find(".nav_2").outerHeight(true);
            if($(".nav_0 .nav_1").eq(i).find("a").attr("date-state") == "true"){
                $(".nav_0 .nav_1").eq(i).addClass("lihover").find(".nav_2").css("height",navheight[i]);
            }else{
                $(".nav_0 .nav_1").eq(i).removeClass("lihover").find(".nav_2").height(0);
            }
        }
        $(".nav_1 a").click(function(){
            var i =$(this).parent("li").index();
            if($(this).attr("date-state") == "true"){
                $(this).attr("date-state","false").siblings(".nav_2").css("height","0");
                $(this).parent("li").removeClass("lihover");
            }else{
                $(this).siblings(".nav_2").css("height",navheight[i]);
                $(this).parent("li").addClass("lihover").siblings().removeClass("lihover").find(".nav_2").css("height",0);
                $(this).attr("date-state","true").parent("li").siblings().find("a").attr("date-state","false");
            }
            var top =0
            for( var b = 0 ; b < i ; b++ ){
                if($(".nav_0 .nav_1").eq(b).find("a").attr("date-state") == "true"){
                    top = top + navheight[b]
                }
                top = top + 40
            }
            $(".bottom .leftBox .xian").css("top",top)
        })
        $(".nav_0 .nav_1").hover(function(){
            $(".bottom .leftBox .xian").css("opacity","1");
            var ii =$(this).index();
            var top =0
            for( var i = 0 ; i < ii ; i++ ){
                if($(".nav_0 .nav_1").eq(i).find("a").attr("date-state") == "true"){
                    top = top + navheight[i]
                }
                top = top + 40
            }
            $(".bottom .leftBox .xian").css("top",top)
        },function(){
            $(".bottom .leftBox .xian").css("opacity","0");
        })
    //确保进入时都不选中
    $(".operationTable input[type='checkbox']").each(function(){
        $(this).prop("checked",false);
    })
    //操作显示内容
    $("body table tr").each(function(){
        if( $(this).find(".state").attr("data-state") == "true"){
            $(this).find("td .lower").css("display","inline-block");
            $(this).find("td .show").css("display","none");
            $(this).find("td .two").text("正常");
        }else{
            $(this).find("td .lower").css("display","none");
            $(this).find("td .show").css("display","inline-block");
            $(this).find("td .two").text("下架");
        }
    })
    // 全选
    $("input[name='btSelectAll']").click(function(){
        if( $(this).prop("checked") == true){
            $("input[name='btSelectItem']").prop("checked",true);
            $(".operationButtom span").css("opacity","1");
        }else{
            $("input[name='btSelectItem']").prop("checked",false);
            $(".operationButtom span").css("opacity","0.6");
            $(".operationButtom .add").css("opacity","1");
        }
    });
    // 重置
    $(".closure input[type='reset']").click(function(){
        $(".addPic .picShow").html("");
        $("input[name='photoone']").each(function () {
            $(this).after($(this).clone().val(""));
            $(this).remove();
        })
        $(this).parent(".closure").siblings(".AddEditBottom").find(".addPic .upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
    })
    // 单选
    $(".operationTable input[name='btSelectItem']").click(function () {
        var size = $("body input[name='btSelectItem']").size()
        var i = 0
        $("body input[name='btSelectItem']").each(function(){
            if( $(this).prop("checked") == true){
                i ++;
            }
        })
        if( i >= 1){
            $(".operationButtom span").css("opacity","1");
        }else{
            $(".operationButtom span").css("opacity","0.6");
            $(".operationButtom .add").css("opacity","1");
        }
        if( i == size){
            $("input[name='btSelectAll']").prop("checked",true);
        }else{
            $("input[name='btSelectAll']").prop("checked",false);
        }
    })
    // 操作区域的最大高度
    var boxheightqq = $(".editBox").outerHeight(true);
    var edittabheightqq = $(".editTab").outerHeight(true);
    var closureheightqq = $(".editBox .closure").outerHeight(true);
        // alert(boxheightqq-edittabheightqq-closureheightqq-55)
    $(".editBox .editCon").css("max-height",boxheightqq-edittabheightqq-closureheightqq-55);
    //清楚缓存
    function clear1(){
        alert('清楚完成');
    }
    // //百度编辑器
    // UE.getEditor('intro_detail',{  //intro_detail为要编辑的textarea的id
    //     initialFrameWidth: 1000,  //初始化宽度
    //     initialFrameHeight: 460,  //初始化高度
    // });
//首页操作end

//下架操作start
    $(".operationButtom .lower").click(function () {
        var lowersize = new Array(); //想要下架的ID
        var i = 0
        var table = $('#table').val();
        $("body input[name='btSelectItem']").each(function(){
            if( $(this).prop("checked") == true){
                lowersize[i]  = $(this).parent().siblings("td").attr("id");
                i ++;
            }
        })
        if ( i > 0){
            var msg = "您真的确定要下架吗？"; 
            if (confirm(msg)==true){ 
                for(var a = 0 ; a <= i ; a++){
                    var htmls = document.getElementById(lowersize[a]);
                    var $htmls=$(htmls);
                    $htmls.parents("tr").find("td").attr("data-state","false").find(".two").text("下架");
                    $htmls.parents("tr").find("td .lower").css("display","none");
                    $htmls.parents("tr").find("td .show").css("display","inline-block");
                    $("body input[type='checkbox']").each(function(){
                        $(this).prop("checked",false);
                    })
                }
                ids = lowersize;
                // 定义参数
                var data = {id:ids,table:table};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'lower';
                $.post(url,data,function(data){
                    // console.log(data);
                });
            }else{ 
                return false; 
            }
        }else{
            alert("请选择第ID")
        }
    })
//下架操作end

// 显示操作start
    $(".operationButtom .show").click(function () {
        var showsize = new Array(); //想要显示的ID
        var i = 0
        var table = $('#table').val();
        $("body input[name='btSelectItem']").each(function(){
            if( $(this).prop("checked") == true){
                showsize[i]  = $(this).parent().siblings("td").attr("id");
                i ++;
            }
        })
        if ( i > 0){
            var msg = "您真的确定要显示吗？"; 
            if (confirm(msg)==true){ 
                for(var a = 0 ; a <= i ; a++){
                    var htmls = document.getElementById(showsize[a]);
                    var $htmls=$(htmls);
                    $htmls.parents("tr").find("td").attr("data-state","true").find(".two").text("正常");
                    $htmls.parents("tr").find("td .show").css("display","none");
                    $htmls.parents("tr").find("td .lower").css("display","inline-block");
                    $("body input[type='checkbox']").each(function(){
                        $(this).prop("checked",false);
                    })
                }
                ids = showsize;
                // 定义参数
                var data = {id:ids,table:table};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'show';
                $.post(url,data,function(data){
                    // console.log(data);
                });
            }else{ 
                return false; 
            }
        }else{
            alert("请选择第ID")
        }
    })
//显示操作end

//删除操作start
    // 删除按钮
    $(".operationButtom .delete").click(function () {
        var deletesize = new Array(); //想要删除的ID
        var i = 0
        var table = $('#table').val();
        $("body input[name='btSelectItem']").each(function(){
            if( $(this).prop("checked") == true){
                deletesize[i]  = $(this).parent().siblings("td").attr("id");
                i ++;
            }
        })
        if ( i > 0){
            var msg = "您真的确定要删除吗？"; 
            if (confirm(msg)==true){ 
                for(var a = 0 ; a <= i ; a++){
                    var htmls = document.getElementById(deletesize[a]);
                    var $htmls=$(htmls);
                    $htmls.parents("tr").remove()
                }
                ids = deletesize;
                // 定义参数
                var data = {id:ids,table:table};
                //开启token
                data['_token'] = $('#token').val();
                var url = 'del';
                $.post(url,data,function(data){
                    // console.log(data);
                });
            }else{ 
                return false; 
            }
        }else{
            alert("请选择第ID")
        }
    })
//删除操作end

//单个下架start
    function lower_one(e,id){
        var msg = "您真的确定要下架吗？"; 
        var table = $('#table').val();
        if (confirm(msg)==true){ 
            $(e).parents("tr").find("td .lower").css("display","none");
            $(e).parents("tr").find("td .show").css("display","inline-block");
            $(e).parents("tr").find("td").attr("data-state","false").find(".two").text("下架");
            // 定义参数
            var data = {id:id,table:table};
            //开启token
            data['_token'] = $('#token').val();
            var url = 'lower_show';
            $.post(url,data,function(data){
                // console.log(data);
            });
        }else{ 
            return false; 
        }
    }
//单个下架end

//单个显示start
    function show_one(e,id){
        var msg = "您真的确定要显示吗？"; 
        var table = $('#table').val();
        if (confirm(msg)==true){ 
            $(e).parents("tr").find("td .show").css("display","none");
            $(e).parents("tr").find("td .lower").css("display","inline-block");
            $(e).parents("tr").find("td").attr("data-state","true").find(".two").text("正常");
            // 定义参数
            var data = {id:id,table:table};
            //开启token
            data['_token'] = $('#token').val();
            var url = 'lower_show';
            $.post(url,data,function(data){
                // console.log(data);
            });
        }else{ 
            return false; 
        }
    }
//单个显示end

//单个删除start
    function del_one(e,id){
        var msg = "您真的确定要删除吗？"; 
        var table = $('#table').val();
        if (confirm(msg)==true){ 
            $(e).parents("tr").remove();
            // 定义参数
            var data = {id:id,table:table};
            //开启token
            data['_token'] = $('#token').val();
            var url = 'del_one';
            $.post(url,data,function(data){
                // console.log(data);
            });
        }else{ 
            return false; 
        }
    }
//单个删除end

//图片操作特效start
    // 操作区域的最大高度
    var boxheightqq = $(".editBox").outerHeight(true);
    var edittabheightqq = $(".editTab").outerHeight(true);
    var closureheightqq = $(".editBox .closure").outerHeight(true);
        // alert(boxheightqq-edittabheightqq-closureheightqq-55)
    $(".editBox .editCon").css("max-height",boxheightqq-edittabheightqq-closureheightqq-55);
    // 单张图片上传
    function preview(obj) {
        var $obj=$(obj);                //js转jQuery
        var $dom = $obj.parents(".inputPic").siblings(".picShow");
        var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
        if (obj.files && obj.files[0]) {
            var reader = new FileReader();
            reader.onload = function(evt) {
                $dom.html("<p class='picBox'><img src='" + evt.target.result + "'><i class='iconfont icon-shanchu'></i></p>");
            }
            reader.readAsDataURL(obj.files[0]);
            $obj.siblings("span").html("<i class='iconfont icon-shangchuan'></i>修改图片")
        } else {
            $dom.find("span").remove();
        }
    }
    //多张图片上传
    // 一个多图一份这个，picssrc进行改变；并且把函数放到点击事件里
    var picssrc = new Array();
    function picssrcfun(){
        var size =$(".picsUploading .picShow[name='picssrc'] .picBox").size();
        for (var a = 0; a < size; a++) {
            picssrc[a] = $(".picsUploading .picShow[name='picssrc'] .picBox").eq(a).find("img").attr("src");
        }
    }
    $(".editBox .closure .input[type='submit'] ").click(function(){
        picssrcfun()
    })
    function previews(obj){
        var $obj=$(obj);                //js转jQuery
        var $dom = $obj.parents(".inputPic").siblings(".picShow");
        var $domhtml = $obj.parents(".inputPic").siblings(".picShow").html();
        if (obj.files) {
            for(i = 0; i < obj.files.length ; i ++) {
                var reader = new FileReader();    
                reader.readAsDataURL(obj.files[i]);  
                reader.onload=function(e){  
                    //多图预览
                    $dom.append("<p class='picBox'><img src='" + this.result + "'><i class='iconfont icon-shanchu'></i></p>");
                }
            }
        } else {
            $dom.html(domhtml);
        }
    }
    // 图片删除
    $(".picShow").on("click",".picBox i",function(){
        $(this).parents(".picShow").siblings(".inputPic").find(".upload span").html("<i class='iconfont icon-shangchuan'></i>上传图片");
        $(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").after($(this).parents(".picShow").siblings(".inputPic").find(".upload input[date-shu='one']").clone().val(""));
        $(this).parent(".picBox").remove();
    })
    //提示的显示
    $("input").focusout(function(){
        if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
            $(this).parent("td").siblings(".prompt").css("opacity","1");
        }else{
            $(this).parent("td").siblings(".prompt").css("opacity","0");
        }
    })
    $("textarea").focusout(function(){
        if($(this).parent("td").siblings(".title").hasClass("required") == true && $(this).val() == ""){
            $(this).parent("td").siblings(".prompt").css("opacity","1");
        }else{
            $(this).parent("td").siblings(".prompt").css("opacity","0");
        }
    })
    // 操作区域界面
    $(".editTab li").click(function(){
        $(this).addClass("lihover").siblings().removeClass("lihover");
        $(".editBox .editCon li").eq($(this).index()).css("display","block").siblings().css("display","none");
    })
    //表单提交
    function zuzhitijiao(){
        biaoti = document.getElementById('biaoti').value;
        if (biaoti) {
            return true;
        }else{
            alert('标题必填');
            return false;
        }
    }
//图片操作特效start